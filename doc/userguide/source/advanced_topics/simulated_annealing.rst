.. highlight:: python
   :linenothreshold: 5

.. _simulated_annealing:

.. index::
   single: Objective function; sampling
   single: Simulated annealing
   single: Monte Carlo sampling
   single: Python interface; example
   single: Examples; Python interface


Optimization via simulated annealing
====================================

Local vs global optimization
----------------------------

As discussed to some extent in :ref:`this example
<mapping_parameter_landscape>` the :ref:`objective function
<objective_function>` :math:`\chi^2` corresponds to a highly
dimensional space with many local minima and an extremely large
variation in gradients and curvature. This complexity poses a
considerable challenge for numerical optimization. :ref:`Local
minimizers <global_vs_local_minimizers>` are designed to find the
closest minimum and thus rely on a good starting point. Global
minimizer minimizers attempt to alleviate this problem by combining a
more coarse grained sampling of the parameter space with local
minimization (using low convergence targets for the later). Both local
and global minimizers are available in :program:`atomicrex` via the
:ref:`NLopt <nlopt>` library. It is also possible to employ the Python
interface to hook up :program:`atomicrex` with suitable Python
libraries (e.g., `SciPy <https://www.scipy.org/>`_) or to implement
custom optimization strategies. An example for the latter approach is
discussed in the following.


Basic algorithm for simulation annealing
----------------------------------------

Simulated annealing is a particular optimization strategy that can be
very effective for complex and/or multi-dimensional parameter
spaces. The basic algorithm is in fact very simple and involves only
evaluations of the objective function itself (but not derivatives
thereof). Specifically it consists of the following basic steps:

#. Set up an :program:`atomicrex` fitjob.

#. Initialize counters for the number of trial steps, the acceptance rate for
   different types of steps etc.

#. Randomly select one of the parameters :math:`i`

#. Generate a trial parameter value
   :math:`p_i^{trial} = p_i^{previous} + \delta_i R` where :math:`R` is
   a uniformly distributed real number :math:`R \in [-1,1]` and
   :math:`\delta_i` is the trial step size.
#. Evaluate the objective function using the new parameter vector
   :math:`\boldsymbol{p}^{trial}` yielding :math:`\chi^2_{trial}`.

#. Apply the Metropolis criterion:

   #. Accept if :math:`\Delta\chi^2 = \chi^2_{trial} - \chi^2_{previous} < 0`.

   #. Else generate a uniformly distributed random number :math:`\phi
      \in [0,1)`.  Accept if :math:`\phi < \exp(-\Delta\chi^2 /
      \Theta)`; Reject otherwise. Here, :math:`\Theta` is the current
      temperature of the ensemble being sampled.

#. Update counters and collect averages.

#. In periodic intervals, rescale the trial step sizes to yield the
   targeted acceptance rate. A reasonable target value is usually
   about 0.2 (see [Fre12]_ for more information.)

#. Return to to step #3.


A practical example
--------------------

The algorithm outlined above can be varied and tailored in a number of
ways. It is implemented for demonstration in the
`sample_objective_function.py` script in the subdirectory
`examples/monte_carlo_sampling` (see below for a code listing). The
potential sampled here is identical to the one used already in several
other examples: :ref:`[1] <example_fitting_eam_potential>`, :ref:`[2]
<example_fitting_nlopt_minimizer>`, :ref:`[3]
<example_python_basic_fitting>`, :ref:`[4]
<mapping_parameter_landscape>`.

.. figure:: ../../../../examples/monte_carlo_sampling/reference_output/objective_function.svg
   :width: 56 %
   :alt: Objective function as a function of Monte Carlo trial step.

   Objective function (blue) and temperature (orange) during a
   simulated annealing run. The temperature is reduced linearly from
   its initial value to close to zero during the course of the
   simulation. A log-scale is used for the x-axis in order to
   highlight the initial decrease in the objective function.

As illustrated in the figure above the objective function drops
considerably during the first 20,000 to 40,000 MC trial steps, after
which it merely oscillates. In this case, one could thus terminate the
simulated annealing procedure at this point and continue with a
conventional local optimization approach, e.g., a gradient based
minimizer.

.. figure:: ../../../../examples/monte_carlo_sampling/reference_output/parameters.svg
   :width: 95 %
   :alt: Parameter variation during simulation annealing.

   Evolution of potential parameters during the simulated annealing run.

The figure above illustrates the variation of the four potential
parameters during the simulated annealing procedure. These variations
can be compared with the change in, e.g., the :math:`A` parameter that
is observed during the "conventional" (local) optimization described
in :ref:`this example <example_fitting_eam_potential>`. In the latter
case the parameter :math:`A` changes very little during the
optimization (e.g., :math:`A_{initial} = 500` vs :math:`A_{final} =
500.214`, compare :ref:`this example <example_fitting_eam_potential>`)
compared to the simulated annealing case. While the optimization
procedure readily reaches the (rather tight) convergence criteria, the
final objective function (:math:`\chi^2_{final}=30535.6`) is
substantially higher than in the present case (:math:`\chi^2_{final} =
15621.1`).

The observed behavior can be attributed to the very "rough" landscape
that applies already for this simple functional form. It is at least
partly related to the very large ratio between the largest and
smallest eigenvalue of the Hessian matrix :math:`\nabla^2_{\boldsymbol
p}\,\chi^2`, which was discussed :ref:`here
<mapping_parameter_landscape>` and is related to the combination of
exponential and linear terms in the functional form.



Location of files
------------------

`examples/monte_carlo_sampling`

Input files
------------------

* `sample_objective_function.py`: script for sampling the objective function

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/monte_carlo_sampling/sample_objective_function.py
       :linenos:
       :language: python


* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/monte_carlo_sampling/main.xml
       :linenos:
       :language: xml

* `potential.xml`: initial parameter set (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/monte_carlo_sampling/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/monte_carlo_sampling/structures.xml
       :linenos:
       :language: xml
