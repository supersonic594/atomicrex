.. index::
   single: Installation
   single: Setup

Installation
*********************

Downloading the code
================================

The code is maintained as a public Git repository on `GitLab
<https://gitlab.com/atomicrex/atomicrex>`_. You can access it using the ``git`` command 
and copy the repository to your local computer by running::

    $ git clone https://gitlab.com/atomicrex/atomicrex.git
    $ cd atomicrex

This will create a new directory named :file:`atomicrex` on your local
machine containing a copy of the :program:`atomicrex` source files.

Compiling the code
================================

Building :program:`atomicrex` from source requires the following:

  #. a C++ compiler and a Fortran compiler,
  #. the `CMake build system <http://www.cmake.org/>`_,
  #. the :program:`xxd` system tool,
  #. several external libraries: 

     #. `boost <http://www.boost.org/>`_, 
     #. `libxml2 <http://www.xmlsoft.org/>`_, 
     #. `muparser <http://muparser.beltoforion.de/>`_, and
     #. `nlopt <http://ab-initio.mit.edu/wiki/index.php/NLopt>`_,

  #. Python interpreter (version 2 or 3) including

     #. Python development files for compiling the :program:`atomicrex` :ref:`Python interface <python_interface>`,
     #. the `Sphinx <http://sphinx-doc.org/>`_ module for generating the user guide in HTML format, and
     #. the `Numpy <http://numpy.org/>`_ module.

On :program:`Ubuntu Linux` these requirements can be readily installed by running::

  sudo apt-get install \
    python-dev \
    python-sphinx \
    python-numpy \
    libboost-dev \
    libxml2-dev \
    libnlopt-dev \
    libmuparser-dev \
    build-essential \
    gfortran \
    vim-common \
    cmake

If you prefer the newer version 3 of the Python interpreter, run:: 

  sudo apt-get install \
    python3-dev \
    python3-sphinx \
    python3-numpy
	 
To compile the code, create a new subdirectory, typically in the local
directory created above, and invoke CMake::

   $ mkdir build
   $ cd build
   $ cmake ..

If CMake succeeds in finding all the necessary libraries, it will
create a Makefile. Now the compilation can be launched by
running::

   $ make

To build the user guide, run::

   $ make userguide

The generated HTML files can be found in the :file:`doc/userguide/build/` directory.

Manually specifying paths
----------------------------------

If CMake does not succeed in finding the external libraries, it is
necessary to manually configure the paths. To this end, you have
different options. This can for example be accomplished by specifying
parameters using the `-D` flag of CMake::

   $ cmake -D NLOPT_INCLUDE_DIR=../path_to_success/ -D NLOPT_LIBRARY=../path_to_success/ .

Alternatively, one can invoke the interactive CMake version by
executing::

   $ ccmake .

Note that it is usually necessary to switch to the
"advanced mode" in order to access the path settings for the libraries.


Compilation options
----------------------------------

By default :program:`atomicrex` is compiled as a release version,
which implies that optimizations are included. If this is not
desirable e.g., for debugging purposes, a debug version can be
requested by running::

   $ cmake -D CMAKE_BUILD_TYPE=Debug .

In this context it can also be useful to turn on verbose output
during compilation, which is accomplished by running `make` as
follows::

   $ make VERBOSE=1

If supported by your C++ compiler, :program:`atomicrex` automatically takes 
advantage of OpenMP parallelization. To explicitly deactivate this feature, 
the `USE_OPENMP` option can switched to `OFF`::
   
   $ cmake -D USE_OPENMP=OFF .

In addition there are several optional features. The use of the
non-linear optimization library `NLopt
<http://ab-initio.mit.edu/wiki/index.php/NLopt>`_ can be disabled via
`ENABLE_NLOPT`.

.. tip:: 

   The non-linear optimization library `NLopt
   <http://ab-initio.mit.edu/wiki/index.php/NLopt>`_ has to be
   compiled as a shared library. Otherwise you might get the following
   error message such as `.rodata.str1.1 can not be used when making a
   shared object` / `error adding symbols: Bad value`. To circumvent
   this error compile `NLopt
   <http://ab-initio.mit.edu/wiki/index.php/NLopt>`_ using the
   `--enable-shared` option during the `configure` stage.

The Python interface functionality can be activated or deactived using the
`ENABLE_PYTHON_INTERFACE` option.
