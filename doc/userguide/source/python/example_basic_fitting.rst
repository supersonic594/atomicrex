.. highlight:: python
   :linenothreshold: 5

.. _example_python_basic_fitting:

.. index::
   single: Python interface; example
   single: Python interface; scipy
   single: Examples; Python interface


Example: Basic fitting
==============================================

This example illustrates the optimization of potential parameter via
the Python interface. It demonstrates both how to start optimizers
that are integrated directly in :program:`atomicrex` and how to use
optimizers imported from other Python librariers, specifically `scipy
<https://www.scipy.org/>`_. Here, potential definition and structures
are provided via xml files.  Note that in the case of the internal
optimizers the settings are read from the xml input file.

Location
------------------

`examples/python_basic_fitting`

Input files
------------------

* `fit_potential.py`: main input file

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_basic_fitting/fit_potential.py
       :linenos:
       :language: python

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_basic_fitting/main.xml
       :linenos:
       :language: xml

* `potential.xml`: definition of potential form and parameters

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_basic_fitting/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: definition of structures and properties

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_basic_fitting/structures.xml
       :linenos:
       :language: xml

		  
Output (files)
------------------

* `log.python_atomicrex`: stdout from run using an internal optimizer
  from :program:`atomicrex`

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_basic_fitting/reference_output/log.python_atomicrex
       :linenos:
       :language: text

* `log.python_scipy`: stdout from run using `optimize.minimize` from
  `scipy <https://www.scipy.org/>`_

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_basic_fitting/reference_output/log.python_scipy
       :linenos:
       :language: text

* `log.no_python` [for reference]: stdout from "pure" (no Python
  involved) execution of :program:`atomicrex`

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_basic_fitting/reference_output/log.no_python
       :linenos:
       :language: text
