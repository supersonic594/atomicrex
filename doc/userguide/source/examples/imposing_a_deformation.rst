.. _example_imposing_a_deformation:

.. index::
   single: Embedded atom method (EAM); example
   single: Examples; Embedded atom method (EAM)
   single: Examples; User defined functions
   single: User defined functions; example
   single: Examples; User defined structures
   single: User defined structures; example
   single: Examples; Deformations
   single: Deformations; example
   single: Examples; extended markup language (XML) inclusions
   single: extended markup language (XML); Inclusions; example


Using deformations with user defined structures
=================================================================================================

This example demonstrates the use of the :ref:`deformations
<deformations>`.  It employs the :ref:`embedded atom method (EAM)
potential <eam_potential>` routine together with :ref:`user defined
functions <user_defined_functions>` [MisMehPap01]_. The example also
illustrates the use of :ref:`XML Inclusions <xml_inclusions>`.

Location
------------------

`examples/imposing_a_deformation`

Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/imposing_a_deformation/main.xml
       :linenos:
       :language: xml

* `potential.xml`: initial parameter set (included in main input file
  via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/imposing_a_deformation/potential.xml
       :linenos:
       :language: xml


Output
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/imposing_a_deformation/reference_output/log
       :linenos:
       :language: text
