.. _example_fitting_nlopt_minimizer:

.. index::
   single: NLopt minimizer; example
   single: Global minimizer; example
   single: Examples; NLopt minimizer
   single: Examples; Global minimizer


Fitting using a minimizer from the NLopt library
======================================================================

This example demonstrates the use of the :ref:`NLopt minimizers
<nlopt>` including in particular the `global optimization algorithms
<http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms>`_. The
example employs a simple embedded atom method potential (EAM) and a
structure database that is also employed in another :ref:`example
<example_fitting_eam_potential>`.

Note that in particular for the global algorithms bounds have been
specified for potential parameters *and* structural properties. As
discussed :ref:`here <global_bounds>` these constraints are necessary
for using these algorithms.

Also note that for the present (very simple) problem the global
algorithms yield higher residuals than the standard internal
optimization via L-BFGS-B. This is related to fact that global
algorithms (usually) do not pursue a very detailed optimization of
individual minima. The lowest of these should/can be subsequently
improved by using conventional (local) minimizers with tight
convergence criteria.

In the present case all algorithms find "optimal" parameters in the
same neighborhood of parameter space. Global optimization algorithms
become truely advantageous in (much) more complex cases with a larger
number of parameters and a more complex functional dependence.


Location
------------------

`examples/fitting_NLopt_minimizer`

Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/main.xml
       :linenos:
       :language: xml

* `potential.xml`: initial parameter set (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/structures.xml
       :linenos:
       :language: xml

* `POSCAR_res_POSCAR_0.9.traj_forces`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/POSCAR_res_POSCAR_0.9.traj_forces
       :linenos:
       :language: text

* `POSCAR_res_POSCAR_1.0.traj_forces`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/POSCAR_res_POSCAR_1.0.traj_forces
       :linenos:
       :language: text

* `POSCAR_res_POSCAR_1.1.traj_forces`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/POSCAR_res_POSCAR_1.1.traj_forces
       :linenos:
       :language: text


Output (files)
------------------

Log files containing the final properties and parameters are provided
for each algorithm separately.

* `log.LD_LBFGS`: limited-memory Broyden-Fletcher-Goldfarb-Shanno (L-BFGS) algorithm

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/reference_output/log.LD_LBFGS
       :linenos:
       :language: text

* `log.LD_MMA`: Method of moving asymptotes (MMA) and conservative convex separable approximation (CCSA)

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/reference_output/log.LD_MMA
       :linenos:
       :language: text

* `log.LN_NELDERMEAD`: Nelder-Mead Simplex

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/reference_output/log.LN_NELDERMEAD
       :linenos:
       :language: text

* `log.LN_SBPLX`: Sbplx (based on Subplex)

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/reference_output/log.LN_SBPLX
       :linenos:
       :language: text

* `log.GN_CRS2_LM`: Controlled random search (CRS) with local mutation

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/reference_output/log.GN_CRS2_LM
       :linenos:
       :language: text

* `log.GN_ESCH`: ESCH (evolutionary algorithm)

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/reference_output/log.GN_ESCH
       :linenos:
       :language: text

* `log.GN_DIRECT`: DIRECT and DIRECT-L
  
  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/reference_output/log.GN_DIRECT
       :linenos:
       :language: text
		  
* `log.GN_ISRES`: ISRES (Improved Stochastic Ranking Evolution Strategy)

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_NLopt_minimizer/reference_output/log.GN_ISRES
       :linenos:
       :language: text

		  
