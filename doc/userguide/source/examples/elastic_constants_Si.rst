.. _example_elastic_constants_GaN:

.. index::
   single: Analytic bond-order potential (ABOP); example
   single: Examples; Analytic bond-order potential (ABOP)
   single: Examples; Elastic constants
   single: Elastic constants ; example
   single: Elastic constants ; clamped ion
   single: Elastic constants ; relaxed ion

Clamped and relaxed ion elastic constants
=================================================================

This example demonstrates relaxation and property evaluation for cubic
structures of silicon. In particular, it illustrates how one can
compute both the 'clamped ion' elastic constant :math:`c_{44}^0` and
the 'relaxed ion' elastic constant :math:`c_{44}`.


Location
--------

`examples/elastic_constants_Si`

Input files
-----------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/elastic_constants_Si/main.xml
       :linenos:
       :language: xml

* `SiC.tersoff`: initial parameter set in `Lammps/Tersoff format
  <http://lammps.sandia.gov/doc/pair_tersoff.html>`_

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/elastic_constants_Si/SiC.tersoff
       :linenos:
       :language: text


Output
------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/elastic_constants_Si/reference_output/log
       :linenos:
       :language: text
