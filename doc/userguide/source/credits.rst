.. _credits:
.. index:: Credits

Credits
***************

The development of :program:`atomicrex` was started in 2011 by
Alexander Stukowski (Technische Universität Darmstadt, Germany) and
Paul Erhart (Chalmers University of Technology, Gothenburg,
Sweden). Notable contributions were made by Markus Mock (Technische
Universität Darmstadt, Germany), Staffan Nilsson, Staffan Ankardal,
Erik Fransson, and Mattias Ångqvist (Chalmers University of
Technology, Gothenburg, Sweden).

If you use :program:`atomicrex` in your research please include
the following citation in publications or presentations:

* A. Stukowski, E. Fransson, M. Mock, and P. Erhart,
  *Atomicrex - a general purpose tool for the construction of
  atomic interaction models*,
  Modelling Simul. Mater. Sci. Eng. **25**, 055003 (2017),
  `doi: 10.1088/1361-651X/aa6ecf
  <http://dx.doi.org/10.1088/1361-651X/aa6ecf>`_

-------

:program:`atomicrex` relies on several external libraries that provide
crucial functionality:

* `boost <http://www.boost.org/>`_
* `libxml2 <http://www.xmlsoft.org/>`_: a library for parsing XML
  documents
* `muparser <http://muparser.beltoforion.de/>`_: a fast math parser
  used for generating user defined functions
* `nlopt <http://ab-initio.mit.edu/wiki/index.php/NLopt>`_: a library
  for nonlinear optimization
