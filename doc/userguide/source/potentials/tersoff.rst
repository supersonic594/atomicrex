.. _tersoff_potential:
.. index::
   single: Cluster functionals; Tersoff potential
   single: Three-body potentials; Tersoff potential
   single: Tersoff potential; description
   single: Tersoff potential; parameters
   single: <tersoff>

Tersoff potential
====================

The Tersoff potential [Ter88b]_ in :program:`atomicrex` follows the
implementation in Lammps.  This version supports multi-component
systems with *all* parameters being dependent on the types of the
atoms involved, i.e. the two-body terms dependent on the types of
atoms :math:`i` and :math:`j`, whereas the three-body terms depend on
the types of atoms :math:`i`, :math:`j`, and :math:`k`. Note that in
the :ref:`analytic bond-order potential (ABOP) form <abop_potential>`
[Bre90]_ the three-body terms are usually dependent on the types of
atoms :math:`i` and :math:`k`, only (see e.g., [AlbNorAve02]_,
[AlbNorNor02]_, [ErhAlb05]_, [JusErhTra05]_). If you want to obtain
this behavior please resort to the ABOP form.

The Tersoff potential form can be written

.. math::
   E = \frac{1}{2} \sum_i \sum_{j\neq i} V_{ij}

with

.. math::
   V_{ij} = f_c(r_{ij}) \left[f_R(r_{ij}) + b_{ij} f_A(r_{ij}) \right].

Here, :math:`V_R(r) = A\exp(-\lambda_1 r)` and :math:`V_A(r) =
-B\exp(-\lambda_2 r)` are repulsive and attractive pair potential
branches, respectively, and :math:`f_c(r_{ij})` is a cut-off function
that is unity and decays sinusodially in the interval
:math:`(R-D,R+D)`, beyond which it vanishes. The three-body
contributions arise due to the bond-order parameter

.. math::
   b_{ij} = \left( 1+\beta^n\zeta_{ij}^n \right)^{-\frac{1}{2n}}

where

.. math::
   \zeta_{ij} = \sum_{k\neq i,j} f_c(r_{ij})g(\theta_{ijk})\exp \left[ \lambda_3^m(r_{ij}-r_{ik})^m \right].

The angular dependence is due to the factor

.. math::
   g(\theta) = \gamma_{ijk} \left( 1+\frac{c^2}{d^2} - \frac{c^2}{d^2 + (h - \cos \theta)^2}\right).

Parameter files are written in `Lammps/Tersoff format
<http://lammps.sandia.gov/doc/pair_tersoff.html>`_.

The following code snippet, to be inserted in the ``<potentials>``
block, illustrates the definition of this potential type in the input
file.

.. code:: xml

   <tersoff id="Co" species-a="*" species-b="*">
      <param-file>Co.tersoff</param-file>
      <fit-dof>
         <A tag="CoCoCo" enabled="true" />
         <B tag="CoCoCo" enabled="true" />
         <lambda1 tag="CoCoCo" enabled="true" />
         <lambda2 tag="CoCoCo" enabled="true" />
         <gamma tag="CoCoCo" enabled="true"/>
         <lambda3 tag="CoCoCo" enabled="true" />
         <c tag="CoCoCo" enabled="false" />
         <d tag="CoCoCo" enabled="false" />
         <theta0 tag="CoCoCo" enabled="false" />
      </fit-dof>
   </tersoff>

.. warning::
   Note that the ``species-a`` and ``species-b`` attributes should always
   be set to ``*``. The assignment of atom types is handled using the
   information in the ``tag`` attribute.

.. rubric:: Elements and attributes

* ``<param-file>``: Name of file with input parameters in
  `Lammps/Tersoff format
  <http://lammps.sandia.gov/doc/pair_tersoff.html>`_
* ``<export-potential>`` (optional): Name of file, to which potential
  parameters are being written in `Lammps/Tersoff format
  <http://lammps.sandia.gov/doc/pair_tersoff.html>`_ [Default: no file
  is written].

.. rubric:: Further information

More detailed information can be found in [Abe85]_, [Ter86]_, [Ter88a]_,
[Ter88b]_, [Ter88c]_, [Ter89]_, [Bre89]_, [Bre90]_, [BreSheHar02]_,
[AlbNorAve02]_, [AlbNorNor02]_, [NorAlbErh03]_, [ErhAlb05]_, [ErhJusGoy06]_,
[JusErhTra05]_, [MulErhAlb07a]_, [MulErhAlb07b]_, [PetGreWah15]_.
