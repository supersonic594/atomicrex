.. _abop_potential:
.. index::
   single: Cluster functionals; Analytic bond-order potential (ABOP)
   single: Three-body potentials; Analytic bond-order potential (ABOP)
   single: Analytic bond-order potential (ABOP); description
   single: Analytic bond-order potential (ABOP); parameters
   single: <abop>

Analytic bond-order potentials
=======================================

The ABOP potential [Bre90]_ is a reformulation of the :ref:`Tersoff
potential <tersoff_potential>` [Ter88b]_ as implemented in
:program:`atomicrex` and Lammps. The difference lies in the two-body
parameters :math:`A`, :math:`B`, :math:`\lambda_1`, and
:math:`\lambda_2`, which have been rewritten in terms of the more
physical choices, :math:`r_0`, :math:`D_0`, :math:`\beta`, and
:math:`S`, which relate to dimer properties and the Pauling relation.

The implementation supports multi-component systems with *all*
parameters being dependent on the types of the atoms involved,
i.e. the two-body terms dependent on the types of atoms :math:`i` and
:math:`j`, whereas the three-body terms depend on the types of atoms
:math:`i`, :math:`j`, and :math:`k`. Note that the three-body terms
are usually dependent on the types of atoms :math:`i` and :math:`k`,
only (see e.g., [AlbNorAve02]_, [AlbNorNor02]_, [ErhAlb05]_,
[JusErhTra05]_). This behavior can be obtained by setting the
`classic-mode` attribute to `True` (default: `False`).

In the ABOP format, the potential can be written

.. math::
   E = \frac{1}{2} \sum_i \sum_{j\neq i} V_{ij}

with

.. math::
   V_{ij} = f_c(r_{ij}) \left[ f_R(r_{ij}) + b_{ij} f_A(r_{ij}) \right].

Here, :math:`V_R(r) = \frac{D_0}{S-1} \exp[-\beta\sqrt{2S} (r-r_0)]`
and :math:`V_A(r) = -\frac{SD_0}{S-1}\exp[-\beta \sqrt{2/S}(r-r_0)]`
are repulsive and attractive pair potential branches, respectively,
and :math:`f_c(r_{ij})` is a cut-off function that is unity and decays
sinusodially in the interval :math:`(R-D,R+D)`, beyond which it
vanishes. The three-body contributions arise due to the bond-order
parameter

.. math::
   b_{ij} =\left( 1+\beta_2^n\zeta_{ij}^n \right)^{-\frac{1}{2n}}

where

.. math::
   \zeta_{ij} = \sum_{k\neq i,j} f_c(r_{ij})g(\theta_{ijk})\exp \left[ \lambda_3^m(r_{ij}-r_{ik})^m \right].

The angular dependence is due to the factor

.. math::
   g(\theta) = \gamma_{ijk} \left( 1+\frac{c^2}{d^2} - \frac{c^2}{d^2 + (h + \cos \theta)^2}\right).

Parameter files are written in the Lammps Tersoff format. In the
output files, the :math:`r_0`, :math:`D_0`, :math:`\beta`, and
:math:`S` parameters are included in comment blocks.

Note that the :math:`h` parameter in the ABOP form has the *opposite*
sign than in the Tersoff formulation.

The following code snippet, to be inserted in the `potentials` block,
illustrates the definition of this potential type in the input file.

.. code:: xml

   <abop id="Si" species-a="*" species-b="*">
      <param-file>ABOP_potential.tersoff</param-file>
      <export-potential>ABOP_potential_fitted.tersoff</export-potential>
      <fit-dof>
         <r0 tag="SiSiSi" enabled="true" />
         <D0 tag="SiSiSi" enabled="true" />
         <beta tag="SiSiSi" enabled="true" />
         <S tag="SiSiSi" enabled="true" />
         <gamma tag="SiSiSi" enabled="true" />
         <c tag="SiSiSi" enabled="true" />
         <d tag="SiSiSi" enabled="true" />
         <h tag="SiSiSi" enabled="true" />
         <twomu tag="SiSiSi" enabled="false" />
         <beta2 tag="SiSiSi" enabled="false" />
         <powern tag="SiSiSi" enabled="false" />
    </fit-dof>
   </abop>

.. warning::
   Note that the `species-a` and `species-b` attributes should always
   be set to `*`. The assignment of atom types is handled using the
   information in the `tag` attribute.

.. rubric:: Elements and attributes

* `<param-file>`: Name of file with input parameters in Lammps/Tersoff
  format.
* `<export-potential>` (optional): Name of file, to which potential
  parameters are being written in Lammps/Tersoff format [Default: no
  file is written].
* `<classic-mode>` (optional): Enforce "classic" coupling of
  three-body parameters, which then depend only on the types of atoms
  :math:`i` and :math:`k` (rather than :math:`i`, :math:`j`, *and*
  :math:`k`) [Default: `False`]

.. rubric:: Further information

An extensive list of references is provided :ref:`here
<bibliography_tersoff_abop>`.
