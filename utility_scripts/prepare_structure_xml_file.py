#!/usr/bin/env python
# PE, 2015/06/04

description_string =
"""The script reads an ASE compatible input files containing one or
more configurations of positions and forces. It then prints XML code
to standard output that can be inserted into an atomicrex input file.

"""

import xml.etree.cElementTree as ET
import os

#------------------------------------------------
# Handle the command line
import argparse
parser = argparse.ArgumentParser(description=description_string)
parser.add_argument('files', nargs='+', action='append',
                    help='input files in modified POSCAR format for force matching.')
args = parser.parse_args()

root = ET.Element('group')
root.attrib['exclude-fit'] = 'false'
root.attrib['exclude-output'] = 'false'
for fname in args.files[0]:

    structure = ET.SubElement(root, 'user-structure')
    structure.attrib['id'] = os.path.basename(fname)

    conffile = ET.SubElement(structure, 'poscar-file').text = fname

    properties = ET.SubElement(structure, 'properties')

    with open(fname, 'r') as f:
        target_energy = float(f.readline().split()[0])
    
    energy = ET.SubElement(properties, 'atomic-energy')
    energy.attrib['relax'] = 'false'
    energy.attrib['fit'] = 'true'
    energy.attrib['target'] = '%.6f' % target_energy

    forces = ET.SubElement(properties, 'atomic-forces')
    forces.attrib['fit'] = 'true'
    forces.attrib['output-all'] = 'true'

print ET.tostring(root)
