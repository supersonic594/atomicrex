///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "../minimizers/Minimizer.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

class FitObject;
class AtomicStructure;
class Potential;

/**
* @brief      Base class for all degrees of freedom a potential or a structure may have.
*
* @details    The degrees of freedom of a potential are varied during fitting to minimize
 * the objective function.
 *
 * The degrees of freedom of an atomic structure (e.g. the atomic coordinates) are
 * varied to relax the structure.
 *
 * Note that a DOF may actually consist of several (sub-)degrees of freedom.
 * For example, a vector DOF consists of three internal scalar DOFs.
*/
class DegreeOfFreedom
{
public:
    /**
 * @brief   Constructor.
 * @param   id  ID associated with degree of freedom
 * @param   tag Tag aassociated with degree of freedom
 */
    DegreeOfFreedom(const FPString& id = FPString(), const FPString& tag = FPString())
        : _id(id), _tag(tag), _fitEnabled(false), _relax(false), _object(nullptr), _resetBeforeRelax(true)
    {
    }

    /// Virtual destructor.
    virtual ~DegreeOfFreedom() {}

    /// Return the identifier of this degree of freedom.
    const FPString& id() const { return _id; }

    /// Sets the identifier of this degree of freedom.
    void setId(const FPString& id) { _id = id; }

    /// Returns the tag on this DOF.
    const FPString& tag() const { return _tag; }

    /// Sets the tag on this DOF.
    void setTag(const FPString& tag) { _tag = tag; }

    /// Returns whether this DOF should be varied during the fitting process.
    bool fitEnabled() const { return _fitEnabled; }

    /// Sets whether this DOF should be varied during the fitting process.
    void setFitEnabled(bool enable) { _fitEnabled = enable; }

    /// Returns whether this DOF should be relaxed during each iteration when fitting.
    bool relax() const { return _relax; }

    /// Sets whether this DOF is being relaxed during each iteration when fitting.
    void setRelax(bool enable) { _relax = enable; }

    /// Returns a pointer to the potential or structure to which this DOF belongs.
    FitObject* object() const { return _object; }

    /**
     * @brief   Returns the number of scalar degrees of freedom this DOF is composed of.
     * @details This is the number of entries created in the state vector during fitting.
     */
    virtual int numScalars() = 0;

    /// Lets the DOF export its current value(s) into the given value array.
    virtual void exportValue(double*& dst) = 0;

    /// Lets the DOF import its value(s) from the given value array.
    virtual void importValue(const double*& src) = 0;

    /// Returns whether this DOF has any bound constraints applied to it.
    virtual bool hasBoundConstraints() const { return false; }

    /// Returns whether this DOF satisfies the bound constraints applied to it.
    virtual bool satisfiesBoundConstraints() const { return true; }

    /// Lets the DOF write its constraints information to the given linear arrays.
    virtual void getBoundConstraints(std::vector<Minimizer::BoundConstraints>::iterator& types,
                                     std::vector<double>::iterator& lowerBounds, std::vector<double>::iterator& upperBounds)
    {
        std::fill(types, types + numScalars(), Minimizer::NO_BOUNDS);
        types += numScalars();
        lowerBounds += numScalars();
        upperBounds += numScalars();
    }

    /// Outputs the current value of the DOF.
    virtual void print(MsgLogger& stream) {}

    /// Returns whether the DOF is reset to the initial value specified by the user each time
    /// a new relaxation round is performed.
    bool resetBeforeRelax() const { return _resetBeforeRelax; }

    /// Sets whether the DOF is reset to the initial value specified by the user each time
    /// a new relaxation round is performed.
    void setResetBeforeRelax(bool enableReset) { _resetBeforeRelax = enableReset; }

    /// Resets the value of the DOF to what was specified in the job file.
    virtual void reset() {}

public:
    /// Parse the contents of the <fit> element in the job file.
    virtual void parseFit(XML::Element fitElement);

    /// Parse the contents of the <relax> element in the job file.
    virtual void parseRelax(XML::Element relaxElement);

    /// Produces an XML representation of the DOF's current value.
    virtual XML::OElement generateXMLValueDefinition() const;

    /// Produces an XML representation of the DOF's fit settings.
    virtual XML::OElement generateXMLFitDefinition() const;

private:
    /// The identifier of this DOF.
    FPString _id;

    /// An additional tag to further differentiate between DOFs with the same ID.
    FPString _tag;

    /// Controls whether this DOF should be varied during the fitting process.
    bool _fitEnabled;

    /// Controls whether this DOF is being relaxed during each iteration when fitting.
    bool _relax;

    /// Controls whether the DOF is reset to the initial value specified by the user each time
    /// a new relaxation round is performed.
    bool _resetBeforeRelax;

    /// The object (can be either a potential or a structure) to which this degree of freedom belongs.
    FitObject* _object;

    friend class FitObject;
};

/**
 * @brief   A simple scalar degree of freedom that consists of a single value.
 */
class ScalarDOF : public DegreeOfFreedom
{
public:
    /// Constructor.
    ScalarDOF(const FPString& id = FPString(), double defaultValue = 0,
              double minValue = -std::numeric_limits<double>::infinity(),
              double maxValue = std::numeric_limits<double>::infinity())
        : DegreeOfFreedom(id),
          _value(defaultValue),
          _initialValue(defaultValue),
          _minValue(minValue),
          _maxValue(maxValue),
          _masterDOF(nullptr)
    {
    }

    /// Returns the current value of the DOF.
    operator double() const { return _value; }

    /// Returns the current value of the DoF.
    double getValue() { return _value; }

    /// Assigns a new value to the DOF.
    ScalarDOF& operator=(double value);

    /// Assigns a new value to the DOF.
    void setValue(double value) { _value = value; };

    /// Returns a pointer to the internal storage field of this DoF.
    double* getValuePointer() { return &_value; }

    /// Sets the initial value of this DOF. This also changes the current value to the same value.
    void setInitialValue(double value)
    {
        _initialValue = value;
        *this = value;
    }

    /// Returns the initial value of this DOF.
    double initialValue() const { return _initialValue; }

    /// Resets the value of the DOF to what was given in the job file.
    virtual void reset() override
    {
        DegreeOfFreedom::reset();
        *this = _initialValue;
    }

    /// Returns the number of scalar degrees of freedom this DOF is composed of.
    virtual int numScalars() override { return 1; }

    /// Lets the DOF export its current value(s) into the given value array.
    virtual void exportValue(double*& dst) override;

    /// Lets the DOF import its value(s) from the given value array.
    virtual void importValue(const double*& src) override;

    /// Outputs the current value of the DOF.
    virtual void print(MsgLogger& stream) override;

    /// Returns the lower bound if this DOF has a constraint applied to it.
    double lowerBound() const { return _minValue; }

    /// Returns whether this DOF has a lower bound constraint applied to it.
    bool hasLowerBound() const { return _minValue != -std::numeric_limits<double>::infinity(); }

    /// Sets a lower bound constraint on this DOF.
    void setLowerBound(double lowerBound) { _minValue = lowerBound; }

    /// Returns the upper bound if this DOF has a constraint applied to it.
    double upperBound() const { return _maxValue; }

    /// Returns whether this DOF has an upper bound constraint applied to it.
    bool hasUpperBound() const { return _maxValue != std::numeric_limits<double>::infinity(); }

    /// Sets an upper bound constraint on this DOF.
    void setUpperBound(double upperBound) { _maxValue = upperBound; }

    /// Returns whether this DOF has any bound constraints applied to it.
    virtual bool hasBoundConstraints() const override { return hasLowerBound() || hasUpperBound(); }

    /// Returns whether this DOF satisfies the bound constraints applied to it.
    virtual bool satisfiesBoundConstraints() const override
    {
        if(hasLowerBound() && _value < lowerBound()) return false;
        if(hasUpperBound() && _value > upperBound()) return false;
        return true;
    };

    /// Lets the DOF write its constraints information to the given linear arrays.
    virtual void getBoundConstraints(std::vector<Minimizer::BoundConstraints>::iterator& types,
                                     std::vector<double>::iterator& lowerBounds,
                                     std::vector<double>::iterator& upperBounds) override
    {
        if(hasLowerBound()) {
            if(hasUpperBound())
                *types++ = Minimizer::BOTH_BOUNDS;
            else
                *types++ = Minimizer::LOWER_BOUNDS;
        }
        else if(hasUpperBound())
            *types++ = Minimizer::UPPER_BOUNDS;
        else
            *types++ = Minimizer::NO_BOUNDS;
        *lowerBounds++ = _minValue;
        *upperBounds++ = _maxValue;
    }

    /**
     * @brief   Sets this DOF to be equal to another DOF.
     * @details After calling this function the DOF will no longer be a real DOF.
     *          Every time the master DOF changes, the value of this DOF will be updated accordingly.
     */
    bool setEqualTo(ScalarDOF* masterDOF);

public:
    /// Parse the contents of the <fit> element in the job file.
    virtual void parseFit(XML::Element fitElement) override;

    /// Parse the contents of the <relax> element in the job file.
    virtual void parseRelax(XML::Element relaxElement) override;

    /// Parses the 'equalto' attribute and links the DOF to another master DOF.
    void parseSetEqualToAttribute(XML::Element element);

    /// Produces an XML representation of the DOF's current value.
    virtual XML::OElement generateXMLValueDefinition() const override;

    /// Produces an XML representation of the DOF's fit settings.
    virtual XML::OElement generateXMLFitDefinition() const override;

private:
    /// The current value of the DOF.
    double _value;

    /// The initial value of the DOF as given in the job file.
    double _initialValue;

    /// The lower bound for this DOF.
    double _minValue;

    /// The upper bound for this DOF.
    double _maxValue;

    /// Points to the master DOF if this DOF is set to be equal to another DOF.
    ScalarDOF* _masterDOF;

    /// List of functions to be called each time the value of this DOF changes.
    std::vector<std::function<void(ScalarDOF&)>> _changeListeners;
};

}  // End of namespace
