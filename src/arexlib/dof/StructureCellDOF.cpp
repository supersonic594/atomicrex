///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "StructureCellDOF.h"
#include "../structures/AtomicStructure.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Returns the structure to which this DOF belongs.
******************************************************************************/
AtomicStructure* StructureCellDOF::structure() const { return static_cast<AtomicStructure*>(object()); }

/******************************************************************************
* Reset the atom positions to the values supplied in the job file.
******************************************************************************/
void StructureCellDOF::reset()
{
    DegreeOfFreedom::reset();
    structure()->changeSimulationCell(structure()->initialSimulationCell(), false);
}

/******************************************************************************
* Returns the number of scalar degrees of freedom this DOF is composed of.
******************************************************************************/
int StructureCellDOF::numScalars() { return 9; }

/******************************************************************************
* Lets the DOF export its current value(s) into the given vector.
******************************************************************************/
void StructureCellDOF::exportValue(double*& dst)
{
    BOOST_ASSERT(sizeof(structure()->simulationCell()) == sizeof(double) * numScalars());
    memcpy(dst, &structure()->simulationCell(), sizeof(double) * numScalars());
    dst += numScalars();
}

/******************************************************************************
* Lets the DOF import its value(s) from the given vector.
******************************************************************************/
void StructureCellDOF::importValue(const double*& src)
{
    Matrix3 newCell;
    memcpy(&newCell, src, sizeof(double) * numScalars());
    src += numScalars();
    structure()->changeSimulationCell(newCell, false);
}

/******************************************************************************
* Parse the contents of the <relax> element in the job file.
******************************************************************************/
void StructureCellDOF::parseRelax(XML::Element relaxElement)
{
    DegreeOfFreedom::parseRelax(relaxElement);
    _scalingFactor = relaxElement.parseOptionalFloatParameterAttribute("scaling-factor", 0);
    FPString mode = relaxElement.parseOptionalStringParameterAttribute("relaxation-mode", "full");
    if(mode == "full") {
        _relaxationMode = FULL;
    }
    else if(mode == "hydrostatic") {
        _relaxationMode = HYDROSTATIC;
    }
    else if(mode == "constant-volume") {
        _relaxationMode = CONSTANT_VOLUME;
    }
    else {
        throw runtime_error(
            str(format("Invalid relaxation-mode attribute in line %1% of XML file: Unknown relaxation mode \"%2%\". "
                       "Possible values are \"full\", \"hydrostatic\" and \"constant-volume\"") %
                relaxElement.lineNumber() % mode));
    }
}

/******************************************************************************
* Lets the DOF write the forces on the cell to the given linear array.
******************************************************************************/
void StructureCellDOF::getCellForces(std::array<double, 6>& virial, std::vector<double>::iterator& g_iter)
{
    double natoms = structure()->numLocalAtoms();
    if(_relaxationMode == FULL) {
        // And for the cell dimension.
        // cell: [xx, yx, zx, xy, yy, zy, xz, yz, zz]
        *g_iter++ = -virial[0] / natoms;
        *g_iter++ = -virial[5] / natoms;
        *g_iter++ = -virial[4] / natoms;
        *g_iter++ = -virial[5] / natoms;
        *g_iter++ = -virial[1] / natoms;
        *g_iter++ = -virial[3] / natoms;
        *g_iter++ = -virial[4] / natoms;
        *g_iter++ = -virial[3] / natoms;
        *g_iter++ = -virial[2] / natoms;
    }
    else if(_relaxationMode == HYDROSTATIC) {
        // Actually the volume is not really constant using this
        // method, but the ase UnitCellFilter makes the same mistake...
        double trace = virial[0] + virial[1] + virial[2];
        *g_iter++ = -(trace / 3.0) / natoms;
        *g_iter++ = 0;
        *g_iter++ = 0;
        *g_iter++ = 0;
        *g_iter++ = -(trace / 3.0) / natoms;
        *g_iter++ = 0;
        *g_iter++ = 0;
        *g_iter++ = 0;
        *g_iter++ = -(trace / 3.0) / natoms;
    }
    else if(_relaxationMode == CONSTANT_VOLUME) {
        // Actually the volume is not really constant using this
        // method, but the ase UnitCellFilter makes the same mistake...
        double trace = virial[0] + virial[1] + virial[2];
        *g_iter++ = -(virial[0] - trace / 3.0) / natoms;
        *g_iter++ = -virial[5] / natoms;
        *g_iter++ = -virial[4] / natoms;
        *g_iter++ = -virial[5] / natoms;
        *g_iter++ = -(virial[1] - trace / 3.0) / natoms;
        *g_iter++ = -virial[3] / natoms;
        *g_iter++ = -virial[4] / natoms;
        *g_iter++ = -virial[3] / natoms;
        *g_iter++ = -(virial[2] - trace / 3.0) / natoms;
    }
    else {
        str(format("Invalid relaxation mode %1% in getCellForces") % _relaxationMode);
    }
}

/******************************************************************************
* Lets the DOF write its constraints information to the given linear arrays.
******************************************************************************/
void StructureCellDOF::getBoundConstraints(std::vector<Minimizer::BoundConstraints>::iterator& types,
                                           std::vector<double>::iterator& lowerBounds, std::vector<double>::iterator& upperBounds)
{
    if(_scalingFactor) {
        for(auto const& column : structure()->initialSimulationCell()) {
            for(auto const& value : column) {
                *types++ = Minimizer::BOTH_BOUNDS;
                // This equation also works on negative values
                *lowerBounds++ = value - _scalingFactor * fabs(value);
                *upperBounds++ = value + _scalingFactor * fabs(value);
            }
        }
    }
    else {
        // Use the parent function that does not set any bounds.
        DegreeOfFreedom::getBoundConstraints(types, lowerBounds, upperBounds);
    }
}
}
