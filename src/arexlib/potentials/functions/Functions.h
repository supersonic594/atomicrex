///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../../job/FitObject.h"
#include "../../util/NumericalIntegration.h"
#include "../../dof/DegreeOfFreedom.h"

namespace atomicrex {

/**
   @brief Base class for all one-dimensional functions that can be fitted.
   @details Optionally, a function can be normalized by multiplying
   it with a constant pre-factor, and a screening function can be imposed.
*/
class FunctionBase : public FitObject, public NumericalIntegration
{
public:
    enum NormalizationMode
    {
        /// Do not perform any normalization; use unmodified function.
        NONE,
        /// Spherical integral over [0,cutoff] is unity, \f$\int dx f(x) = 1\f$.
        SPHERICAL_INTEGRAL,
        /// Function is one at origin, \f$f(0) = 1\f$.
        UNITY_ORIGIN,
    };

public:
    /// Constructor.
    FunctionBase(const FPString& id, FitJob* job, const FPString& tag = FPString())
        : FitObject(id, job, tag), _normalizationMode(NONE), _prefactor(0), _cutoff(FLOATTYPE_MAX)
    {
    }

    /// Returns whether this function has a cutoff.
    bool hasCutoff() const { return cutoff() != FLOATTYPE_MAX; }

    /// Returns the cutoff of this function.
    /// The function will evaluate to flat zero for positions beyond this cutoff.
    double cutoff() const { return _cutoff; }

    /// Sets the cutoff of this function.
    /// The function will evaluate to flat zero for positions beyond this cutoff.
    void setCutoff(double cutoff) { _cutoff = cutoff; }

    /// Returns the constant prefactor the function is multiplied with.
    double prefactor() const { return _prefactor; }

    /// Evaluates the function at the given r and multiplies the value with the proper normalization factor.
    double evaluate(double r);

    /// Evaluates the function and its derivative at the given r and multiplies the values with the proper normalization factor.
    double evaluate(double r, double& deriv);

    /**
     @brief Write tabulated function to stream for visualization with Gnuplot.
     @param out[in]  output stream
     @param rmin[in] lower limit of range for input variable
     @param rmax[in] upper limit of range for input variable
     @param dr[in]   spacing of input variable
    */
    void writeTabulated(std::ostream& out, double rmin, double rmax, double dr);

    /// Returns the normalization mode set for this function.
    NormalizationMode normalizationMode() const { return _normalizationMode; }

    /// Specifies the normalization mode to be used for this function.
    void setNormalizationMode(NormalizationMode mode) { _normalizationMode = mode; }

    /// Normalizes the function by computing the constant prefactor.
    /// This function is called once every time the parameters of the function have changed.
    void computePrefactor();

    /// This function is called by the DOFs of this object each time their value changes.
    /// Setting the prefactor to zero will trigger its recalculation next time evaluateNormalized() is called.
    virtual void dofValueChanged(DegreeOfFreedom& dof) override
    {
        FitObject::dofValueChanged(dof);
        _prefactor = 0;
    }

    /// Returns the screening function applied to this function, or NULL.
    const std::shared_ptr<FunctionBase>& screeningFunction() const { return _screeningFunction; }

    /// Applies the given screening function to this function.
    /// This function becomes the owner of the screening function and will delete it when done.
    void setScreeningFunction(const std::shared_ptr<FunctionBase>& func)
    {
        BOOST_ASSERT(!_screeningFunction);
        _screeningFunction = func;
        registerSubObject(func.get());
    }

    /// Makes sure that the analytical first derivative is correct by comparing
    /// its value at various point to the numeric first derivative.
    void verifyDerivative();

    /// Performs a validation of the analytical derivative by comparing it to a numerically computed derivative at the given r.
    /// Throws an exception if the analytical derivative does not match.
    void checkDerivativeNumerically(double r);

    /// Compute the numerical derivative at the given r using the 5-point rule (\f$-h\f$, \f$-h/2\f$, \f$0\f$, \f$+h/2\f$,
    /// \f$+h\f$).
    /// Note that the value at the central point is not used.
    double centralDerivative(double r, double h, double& abserr_trunc, double& abserr_round);

public:
    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Parses an XML element and creates a function object out of it.
    static std::shared_ptr<FunctionBase> createAndParse(XML::Element functionElement, const FPString& id, FitJob* job);

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName);

protected:
    /// Evaluates the function at the given r.
    /// This does not include the constant pre-factor, or the optional screening function.
    virtual double evaluateInternal(double r) override = 0;

    /// Evaluates the function and its derivative at the given r.
    /// This does not include the constant pre-factor, or the optional screening function.
    virtual double evaluateInternal(double r, double& deriv) = 0;

    /// Returns the list of DOFs that should be exported as part of the
    /// <fit-dof> element. By default, all DOFs of the function are exported
    /// in this way. Subclasses may override this method to implement custom
    /// output formats for certain DOFs.
    virtual std::vector<DegreeOfFreedom*> DOFToExport() { return DOF(); }

protected:
    /// The function cutoff.
    double _cutoff;

    /// The constant prefactor used to normalize the function.
    double _prefactor;

    /// Controls the type of normalization applied to the function.
    NormalizationMode _normalizationMode;

    /// Optional screening function.
    std::shared_ptr<FunctionBase> _screeningFunction;
};

/************************************************************
 * exp[1/(r-rc)]
 ************************************************************/
class FunctionExpA : public FunctionBase
{
public:
    FunctionExpA(const FPString& id, FitJob* job) : FunctionBase(id, job, "exp") {}

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;
};

/************************************************************
 * Gaussian function
 ************************************************************/
class FunctionGaussian : public FunctionBase
{
public:
    FunctionGaussian(const FPString& id, FitJob* job);

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName) override;

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;

private:
    ScalarDOF _prefactor, _mu, _eta;
};

/************************************************************
 * exp[sgn(n)*alpha/(1-(r/rc)^n)]
 ************************************************************/
class FunctionExpB : public FunctionBase
{
public:
    FunctionExpB(const FPString& id, FitJob* job);

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName) override;

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;

private:
    ScalarDOF _alpha, _exponent, _rc;
};

/************************************************************
 * ExpB * gaussian
 ************************************************************/
class FunctionExpGaussian : public FunctionBase
{
public:
    FunctionExpGaussian(const FPString& id, FitJob* job);

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName) override;

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;

private:
    ScalarDOF _stddev, _alpha, _exponent;
};

/************************************************************
 * Standard Morse potential (type A).
 *
 * D0 * [exp(-2*alpha*(r-r0)) - 2*exp(-alpha*(r-r0))]
 ************************************************************/
class FunctionMorseA : public FunctionBase
{
public:
    /// Constructor
    FunctionMorseA(const FPString& id, FitJob* job);

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName) override;

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;

private:
    ScalarDOF _D0, _alpha, _r0;
};

/************************************************************
 * Modified Morse potential - type B.
 *
 * [D0 / (S-1) * exp(-beta sqrt(2 S) (r - r0)] - [D0 S / (S-1) * exp(-beta sqrt(2 S) (r - r0))] + delta
 ************************************************************/
class FunctionMorseB : public FunctionBase
{
public:
    FunctionMorseB(const FPString& id, FitJob* job);

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName) override;

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;

private:
    ScalarDOF _D0, _r0, _S, _beta, _delta;
};

/************************************************************
 * Modified Morse potential - type C.
 ************************************************************/
class FunctionMorseC : public FunctionBase
{
public:
    FunctionMorseC(const FPString& id, FitJob* job);

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName) override;

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;

private:
    ScalarDOF _A, _B, _mu, _lambda, _delta;
};

/************************************************************
 * Polynomial
 ************************************************************/
class FunctionPoly : public FunctionBase
{
public:
    FunctionPoly(const FPString& id, FitJob* job) : FunctionBase(id, job, "poly"), _isPrepared(false) {}

    double integrateSpherical(double a, double b) override;

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName) override;

    /// This callback function is called by the DOFs of this function each time when their values changes.
    virtual void dofValueChanged(DegreeOfFreedom& dof) override
    {
        FunctionBase::dofValueChanged(dof);
        if(&dof != &_nodes.back().coeff) {
            _isPrepared = false;
        }
    }

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;

    void imposeBoundaryConditions();

    /// Do not export the nodes as part of the standard <fit-dof> element. Instead, generateXMLDefinition()
    /// will format them in a different way.
    virtual std::vector<DegreeOfFreedom*> DOFToExport() override { return std::vector<DegreeOfFreedom*>(); }

private:
    struct PolyNode {
        ScalarDOF coeff;
        int exponent;
    };

    /// Indicates that the boundary conditions have been imposed.
    bool _isPrepared;

    std::vector<PolyNode> _nodes;

    int _degree;
};

/************************************************************
 * Cubic spline
 ************************************************************/
class FunctionSpline : public FunctionBase
{
public:
    /// Constructor.
    FunctionSpline(const FPString& id, FitJob* job) : FunctionBase(id, job, "spline"), _isPrepared(false) {}

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName) override;

    /// This callback function is called by the DOFs of this function each time their value changes.
    virtual void dofValueChanged(DegreeOfFreedom& dof) override
    {
        FunctionBase::dofValueChanged(dof);
        _isPrepared = false;
    }

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;
    void prepareSpline();

    /// Do not export the nodes as part of the standard <fit-dof> element. Instead, generateXMLDefinition()
    /// will format them in a different way.
    virtual std::vector<DegreeOfFreedom*> DOFToExport() override { return std::vector<DegreeOfFreedom*>(); }

private:
    struct SplineNode {
        double x, y2;
        ScalarDOF y;

        bool operator<(const SplineNode& other) const { return x < other.x; }  // Used for node sorting.
    };

    /// Indicates that the second derivatives have been calculated.
    bool _isPrepared;

    // Vector of spline nodes.
    std::vector<SplineNode> _nodes;

    // Derivatives at left and right most nodes.
    ScalarDOF _deriv0, _derivN;
};

/************************************************************
 * Function of function of 1-x
 ************************************************************/
class FunctionInverseArgument : public FunctionBase
{
public:
    /// Constructor.
    FunctionInverseArgument(const FPString& id, FitJob* job, const std::shared_ptr<FunctionBase>& func)
        : FunctionBase(id, job, "functionInverseArgument"), _innerFunction(func)
    {
    }

    /// Returns the inner function.
    const std::shared_ptr<FunctionBase>& innerFunction() const { return _innerFunction; }

protected:
    double evaluateInternal(double r) override { return _innerFunction->evaluate(1.0 - r); }

    double evaluateInternal(double r, double& deriv) override
    {
        double h = _innerFunction->evaluate(1.0 - r, deriv);
        deriv = -deriv;
        return h;
    }

private:
    std::shared_ptr<FunctionBase> _innerFunction;
};

/************************************************************
 * Sum of two or more functions.
 ************************************************************/
class FunctionSum : public FunctionBase
{
public:
    /// Constructor.
    FunctionSum(const FPString& id, FitJob* job) : FunctionBase(id, job, "functionSum") {}

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

protected:
    double evaluateInternal(double r) override
    {
        double s = 0;
        for(const auto& f : _functions) s += f->evaluate(r);
        return s;
    }

    double evaluateInternal(double r, double& deriv) override
    {
        double s = 0;
        deriv = 0;
        for(const auto& f : _functions) {
            double d;
            s += f->evaluate(r, d);
            deriv += d;
        }
        return s;
    }

private:
    std::vector<std::shared_ptr<FunctionBase>> _functions;
};

/************************************************************
 * A constant function, with the constant being a degree of freedom.
 ************************************************************/
class FunctionConstant : public FunctionBase
{
public:
    /// Constructor.
    FunctionConstant(const FPString& id, FitJob* job);

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

    /// Produces an XML representation of the function's current parameters and DOFs.
    virtual XML::OElement generateXMLDefinition(const FPString& elementName) override;

protected:
    double evaluateInternal(double r) override;
    double evaluateInternal(double r, double& deriv) override;

private:
    ScalarDOF _const;
};

/************************************************************
 * Product of two functions.
 ************************************************************/
class FunctionProduct : public FunctionBase
{
public:
    /// Constructor.
    FunctionProduct(const FPString& id, FitJob* job) : FunctionBase(id, job, "functionProduct") {}

    /// Parses function parameters from the given XML element.
    virtual void parse(XML::Element functionElement) override;

protected:
    double evaluateInternal(double r) override { return _function1->evaluate(r) * _function2->evaluate(r); }

    double evaluateInternal(double r, double& deriv) override
    {
        double p1, p2;
        double d1, d2;
        p1 = _function1->evaluate(r, d1);
        p2 = _function2->evaluate(r, d2);
        deriv = p1 * d2 + p2 * d1;
        return p1 * p2;
    }

private:
    std::shared_ptr<FunctionBase> _function1;
    std::shared_ptr<FunctionBase> _function2;
};

}  // End of namespace
