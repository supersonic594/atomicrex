///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "Functions.h"
#include "../../job/FitJob.h"
#include "../../util/xml/XMLUtilities.h"
#include "ParsedFunction.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/// Our signum function.
inline int sign(double x) { return x < 0.0 ? -1 : 1; }

/*****************************************************************************
* Parses an XML element and creates a function object out of it.
******************************************************************************/
std::shared_ptr<FunctionBase> FunctionBase::createAndParse(XML::Element functionElement, const FPString& id, FitJob* job)
{
    std::shared_ptr<FunctionBase> func;

    // Create an instance of the selected function class.
    if(functionElement.tagEquals("exp-A"))
        func = std::make_shared<FunctionExpA>(id, job);
    else if(functionElement.tagEquals("exp-B"))
        func = std::make_shared<FunctionExpB>(id, job);
    else if(functionElement.tagEquals("exp-gaussian"))
        func = std::make_shared<FunctionExpGaussian>(id, job);
    else if(functionElement.tagEquals("gaussian"))
        func = std::make_shared<FunctionGaussian>(id, job);
    else if(functionElement.tagEquals("poly"))
        func = std::make_shared<FunctionPoly>(id, job);
    else if(functionElement.tagEquals("spline"))
        func = std::make_shared<FunctionSpline>(id, job);
    else if(functionElement.tagEquals("morse-A"))
        func = std::make_shared<FunctionMorseA>(id, job);
    else if(functionElement.tagEquals("morse-B"))
        func = std::make_shared<FunctionMorseB>(id, job);
    else if(functionElement.tagEquals("morse-C"))
        func = std::make_shared<FunctionMorseC>(id, job);
    else if(functionElement.tagEquals("constant"))
        func = std::make_shared<FunctionConstant>(id, job);
    else if(functionElement.tagEquals("sum"))
        func = std::make_shared<FunctionSum>(id, job);
    else if(functionElement.tagEquals("product"))
        func = std::make_shared<FunctionProduct>(id, job);
    else if(functionElement.tagEquals("user-function"))
        func = std::make_shared<ParsedFunction>(id, job);
    else
        throw runtime_error(str(format("Unknown function type <%1%> in line %2% of XML file.") % functionElement.tag() %
                                functionElement.lineNumber()));

    // Parse user-defined ID.
    if(id.empty()) func->setId(functionElement.parseOptionalStringParameterAttribute("id"));

    // Let the function object parse the rest.
    func->parse(functionElement);

    return func;
}

/*****************************************************************************
* Parses general function parameters in the XML element in the job file.
* N.B.: The first part is virtually identical to Potential::parse().
******************************************************************************/
void FunctionBase::parse(XML::Element functionElement)
{
    // Call base class.
    FitObject::parse(functionElement);

    // Parse <fit-dof> element.
    XML::Element fitDOFElement = functionElement.firstChildElement("fit-dof");
    if(fitDOFElement) {
        for(XML::Element dofElement = fitDOFElement.firstChildElement(); dofElement; dofElement = dofElement.nextSibling()) {
            // Lookup degree of freedom.
            FPString dofId = dofElement.tag();
            FPString dofTag = dofElement.parseOptionalStringParameterAttribute("tag");
            DegreeOfFreedom* dof = DOFById(dofId, dofTag);
            if(!dof)
                throw runtime_error(
                    str(format("Invalid element in line %1% of XML file: Non-existent degree of freedom \"%2%\" (tag=%3%).") %
                        dofElement.lineNumber() % dofId % (dofTag.empty() ? FPString("<empty>") : dofTag)));

            // Let the DOF parse the rest.
            dof->parseFit(dofElement);
        }
    }

    // Parse optional screening function.
    XML::Element screeningElement = functionElement.firstChildElement("screening");
    if(screeningElement) {
        XML::Element funcElement = screeningElement.firstChildElement();
        if(!funcElement)
            throw runtime_error(str(format("<screening> element in line %1% of XML file must contain a function element.") %
                                    screeningElement.lineNumber()));
        setScreeningFunction(createAndParse(funcElement, "Screening", job()));

        if(screeningFunction()->cutoff() !=
           functionElement.parseOptionalFloatParameterAttribute("cutoff", screeningFunction()->cutoff()))
            throw runtime_error(str(
                format("Cutoff of screening function differs from cutoff of function being screened at line %1% of job file.") %
                screeningElement.lineNumber()));

        // Adopt cutoff radius of screening function.
        _cutoff = screeningFunction()->cutoff();
    }
    else {
        // Parse cutoff radius.
        _cutoff = functionElement.parseOptionalFloatParameterElement("cutoff", _cutoff);
        if(_cutoff <= 0)
            throw runtime_error(
                str(format("Function cutoff must be positive in line %1% of XML file.") % functionElement.lineNumber()));
    }
}

/*****************************************************************************
* Produces an XML representation of the function's current parameter values
* and DOFs that can be used as input in a subsequent fit job.
******************************************************************************/
XML::OElement FunctionBase::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement(elementName.c_str());

    if(screeningFunction()) {
        // Generate <screening> sub-element.
        XML::OElement screeningElement("screening");
        XML::OElement screeningFuncElement = screeningFunction()->generateXMLDefinition(screeningFunction()->tag());
        screeningElement.appendChild(screeningFuncElement);
        funcElement.appendChild(screeningElement);
    }
    else {
        // Generate <cutoff> element.
        if(hasCutoff()) funcElement.createFloatParameterElement("cutoff", cutoff());
    }

    // Generate <fit-dof> element.
    XML::OElement fitdofElement("fit-dof");
    for(DegreeOfFreedom* dof : DOFToExport()) {
        XML::OElement dofElement = dof->generateXMLFitDefinition();
        if(dof->fitEnabled() || dofElement.attributeCount() > 1) fitdofElement.appendChild(dofElement);
    }
    if(fitdofElement.firstChildElement()) funcElement.appendChild(fitdofElement);

    return funcElement;
}

/*****************************************************************************
* Write tabulated function to stream for visualization with Gnuplot.
******************************************************************************/
void FunctionBase::writeTabulated(ostream& out, double rmin, double rmax, double dr)
{
    out << "# 1st col: index" << endl;
    out << "# 2nd col: x" << endl;
    out << "# 3rd col: y" << endl;
    out << "# 4th col: derivative" << endl;
    out << endl;
    int nr = int((rmax - rmin) / dr);
    for(int i = 0; i <= nr; ++i) {
        double r = dr * i + rmin;
        double df;
        double f = evaluate(r, df);
        out << " " << i << " " << r << " " << f << " " << df << endl;
    }
}

/************************************************************
* Evaluates the function at the given r and multiplies the
* value with the proper normalization factor.
************************************************************/
double FunctionBase::evaluate(double r)
{
    // Handle case of unmodified function first.
    if(!screeningFunction() && normalizationMode() == NONE) return evaluateInternal(r);

    double screeningFactor = 1;
    if(screeningFunction()) screeningFactor = screeningFunction()->evaluate(r);

    if(normalizationMode() == NONE)
        return screeningFactor * evaluateInternal(r);
    else if(prefactor() == 0)
        computePrefactor();

    return prefactor() * screeningFactor * evaluateInternal(r);
}

/************************************************************
* Evaluates the function and its derivative at the given r and
* multiplies the values with the proper normalization factor.
************************************************************/
double FunctionBase::evaluate(double r, double& deriv)
{
    // Handle case of unmodified function first.
    if(!screeningFunction() && normalizationMode() == NONE) return evaluateInternal(r, deriv);

    double screeningFactor = 1, screeningFactorDeriv = 0;
    if(screeningFunction()) screeningFactor = screeningFunction()->evaluate(r, screeningFactorDeriv);

    if(normalizationMode() == NONE) {
        double v = evaluateInternal(r, deriv);
        deriv = deriv * screeningFactor + screeningFactorDeriv * v;
        return v * screeningFactor;
    }
    else if(prefactor() == 0)
        computePrefactor();

    double v = evaluateInternal(r, deriv);
    deriv = prefactor() * (deriv * screeningFactor + screeningFactorDeriv * v);
    return prefactor() * v * screeningFactor;
}

/************************************************************
* Compute normalization prefactor.
************************************************************/
void FunctionBase::computePrefactor()
{
    switch(normalizationMode()) {
        case NONE: _prefactor = 1; break;
        case SPHERICAL_INTEGRAL: {
            if(!hasCutoff())
                throw runtime_error(str(format("Cannot normalize function %1%, which doesn't have a cutoff.") % id()));
            // Normalize such that integral of function over sphere is 1 (or actually just constant).
            double sphIntegral = integrateSpherical(0, cutoff());
            if(sphIntegral <= 0) throw runtime_error(str(format("Spherical integral over function %1% non-positive.") % id()));
            _prefactor = 1.0 / sphIntegral;
        } break;
        case UNITY_ORIGIN: {
            if(!hasCutoff())
                throw runtime_error(str(format("Cannot normalize function %1%, which doesn't have a cutoff.") % id()));
            // Normalize such that function is 1 if argument is 0.
            double zeroVal = evaluateInternal(0);
            if(zeroVal <= 0) throw runtime_error(str(format("Function %1% is non-positive at origin.") % id()));
            _prefactor = 1.0 / zeroVal;
        } break;
    }
}

/**************************************************************************************
* Makes sure that the analytical first derivative is correct by comparing
* its value at various point to the numeric first derivative.
***************************************************************************************/
void FunctionBase::verifyDerivative()
{
    double dc = hasCutoff() ? (cutoff() / 20.0) : 1.0;
    for(int i = 1; i <= 20; i++) {
        checkDerivativeNumerically(i * dc);
    }
}

/**************************************************************************************
* Performs a validation of the analytical derivative by comparing it to a numerically
* computed derivative at the given r. Throws an exception if the analytical derivative
* does not match.
***************************************************************************************/
void FunctionBase::checkDerivativeNumerically(double r)
{
    // The predefined magnitude of the displacement applied to each atomic position:
    double h = 1e-4;

    double round, trunc;
    double numericalDerivative = centralDerivative(r, h, round, trunc);
    double numericalDerivativeError = round + trunc;

    if(round < trunc && (round > 0 && trunc > 0)) {
        // Compute an optimized stepsize to minimize the total error,
        // using the scaling of the truncation error (O(h^2)) and
        // rounding error (O(1/h)).
        double h_opt = h * pow(round / (2.0 * trunc), 1.0 / 3.0);
        double round_opt, trunc_opt;
        double r_opt = centralDerivative(r, h_opt, round_opt, trunc_opt);
        double error_opt = round_opt + trunc_opt;

        // Check that the new error is smaller, and that the new derivative
        // is consistent with the error bounds of the original estimate.
        if(error_opt < numericalDerivativeError && fabs(r_opt - numericalDerivative) < 4.0 * numericalDerivativeError) {
            numericalDerivative = r_opt;
            numericalDerivativeError = error_opt;
        }
    }

    double analyticalDerivative;
    double value = evaluate(r, analyticalDerivative);
    BOOST_ASSERT(fabs(value - evaluate(r)) <= 1e-10);
    double relativeDeviation = (analyticalDerivative - numericalDerivative) / analyticalDerivative;

    if(fabs(numericalDerivative - analyticalDerivative) > numericalDerivativeError) {
        MsgLogger(medium) << "Function " << id() << " (r=" << r << ") Numerical derivative: " << numericalDerivative
                          << " (err: " << numericalDerivativeError << ")   Analytical derivative: " << analyticalDerivative
                          << "   Deviation: " << relativeDeviation << endl;

        throw runtime_error(
            str(format("Numerical derivative deviates too much from the analytical energy gradient computed by function \"%1%\"") %
                id()));
    }
}

/**************************************************************************************
* Computes the numerical derivative at the given r using the 5-point rule (-h, -h/2, 0, +h/2, +h).
* Note that the value at the central point is not used.
***************************************************************************************/
double FunctionBase::centralDerivative(double r, double h, double& abserr_trunc, double& abserr_round)
{
    // Compute the error using the difference between the 5-point and
    // the 3-point rule (-h,0,+h). Again the central point is not
    // used.
    double fm1 = evaluate(r - h);
    double fp1 = evaluate(r + h);

    double fmh = evaluate(r - (h / 2));
    double fph = evaluate(r + (h / 2));

    double r3 = 0.5 * (fp1 - fm1);
    double r5 = (4.0 / 3.0) * (fph - fmh) - (1.0 / 3.0) * r3;

    double e3 = (fabs(fp1) + fabs(fm1)) * 2.2204460492503131e-16;
    double e5 = 2.0 * (fabs(fph) + fabs(fmh)) * 2.2204460492503131e-16 + e3;

    // The truncation error in the r5 approximation itself is O(h^4).
    // However, for safety, we estimate the error from r5-r3, which is
    // O(h^2).  By scaling h we will minimize this estimated error, not
    // the actual truncation error in r5.

    double result = r5 / h;
    abserr_trunc = fabs((r5 - r3) / h);  // Estimated truncation error O(h^2)
    abserr_round = fabs(e5 / h);         // Rounding error (cancellations)
    return result;
}

//========================================================================
//========================================================================
//========================================================================

/************************************************************
* Evaluate function --> exp[1/(r-rc)]
************************************************************/
double FunctionExpA::evaluateInternal(double r)
{
    BOOST_ASSERT(hasCutoff());
    if(r < _cutoff) return exp(1.0 / (r - cutoff()));
    return 0;
}

/************************************************************
* Evaluate both the function and its derivative.
************************************************************/
double FunctionExpA::evaluateInternal(double r, double& deriv)
{
    if(r < cutoff()) {
        double dr = r - cutoff();
        double f = exp(1.0 / dr);
        deriv = -1.0 / square(dr) * f;
        return f;
    }
    deriv = 0;
    return 0;
}

//========================================================================
//========================================================================
//========================================================================

/******************************************************************************
* Constructor for sigma function of ExpB type.
******************************************************************************/
FunctionExpB::FunctionExpB(const FPString& id, FitJob* job)
    : FunctionBase(id, job, "nexp"), _alpha("alpha"), _exponent("exponent"), _rc("rc")
{
    registerDOF(&_alpha);
    registerDOF(&_exponent);
    registerDOF(&_rc);
}

/************************************************************************
* Evaluate function --> exp[-sgn(n)*alpha/(1-((r-b)/(rc-b))^n)]
************************************************************************/
double FunctionExpB::evaluateInternal(double r)
{
    if(r <= _rc)
        return 1;
    else if(r < cutoff()) {
        double x = (r - _rc) / (cutoff() - _rc);
        return exp(-sign(_exponent) * _alpha / (1.0 - pow(x, _exponent)));
    }
    else
        return 0;
}

/**********************************************************************************
* Evaluate derivative of function --> exp[-sgn(n)*alpha/(1-((r-b)/(rc-b))^n)]
**********************************************************************************/
double FunctionExpB::evaluateInternal(double r, double& deriv)
{
    if(r <= _rc) {
        deriv = 0;
        return 1;
    }
    else if(r < cutoff()) {
        double dc = 1 / (cutoff() - _rc);
        double x = (r - _rc) * dc;
        double xm = pow(x, _exponent - 1);
        double par = -sign(_exponent) * _alpha;
        double denom = 1. / (1. - xm * x);
        double f = exp(par * denom);
        deriv = _exponent * dc * xm * par * square(denom) * f;
        return f;
    }
    else {
        deriv = 0;
        return 0;
    }
}

/******************************************************************************
* Parses concentration function parameters in the XML element in the job file.
******************************************************************************/
void FunctionExpB::parse(XML::Element sigElement)
{
    FunctionBase::parse(sigElement);

    // Enumerator in exponential.
    _alpha = sigElement.parseFloatParameterElement("alpha");
    if(_alpha <= 0)
        throw runtime_error(
            str(format("Alpha (value=%1%) has to be positive in line %2% of XML file.") % _alpha % sigElement.lineNumber()));

    // Exponent under the fraction.
    _exponent = sigElement.parseFloatParameterElement("exponent");

    // Inner cutoff.
    _rc = sigElement.parseFloatParameterElement("rc");
    if(_rc < 0)
        throw runtime_error(
            str(format("Rc (value=%1%) has to be positive in line %2% of XML file.") % _rc % sigElement.lineNumber()));
}

/************************************************************************
* Produces an XML representation of the function's current parameter values.
************************************************************************/
XML::OElement FunctionExpB::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement = FunctionBase::generateXMLDefinition(elementName);
    funcElement.appendChild(_alpha.generateXMLValueDefinition());
    funcElement.appendChild(_exponent.generateXMLValueDefinition());
    funcElement.appendChild(_rc.generateXMLValueDefinition());
    return funcElement;
}

//========================================================================
//========================================================================
//========================================================================

/******************************************************************************
* Constructor.
******************************************************************************/
FunctionExpGaussian::FunctionExpGaussian(const FPString& id, FitJob* job)
    : FunctionBase(id, job, "exp-gaussian"), _alpha("alpha"), _exponent("exponent"), _stddev("stddev")
{
    registerDOF(&_alpha);
    registerDOF(&_exponent);
    registerDOF(&_stddev);
}

/************************************************************************
* Evaluate sigma function --> ExpB * gaussian
************************************************************************/
double FunctionExpGaussian::evaluateInternal(double r)
{
    if(r < cutoff()) {
        // Gaussian function.
        double gaussian = 1.0 / (FLOATTYPE_SQRTTWOPI * _stddev) * exp(-0.5 * square(r / _stddev));
        // Cutoff function.
        double fc = exp(-sign(_exponent) * _alpha / (1.0 - pow(r / cutoff(), _exponent)));
        return fc * gaussian;
    }
    else
        return 0;
}

/**********************************************************************************
* Evaluate both function and its derivative.
**********************************************************************************/
double FunctionExpGaussian::evaluateInternal(double r, double& deriv)
{
    if(r < cutoff()) {
        // Gaussian function.
        double gaussian = 1.0 / (FLOATTYPE_SQRTTWOPI * _stddev) * exp(-0.5 * square(r / _stddev));
        double dgaussian = -r / square((double)_stddev) * gaussian;
        // Cutoff function.
        double drc = 1. / cutoff();
        double x = r * drc;
        double xm = pow(x, _exponent - 1);
        double par = -sign(_exponent) * _alpha;
        double denom = 1. / (1. - xm * x);
        double fc = exp(par * denom);
        double dfc = _exponent * drc * xm * par * square(denom) * fc;
        // Full function.
        if(r == 0) {
            deriv = 0;
            return gaussian;
        }
        deriv = fc * dgaussian + dfc * gaussian;
        return fc * gaussian;
    }
    else {
        deriv = 0;
        return 0;
    }
}

/******************************************************************************
* Parses concentration function parameters in the XML element in the job file.
******************************************************************************/
void FunctionExpGaussian::parse(XML::Element sigElement)
{
    FunctionBase::parse(sigElement);

    // Enumerator in exponential.
    _alpha = sigElement.parseFloatParameterElement("alpha");
    if(_alpha <= 0)
        throw runtime_error(
            str(format("Alpha (value=%1%) has to be positive in line %2% of XML file.") % _alpha % sigElement.lineNumber()));
    // Coefficient of x.
    _exponent = sigElement.parseFloatParameterElement("exponent");

    // Standard deviation of Gaussian.
    _stddev = sigElement.parseFloatParameterElement("stddev");
    if(_stddev <= 0)
        throw runtime_error(
            str(format("Stddev (value=%1%) has to be positive in line %2% of job file.") % _stddev % sigElement.lineNumber()));
}

/************************************************************************
* Produces an XML representation of the function's current parameter values.
************************************************************************/
XML::OElement FunctionExpGaussian::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement = FunctionBase::generateXMLDefinition(elementName);
    funcElement.appendChild(_alpha.generateXMLValueDefinition());
    funcElement.appendChild(_exponent.generateXMLValueDefinition());
    funcElement.appendChild(_stddev.generateXMLValueDefinition());
    return funcElement;
}

//========================================================================
//========================================================================
//========================================================================

/******************************************************************************
* Constructor.
******************************************************************************/
FunctionGaussian::FunctionGaussian(const FPString& id, FitJob* job)
        : FunctionBase(id, job, "gaussian"),
        _prefactor("prefactor"), _mu("mu"), _eta("eta")
{
    registerDOF(&_prefactor);
    registerDOF(&_mu);
    registerDOF(&_eta);
}

/************************************************************************
* Evaluate Gaussian function
************************************************************************/
double FunctionGaussian::evaluateInternal(double r)
{
    if(r < cutoff()) {
        // Gaussian function.
        return _prefactor * exp( -_eta * square(r - _mu) );
    }
    else return 0;
}

/**********************************************************************************
* Evaluate both function and its derivative.
**********************************************************************************/
double FunctionGaussian::evaluateInternal(double r, double& deriv)
{
    if(r < cutoff()) {
        // Gaussian function.
        double gaussian = _prefactor * exp( -_eta * square(r - _mu) );
        deriv = _eta * -2.0 * gaussian * r;
        return gaussian;
    }
    else {
        deriv = 0;
        return 0;
    }
}

/******************************************************************************
* Parses function parameters in the XML element in the job file.
******************************************************************************/
void FunctionGaussian::parse(XML::Element sigElement)
{
    FunctionBase::parse(sigElement);

    _prefactor = sigElement.parseFloatParameterElement("prefactor");
    _mu = sigElement.parseFloatParameterElement("mu");
    _eta = sigElement.parseFloatParameterElement("eta");
}

/************************************************************************
* Produces an XML representation of the function's current parameter values.
************************************************************************/
XML::OElement FunctionGaussian::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement = FunctionBase::generateXMLDefinition(elementName);
    funcElement.appendChild(_prefactor.generateXMLValueDefinition());
    funcElement.appendChild(_mu.generateXMLValueDefinition());
    funcElement.appendChild(_eta.generateXMLValueDefinition());
    return funcElement;
}

//========================================================================
//========================================================================
//========================================================================

/******************************************************************************
* Constructor for the standard Morse function (type A).
******************************************************************************/
FunctionMorseA::FunctionMorseA(const FPString& id, FitJob* job)
    : FunctionBase(id, job, "morse-A"), _D0("D0"), _alpha("alpha"), _r0("r0")
{
    registerDOF(&_D0);
    registerDOF(&_alpha);
    registerDOF(&_r0);
}

/************************************************************************
* Evaluate function --> D0 * [exp(-2*alpha*(r-r0)) - 2*exp(-alpha*(r-r0))]
************************************************************************/
double FunctionMorseA::evaluateInternal(double r)
{
    if(r < cutoff())
        return _D0 * (exp(-2.0 * _alpha * (r - _r0)) - 2.0 * exp(-_alpha * (r - _r0)));
    else
        return 0;
}

/**********************************************************************************
* Evaluate derivative of function.
**********************************************************************************/
double FunctionMorseA::evaluateInternal(double r, double& deriv)
{
    if(r < cutoff()) {
        double e = exp(_alpha * (_r0 - r));
        deriv = -2.0 * _alpha * _D0 * e * (e - 1.0);
        return _D0 * (exp(-2.0 * _alpha * (r - _r0)) - 2.0 * e);
    }
    else {
        deriv = 0.0;
        return 0.0;
    }
}

/******************************************************************************
* Parses concentration function parameters in the XML element in the job file.
******************************************************************************/
void FunctionMorseA::parse(XML::Element sigElement)
{
    FunctionBase::parse(sigElement);

    _D0 = sigElement.parseFloatParameterElement("D0");
    _alpha = sigElement.parseFloatParameterElement("alpha");
    _r0 = sigElement.parseFloatParameterElement("r0");
}

/************************************************************************
* Produces an XML representation of the function's current parameter values.
************************************************************************/
XML::OElement FunctionMorseA::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement = FunctionBase::generateXMLDefinition(elementName);
    funcElement.appendChild(_D0.generateXMLValueDefinition());
    funcElement.appendChild(_alpha.generateXMLValueDefinition());
    funcElement.appendChild(_r0.generateXMLValueDefinition());
    return funcElement;
}

/*****************************************************************************
* Constructor for Morse potential of type B.
******************************************************************************/
FunctionMorseB::FunctionMorseB(const FPString& id, FitJob* job)
    : FunctionBase(id, job, "morse-B"), _D0("D0"), _r0("r0"), _S("S"), _beta("beta"), _delta("delta")
{
    registerDOF(&_D0);
    registerDOF(&_r0);
    registerDOF(&_S);
    registerDOF(&_beta);
    registerDOF(&_delta);
}

/*****************************************************************************
* Evaluate pair potential.
******************************************************************************/
double FunctionMorseB::evaluateInternal(double r)
{
    double VR, VA, dr, dS;
    dr = r - _r0;
    dS = 1 / (_S - 1);
    VR = _D0 * dS * exp(-_beta * sqrt(2. * _S) * dr);
    VA = _D0 * _S * dS * exp(-_beta * sqrt(2. / _S) * dr);
    return VR - VA + _delta;
}

/*****************************************************************************
* Evaluate derivative of pair potential.
******************************************************************************/
double FunctionMorseB::evaluateInternal(double r, double& deriv)
{
    double VR, VA, dVR, dVA, expPrefR, expPrefA, dr, dS;
    dr = r - _r0;
    dS = 1 / (_S - 1);
    expPrefR = -_beta * sqrt(2. * _S);
    expPrefA = -_beta * sqrt(2. / _S);
    VR = _D0 * dS * exp(expPrefR * dr);
    VA = _S * _D0 * dS * exp(expPrefA * dr);
    dVR = expPrefR * VR;
    dVA = expPrefA * VA;
    deriv = dVR - dVA;
    return VR - VA + _delta;
}

/******************************************************************************
* Parse parameters in the XML element in the job file.
******************************************************************************/
void FunctionMorseB::parse(XML::Element element)
{
    // Call base class.
    FunctionBase::parse(element);

    // Parameters of Morse potential.
    _D0 = element.parseFloatParameterElement("D0");
    _r0 = element.parseFloatParameterElement("r0");
    _S = element.parseFloatParameterElement("S");
    _beta = element.parseFloatParameterElement("beta");
    _delta = element.parseFloatParameterElement("delta");
}

/************************************************************************
* Produces an XML representation of the function's current parameter values.
************************************************************************/
XML::OElement FunctionMorseB::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement = FunctionBase::generateXMLDefinition(elementName);
    funcElement.appendChild(_D0.generateXMLValueDefinition());
    funcElement.appendChild(_r0.generateXMLValueDefinition());
    funcElement.appendChild(_S.generateXMLValueDefinition());
    funcElement.appendChild(_beta.generateXMLValueDefinition());
    funcElement.appendChild(_delta.generateXMLValueDefinition());
    return funcElement;
}

//========================================================================
//========================================================================
//========================================================================

/*****************************************************************************
* Constructor for Morse potential of type C.
******************************************************************************/
FunctionMorseC::FunctionMorseC(const FPString& id, FitJob* job)
    : FunctionBase(id, job, "morse-C"), _A("A"), _B("B"), _mu("mu"), _lambda("lambda"), _delta("delta")
{
    registerDOF(&_A);
    registerDOF(&_B);
    registerDOF(&_mu);
    registerDOF(&_lambda);
    registerDOF(&_delta);
}

/*****************************************************************************
* Evaluate pair potential.
******************************************************************************/
double FunctionMorseC::evaluateInternal(double r)
{
    double VR, VA;
    VR = _A * exp(-_lambda * r);
    VA = _B * exp(-_mu * r);
    return VR - VA + _delta;
}

/*****************************************************************************
* Evaluate pair potential and its derivative.
******************************************************************************/
double FunctionMorseC::evaluateInternal(double r, double& deriv)
{
    double VR, VA, dVR, dVA;
    VR = _A * exp(-_lambda * r);
    VA = _B * exp(-_mu * r);
    dVR = -_lambda * VR;
    dVA = -_mu * VA;
    deriv = dVR - dVA;
    return VR - VA + _delta;
}

/******************************************************************************
* Parse parameters in the XML element in the job file.
******************************************************************************/
void FunctionMorseC::parse(XML::Element element)
{
    // Call base class.
    FunctionBase::parse(element);

    // Parameters of Morse potential.
    _A = element.parseFloatParameterElement("A");
    _B = element.parseFloatParameterElement("B");
    _mu = element.parseFloatParameterElement("mu");
    _lambda = element.parseFloatParameterElement("lambda");
    _delta = element.parseFloatParameterElement("delta");
}

/************************************************************************
* Produces an XML representation of the function's current parameter values.
************************************************************************/
XML::OElement FunctionMorseC::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement = FunctionBase::generateXMLDefinition(elementName);
    funcElement.appendChild(_A.generateXMLValueDefinition());
    funcElement.appendChild(_B.generateXMLValueDefinition());
    funcElement.appendChild(_mu.generateXMLValueDefinition());
    funcElement.appendChild(_lambda.generateXMLValueDefinition());
    funcElement.appendChild(_delta.generateXMLValueDefinition());
    return funcElement;
}

//========================================================================
//========================================================================
//========================================================================

/************************************************************************
* Evaluate function --> Polynomial
************************************************************************/
double FunctionPoly::evaluateInternal(double r)
{
    if(r < cutoff()) {
        if(!_isPrepared) imposeBoundaryConditions();

        double sum = 0;
        for(const PolyNode& polyNode : _nodes) sum += polyNode.coeff * pow(r, polyNode.exponent);
        return sum;
    }
    return 0;
}

/**********************************************************************************
* Evaluate both function and its derivative.
**********************************************************************************/
double FunctionPoly::evaluateInternal(double r, double& deriv)
{
    if(r < cutoff()) {
        if(!_isPrepared) imposeBoundaryConditions();

        double f = deriv = 0;
        for(const PolyNode& polyNode : _nodes) {
            f += polyNode.coeff * pow(r, polyNode.exponent);
            if(polyNode.exponent > 0) deriv += polyNode.exponent * polyNode.coeff * pow(r, polyNode.exponent - 1);
        }
        return f;
    }
    else {
        deriv = 0;
        return 0;
    }
}

/**********************************************************************************
* Evaluate analytically the integral of r^2 * polynomial without the pre-factor.
**********************************************************************************/
double FunctionPoly::integrateSpherical(double a, double b)
{
    if(a > b)
        throw runtime_error(
            str(format("Left boundary larger than right boundary in FunctionPoly::integrateSpherical(): %1% > %2%.") % a % b));

    if(!_isPrepared) imposeBoundaryConditions();

    double c = (b > cutoff() ? cutoff() : b);
    double suma = 0;
    double sumc = 0;
    for(const PolyNode& polyNode : _nodes) {
        suma += polyNode.coeff / (polyNode.exponent + 3) * pow(a, polyNode.exponent + 3);
        sumc += polyNode.coeff / (polyNode.exponent + 3) * pow(c, polyNode.exponent + 3);
    }
    return sumc - suma;
}

/******************************************************************************
* Impose boundary conditions on polynomial: f(0) = 1 and f(1) = 0.
******************************************************************************/
void FunctionPoly::imposeBoundaryConditions()
{
    double sum = 0;
    for(const PolyNode& polyNode : _nodes)
        if(polyNode.exponent != _degree) sum += polyNode.coeff * pow(cutoff(), polyNode.exponent);
    sum *= -pow(cutoff(), -_degree);
    _nodes.back().coeff = sum;

    _isPrepared = true;
}

/******************************************************************************
* Parses polynomial function parameters in the XML element in the job file.
******************************************************************************/
void FunctionPoly::parse(XML::Element element)
{
    // Cutoff radius.
    _cutoff = element.parseFloatParameterElement("cutoff");
    if(_cutoff <= 0)
        throw runtime_error(
            str(format("Cutoff (value=%1%) has to be positive in line %2% of XML file.") % _cutoff % element.lineNumber()));

    // Coefficients of polynomial.
    _degree = 0;

    XML::Element coeffsElement = element.expectChildElement("coefficients");
    for(XML::Element coeffElement = coeffsElement.firstChildElement(); coeffElement; coeffElement = coeffElement.nextSibling()) {
        // Are we being reasonable?
        coeffElement.expectTag("coeff");

        // Exponent of polynomial term.
        int n = coeffElement.parseIntParameterAttribute("n");
        _degree = max(_degree, n + 1);
        if(n == 0)
            throw runtime_error(str(format("Zero-th coefficient of polynomial cannot be set by user (line %1% of XML file).") %
                                    coeffElement.lineNumber()));
        double value = coeffElement.parseFloatParameterAttribute("value");

        // Create new node and add to list.
        PolyNode node;
        node.coeff = value;
        node.coeff.setId(str(format("coeff.%1%") % n));
        node.exponent = n;

        // Parse "fit-dof" equivalent parameters.
        if(coeffElement.parseOptionalBooleanParameterAttribute("enabled", true)) node.coeff.setFitEnabled(true);
        if(!coeffElement.parseOptionalBooleanParameterAttribute("reset", true)) node.coeff.setResetBeforeRelax(false);
        if(coeffElement.hasAttribute("min")) node.coeff.setLowerBound(coeffElement.parseFloatParameterAttribute("min"));
        if(coeffElement.hasAttribute("max")) node.coeff.setUpperBound(coeffElement.parseFloatParameterAttribute("max"));

        _nodes.push_back(node);
    }

    // Initialize outermost coefficients (required for imposing boundary conditions).
    PolyNode node;
    node.coeff = 1.0;
    node.coeff.setId("coeff.0");
    node.exponent = 0;
    _nodes.push_back(node);
    node.coeff = 0.0;
    node.coeff.setId(str(format("coeff.%1%") % _degree));
    node.exponent = _degree;
    _nodes.push_back(node);

    // Register coefficients.
    for(auto& node : _nodes) registerDOF(&node.coeff);
}

/************************************************************************
* Produces an XML representation of the function's current parameter values.
************************************************************************/
XML::OElement FunctionPoly::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement = FunctionBase::generateXMLDefinition(elementName);

    // Generate <coefficients> element.
    XML::OElement coefficientsElement("coefficients");
    for(const PolyNode& node : _nodes) {
        if(node.exponent == 0 || node.exponent == _degree) continue;
        XML::OElement coeffElement = node.coeff.generateXMLFitDefinition();
        coeffElement.setTagName("coeff");
        coeffElement.setAttribute("value", std::to_string(node.coeff));
        coeffElement.setAttribute("n", std::to_string(node.exponent));
        coefficientsElement.appendChild(coeffElement);
    }
    funcElement.appendChild(coefficientsElement);
    return funcElement;
}

//========================================================================
//========================================================================
//========================================================================

/************************************************************************
* Prepare spline. Adapted from CubicSpline.cpp.
************************************************************************/
void FunctionSpline::prepareSpline()
{
    int N = _nodes.size();
    std::vector<double> u(N);
    double dx = _nodes[1].x - _nodes[0].x;
    _nodes[0].y2 = -0.5;
    u[0] = 3.0 / dx * ((_nodes[1].y - _nodes[0].y) / dx - _deriv0);
    for(int i = 1; i <= N - 2; i++) {
        double sig = (_nodes[i].x - _nodes[i - 1].x) / (_nodes[i + 1].x - _nodes[i - 1].x);
        double p = sig * _nodes[i - 1].y2 + 2.0;
        _nodes[i].y2 = (sig - 1.0) / p;
        u[i] = (_nodes[i + 1].y - _nodes[i].y) / (_nodes[i + 1].x - _nodes[i].x) -
               (_nodes[i].y - _nodes[i - 1].y) / (_nodes[i].x - _nodes[i - 1].x);
        u[i] = (6.0 * u[i] / (_nodes[i + 1].x - _nodes[i - 1].x) - sig * u[i - 1]) / p;
    }

    double qn = 0.5;
    dx = _nodes[N - 1].x - _nodes[N - 2].x;
    double un = 3.0 / dx * (_derivN - (_nodes[N - 1].y - _nodes[N - 2].y) / dx);
    _nodes[N - 1].y2 = (un - qn * u[N - 2]) / (qn * _nodes[N - 2].y2 + 1.0);
    for(int k = N - 2; k >= 0; k--) {
        _nodes[k].y2 = _nodes[k].y2 * _nodes[k + 1].y2 + u[k];
    }

    _isPrepared = true;
}

/************************************************************************
* Evaluate sigma function --> Spline.
************************************************************************/
double FunctionSpline::evaluateInternal(double x)
{
    if(!_isPrepared) prepareSpline();

    double value = 0;
    int N = _nodes.size();
    if(x > _nodes[0].x && x < _nodes[N - 1].x) {
        // Do interval search.
        int klo = 0;
        int khi = N - 1;
        while(khi - klo > 1) {
            int k = (khi + klo) / 2;
            if(_nodes[k].x > x)
                khi = k;
            else
                klo = k;
        }
        // Do spline interpolation.
        double h = _nodes[khi].x - _nodes[klo].x;
        double a = (_nodes[khi].x - x) / h;
        double b = (x - _nodes[klo].x) / h;
        value = a * _nodes[klo].y + b * _nodes[khi].y +
                ((a * a * a - a) * _nodes[klo].y2 + (b * b * b - b) * _nodes[khi].y2) * (h * h) / 6.0;
    }
    else if(x <= _nodes[0].x) {  // Left extrapolation.
        value = _nodes[0].y + _deriv0 * (x - _nodes[0].x);
    }
    else {  // Right extrapolation.
        value = _nodes[N - 1].y + _derivN * (x - _nodes[N - 1].x);
    }
    return value;
}

/**********************************************************************************
* Evaluate function and its derivative.
**********************************************************************************/
double FunctionSpline::evaluateInternal(double x, double& deriv)
{
    if(!_isPrepared) prepareSpline();

    int N = _nodes.size();
    double f;
    if(x > _nodes[0].x && x < _nodes[N - 1].x) {
        // Do interval search.
        int klo = 0;
        int khi = N - 1;
        while(khi - klo > 1) {
            int k = (khi + klo) / 2;
            if(_nodes[k].x > x)
                khi = k;
            else
                klo = k;
        }
        // Do spline interpolation.
        double h = _nodes[khi].x - _nodes[klo].x;
        double a = (_nodes[khi].x - x) / h;
        double b = (x - _nodes[klo].x) / h;
        deriv = (_nodes[khi].y - _nodes[klo].y) / h +
                ((3.0 * b * b - 1.0) * _nodes[khi].y2 - (3.0 * a * a - 1.0) * _nodes[klo].y2) * h / 6.0;
        f = a * _nodes[klo].y + b * _nodes[khi].y +
            ((a * a * a - a) * _nodes[klo].y2 + (b * b * b - b) * _nodes[khi].y2) * (h * h) / 6.0;
    }
    else if(x <= _nodes[0].x) {  // Left extrapolation.
        deriv = _deriv0;
        f = _nodes[0].y + _deriv0 * (x - _nodes[0].x);
    }
    else {  // Right extrapolation.
        deriv = _derivN;
        f = _nodes[N - 1].y + _derivN * (x - _nodes[N - 1].x);
    }
    return f;
}

/******************************************************************************
* Parses concentration function parameters in the XML element in the job file.
******************************************************************************/
void FunctionSpline::parse(XML::Element sigElement)
{
    // Cutoff radius.
    _cutoff = sigElement.parseOptionalFloatParameterElement("cutoff", _cutoff);
    if(_cutoff <= 0)
        throw runtime_error(
            str(format("Cutoff (value=%1%) must be positive in line %2% of XML file.") % _cutoff % sigElement.lineNumber()));

    // TODO: Maybe we want to turn deriv0 and derivN into 'true' DOFs.
    // At the moment they are declared as ScalarDOFs in order for them to show up in the output.
    // None of the other DOF features have been implemented though.

    // Derivatives at the left boundary.
    _deriv0 = sigElement.parseOptionalFloatParameterElement("derivative-left", 0);
    _deriv0.setId("deriv0");
    registerDOF(&_deriv0);

    // Derivatives at the right boundary.
    _derivN = sigElement.parseOptionalFloatParameterElement("derivative-right", 0);
    _derivN.setId("derivN");
    registerDOF(&_derivN);

#if 0
    // Add outermost nodes.
    SplineNode node;
    node.x = 0;
    node.y = 1;
    node.y.setId("node[0].y");
    _nodes.push_back(node);
    node.x = cutoff();
    node.y = 0;
    node.y.setId("node[cutoff].y");
    _nodes.push_back(node);
#endif

    // Parse spline coefficients.
    XML::Element nodesElement = sigElement.expectChildElement("nodes");
    for(XML::Element nodeElement = nodesElement.firstChildElement(); nodeElement; nodeElement = nodeElement.nextSibling()) {
        // Are we being reasonable?
        nodeElement.expectTag("node");

        // Parse spline node.
        double xn = nodeElement.parseFloatParameterAttribute("x");
        if(hasCutoff() && (xn < 0 || xn > cutoff()))
            throw runtime_error(str(format("Node (x=%1%) outside of permissible range in line %2% of XML file.") % xn %
                                    nodeElement.lineNumber()));

        double yn = nodeElement.parseFloatParameterAttribute("y");
        SplineNode node;

        node.x = xn;
        node.y = yn;
        node.y.setId(str(format("node[%1%].y") % xn));

        // Parse "fit-dof" equivalent parameters.
        if(nodeElement.parseOptionalBooleanParameterAttribute("enabled", true)) node.y.setFitEnabled(true);
        if(!nodeElement.parseOptionalBooleanParameterAttribute("reset", true)) node.y.setResetBeforeRelax(false);
        if(nodeElement.hasAttribute("min")) node.y.setLowerBound(nodeElement.parseFloatParameterAttribute("min"));
        if(nodeElement.hasAttribute("max")) node.y.setUpperBound(nodeElement.parseFloatParameterAttribute("max"));

        _nodes.push_back(node);
    }
    if(_nodes.size() < 2)
        throw runtime_error(
            str(format("Spline function '%1%' in line %2% of XML file has not enough nodes.") % id() % sigElement.lineNumber()));

    // We want the nodes to be ordered.
    sort(_nodes.begin(), _nodes.end());

    // Register coefficients.
    for(auto& node : _nodes) registerDOF(&node.y);
}

/************************************************************************
* Produces an XML representation of the function's current parameter values.
************************************************************************/
XML::OElement FunctionSpline::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement = FunctionBase::generateXMLDefinition(elementName);

    // Generate <nodes> element.
    XML::OElement nodesElement("nodes");
    for(const SplineNode& node : _nodes) {
        //		if(node.x == 0 || node.x == cutoff())
        //			continue;
        XML::OElement nodeElement = node.y.generateXMLFitDefinition();
        nodeElement.setTagName("node");
        nodeElement.setAttribute("x", std::to_string(node.x));
        nodeElement.setAttribute("y", std::to_string(node.y));
        nodesElement.appendChild(nodeElement);
    }
    funcElement.appendChild(nodesElement);

    return funcElement;
}

//========================================================================
//========================================================================
//========================================================================

/******************************************************************************
* Parses function parameters in the XML element in the job file.
******************************************************************************/
void FunctionSum::parse(XML::Element functionElement)
{
    FunctionBase::parse(functionElement);
    _cutoff = 0;
    for(XML::Element childElement = functionElement.firstChildElement(); childElement;
        childElement = childElement.nextSibling()) {
        // Parse function object.
        std::shared_ptr<FunctionBase> func = FunctionBase::createAndParse(childElement, FPString(), job());
        // Add to list of sub-functions.
        _functions.push_back(func);
        _cutoff = std::max(_cutoff, func->cutoff());
        registerSubObject(func.get());
    }
}

//========================================================================
//========================================================================
//========================================================================

/******************************************************************************
* Parses function parameters in the XML element in the job file.
******************************************************************************/
void FunctionProduct::parse(XML::Element functionElement)
{
    FunctionBase::parse(functionElement);
    _cutoff = std::numeric_limits<double>::max();
    int n = 0;
    for(XML::Element childElement = functionElement.firstChildElement(); childElement;
        childElement = childElement.nextSibling()) {
        // Parse function object.
        std::shared_ptr<FunctionBase> func = FunctionBase::createAndParse(childElement, FPString(), job());
        // Determine maximum cutoff.
        _cutoff = std::min(_cutoff, func->cutoff());
        if(n == 0)
            _function1 = func;
        else
            _function2 = func;
        n++;
        registerSubObject(func.get());
    }
    if(n != 2)
        throw runtime_error(str(format("Product function in line %1% of XML file requires exactly two sub-functions.") %
                                functionElement.lineNumber()));
}

//========================================================================
//========================================================================
//========================================================================

/******************************************************************************
* Constructor for the constant function.
******************************************************************************/
FunctionConstant::FunctionConstant(const FPString& id, FitJob* job) : FunctionBase(id, job, "constant"), _const("const")
{
    registerDOF(&_const);
}

/************************************************************************
* Evaluate function.
************************************************************************/
double FunctionConstant::evaluateInternal(double r) { return _const; }

/**********************************************************************************
* Evaluate derivative of function.
**********************************************************************************/
double FunctionConstant::evaluateInternal(double r, double& deriv)
{
    deriv = 0.0;
    return _const;
}

/******************************************************************************
* Parses concentration function parameters in the XML element in the job file.
******************************************************************************/
void FunctionConstant::parse(XML::Element sigElement)
{
    FunctionBase::parse(sigElement);
    _const = sigElement.parseFloatParameterElement("const");
}

/************************************************************************
* Produces an XML representation of the function's current parameter values.
************************************************************************/
XML::OElement FunctionConstant::generateXMLDefinition(const FPString& elementName)
{
    XML::OElement funcElement = FunctionBase::generateXMLDefinition(elementName);
    funcElement.appendChild(_const.generateXMLValueDefinition());
    return funcElement;
}
}
