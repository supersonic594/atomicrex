///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "ABOPotential.h"
#include "../structures/AtomicStructure.h"
#include "../structures/NeighborList.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor.
******************************************************************************/
ABOPotential::ABOPotential(const FPString& id, FitJob* job, const FPString& tag) : Potential(id, job, tag), _cutoff(0)
{
    // Remember the number of atom types.
    _numAtomTypes = job->numAtomTypes();

    // Allocate memory for parameter sets (one for each triplet of atom types).
    ABOParamSet p;
    p.powermint = 0;
    _params.resize(_numAtomTypes * _numAtomTypes * _numAtomTypes, p);

    // Initialize DOFs. Every triplet gets its own independent parameter set.
    auto iter = _params.begin();
    for(int i = 0; i < _numAtomTypes; i++) {
        for(int j = 0; j < _numAtomTypes; j++) {
            for(int k = 0; k < _numAtomTypes; k++) {
                iter->r0.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->D0.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->beta.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->S.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->gamma.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->c.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->d.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->h.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->twomu.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->beta2.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->powern.setTag(
                    str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->bigr.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                iter->bigd.setTag(str(format("%1%%2%%3%") % job->atomTypeName(i) % job->atomTypeName(j) % job->atomTypeName(k)));
                registerDOF(&iter->r0);
                registerDOF(&iter->D0);
                registerDOF(&iter->beta);
                registerDOF(&iter->S);
                registerDOF(&iter->gamma);
                registerDOF(&iter->c);
                registerDOF(&iter->d);
                registerDOF(&iter->h);
                registerDOF(&iter->twomu);
                registerDOF(&iter->beta2);
                registerDOF(&iter->powern);
                registerDOF(&iter->bigr);
                registerDOF(&iter->bigd);
                ++iter;
            }
        }
    }
}

/******************************************************************************
* Computes the total energy of the structure.
******************************************************************************/
double ABOPotential::computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        if(!isAtomTypeEnabled(itype)) continue;

        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);

        // Two-body interactions
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            int j = neighbor_j->localIndex;
            int jtype = structure.atomType(j);
            if(!isAtomTypeEnabled(jtype)) continue;

            const ABOParamSet& param_ij = getParamSet(itype, jtype, jtype);
            double rij = neighbor_j->r;
            if(rij < param_ij.cut()) {
                double tmp_fc = param_ij.ters_fc(rij);
                double tmp_exp = exp(-param_ij.beta * sqrt(2.0 * param_ij.S) * (rij - param_ij.r0));
                totalEnergy += tmp_fc * param_ij.D0 / (param_ij.S - 1.0) * tmp_exp;
            }
        }

        // Three-body interactions
        neighbor_j = data.neighborList.neighborList(i);
        jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            int j = neighbor_j->localIndex;
            int jtype = structure.atomType(j);
            if(!isAtomTypeEnabled(jtype)) continue;

            const ABOParamSet& param_ij = getParamSet(itype, jtype, jtype);
            double rij = neighbor_j->r;
            if(rij > param_ij.cut()) continue;

            // Accumulate bond-order zeta for each i-j interaction via loop over k.
            double zeta_ij = 0.0;
            NeighborListEntry* neighbor_k = data.neighborList.neighborList(i);
            for(int kk = 0; kk < jnum; kk++, neighbor_k++) {
                if(kk == jj) continue;
                int k = neighbor_k->localIndex;
                int ktype = structure.atomType(k);

                const ABOParamSet& param_ijk = getParamSet(itype, jtype, ktype);
                double rik = neighbor_k->r;
                if(rik < param_ijk.cut()) {
                    double costheta = neighbor_k->delta.dot(neighbor_j->delta) / (rij * rik);
                    double arg = param_ijk.twomu * (rij - rik);
                    BOOST_ASSERT(param_ijk.powermint == 1 || param_ijk.powermint == 3);
                    if(param_ijk.powermint == 3) arg = arg * arg * arg;
                    double ex_delr;
                    if(arg > 69.0776)
                        ex_delr = 1.e30;
                    else if(arg < -69.0776)
                        ex_delr = 0.0;
                    else
                        ex_delr = exp(arg);
                    zeta_ij += param_ijk.ters_fc(rik) * param_ijk.ters_gijk(costheta) * ex_delr;
                }
            }

            // Pairwise force due to zeta.
            double fa = param_ij.ters_fa(rij);
            double bij = param_ij.ters_bij(zeta_ij);
            totalEnergy += 0.5 * bij * fa;
        }
    }
    return totalEnergy;
}

/******************************************************************************
* Computes the total energy and forces of the structure.
******************************************************************************/
double ABOPotential::computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    std::vector<Vector3>& forces = structure.atomForces();
    std::array<double, 6>& virial = structure.virial();

    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        if(!isAtomTypeEnabled(itype)) continue;

        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);

        // Two-body interactions
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            int j = neighbor_j->localIndex;
            int jtype = structure.atomType(j);
            if(!isAtomTypeEnabled(jtype)) continue;

            const ABOParamSet& param_ij = getParamSet(itype, jtype, jtype);
            double rij = neighbor_j->r;
            if(rij < param_ij.cut()) {
                double tmp_fc = param_ij.ters_fc(rij);
                double tmp_fc_d = param_ij.ters_fc_d(rij);
                double tmp_exp = exp(-param_ij.beta * sqrt(2.0 * param_ij.S) * (rij - param_ij.r0));
                totalEnergy += tmp_fc * param_ij.D0 / (param_ij.S - 1.0) * tmp_exp;
                double fpair =
                    -param_ij.D0 / (param_ij.S - 1.0) * tmp_exp * (tmp_fc_d - tmp_fc * param_ij.beta * sqrt(2.0 * param_ij.S));
                Vector3 fvec = neighbor_j->delta * (fpair / rij);
                BOOST_ASSERT(i < forces.size() && j < forces.size());
                forces[i] -= fvec;
                forces[j] += fvec;
                virial[0] += neighbor_j->delta.x() * fvec.x();
                virial[1] += neighbor_j->delta.y() * fvec.y();
                virial[2] += neighbor_j->delta.z() * fvec.z();
                virial[3] += neighbor_j->delta.y() * fvec.z();
                virial[4] += neighbor_j->delta.x() * fvec.z();
                virial[5] += neighbor_j->delta.x() * fvec.y();
            }
        }

        // Three-body interactions
        neighbor_j = data.neighborList.neighborList(i);
        jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            int j = neighbor_j->localIndex;
            int jtype = structure.atomType(j);
            if(!isAtomTypeEnabled(jtype)) continue;

            const ABOParamSet& param_ij = getParamSet(itype, jtype, jtype);
            double rij = neighbor_j->r;
            if(rij > param_ij.cut()) continue;

            // Accumulate bond-order zeta for each i-j interaction via loop over k.
            double zeta_ij = 0.0;
            NeighborListEntry* neighbor_k = data.neighborList.neighborList(i);
            for(int kk = 0; kk < jnum; kk++, neighbor_k++) {
                if(kk == jj) continue;
                int k = neighbor_k->localIndex;
                int ktype = structure.atomType(k);

                const ABOParamSet& param_ijk = getParamSet(itype, jtype, ktype);
                double rik = neighbor_k->r;
                if(rik < param_ijk.cut()) {
                    double costheta = neighbor_k->delta.dot(neighbor_j->delta) / (rij * rik);
                    double arg = param_ijk.twomu * (rij - rik);
                    BOOST_ASSERT(param_ijk.powermint == 1 || param_ijk.powermint == 3);
                    if(param_ijk.powermint == 3) arg = arg * arg * arg;
                    double ex_delr;
                    if(arg > 69.0776)
                        ex_delr = 1.e30;
                    else if(arg < -69.0776)
                        ex_delr = 0.0;
                    else
                        ex_delr = exp(arg);
                    zeta_ij += param_ijk.ters_fc(rik) * param_ijk.ters_gijk(costheta) * ex_delr;
                }
            }

            Vector3 rij_hat = neighbor_j->delta / rij;

            // Pairwise force due to zeta.
            double fa = param_ij.ters_fa(rij);
            double fa_d = param_ij.ters_fa_d(rij);
            double bij = param_ij.ters_bij(zeta_ij);
            double fpair = 0.5 * bij * fa_d;
            double prefactor = -0.5 * fa * param_ij.ters_bij_d(zeta_ij);
            totalEnergy += 0.5 * bij * fa;
            Vector3 fvec = rij_hat * fpair;
            forces[i] += fvec;
            forces[j] -= fvec;
            virial[0] -= neighbor_j->delta.x() * fvec.x();
            virial[1] -= neighbor_j->delta.y() * fvec.y();
            virial[2] -= neighbor_j->delta.z() * fvec.z();
            virial[3] -= neighbor_j->delta.y() * fvec.z();
            virial[4] -= neighbor_j->delta.x() * fvec.z();
            virial[5] -= neighbor_j->delta.x() * fvec.y();

            // Attractive term via loop over k.
            neighbor_k = data.neighborList.neighborList(i);
            for(int kk = 0; kk < jnum; kk++, neighbor_k++) {
                if(kk == jj) continue;
                int k = neighbor_k->localIndex;
                int ktype = structure.atomType(k);

                const ABOParamSet& param_ijk = getParamSet(itype, jtype, ktype);
                double rik = neighbor_k->r;
                if(rik < param_ijk.cut()) {
                    Vector3 rik_hat = neighbor_k->delta / rik;

                    double fc = param_ijk.ters_fc(rik);
                    double dfc = param_ijk.ters_fc_d(rik);
                    double tmp = param_ijk.twomu * (rij - rik);
                    if(param_ijk.powermint == 3) tmp = tmp * tmp * tmp;

                    double ex_delr;
                    if(tmp > 69.0776)
                        ex_delr = 1.e30;
                    else if(tmp < -69.0776)
                        ex_delr = 0.0;
                    else
                        ex_delr = exp(tmp);

                    double ex_delr_d;
                    if(param_ijk.powermint == 3)
                        ex_delr_d = 3.0 * pow(param_ijk.twomu, 3.0) * pow(rij - rik, 2.0) * ex_delr;
                    else
                        ex_delr_d = param_ijk.twomu * ex_delr;

                    double cos_theta = rij_hat.dot(rik_hat);
                    double gijk = param_ijk.ters_gijk(cos_theta);
                    double gijk_d = param_ijk.ters_gijk_d(cos_theta);

                    Vector3 dcosdrj = (-cos_theta * rij_hat + rik_hat) / rij;
                    Vector3 dcosdrk = (-cos_theta * rik_hat + rij_hat) / rik;
                    Vector3 dcosdri = -(dcosdrj + dcosdrk);

                    Vector3 dri = (-dfc * gijk * ex_delr) * rik_hat;
                    dri += (fc * gijk_d * ex_delr) * dcosdri;
                    dri += (fc * gijk * ex_delr_d) * rik_hat;
                    dri += (-fc * gijk * ex_delr_d) * rij_hat;
                    dri *= prefactor;
                    Vector3 drj = (fc * gijk_d * ex_delr) * dcosdrj;
                    drj += (fc * gijk * ex_delr_d) * rij_hat;
                    drj *= prefactor;
                    Vector3 drk = (dfc * gijk * ex_delr) * rik_hat;
                    drk += (fc * gijk_d * ex_delr) * dcosdrk;
                    drk += (-fc * gijk * ex_delr_d) * rik_hat;
                    drk *= prefactor;

                    forces[i] += dri;
                    forces[j] += drj;
                    forces[k] += drk;

                    virial[0] += neighbor_j->delta.x() * drj.x() + neighbor_k->delta.x() * drk.x();
                    virial[1] += neighbor_j->delta.y() * drj.y() + neighbor_k->delta.y() * drk.y();
                    virial[2] += neighbor_j->delta.z() * drj.z() + neighbor_k->delta.z() * drk.z();
                    virial[3] += neighbor_j->delta.y() * drj.z() + neighbor_k->delta.y() * drk.z();
                    virial[4] += neighbor_j->delta.x() * drj.z() + neighbor_k->delta.x() * drk.z();
                    virial[5] += neighbor_j->delta.x() * drj.y() + neighbor_k->delta.x() * drk.y();
                }
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
 * This function is called by the fit job on shutdown, i.e. after the fitting
 * process has finished.
 ******************************************************************************/
void ABOPotential::outputResults()
{
    Potential::outputResults();

    if(_exportPotentialFile.empty() == false) {
        MsgLogger(medium) << "Writing bond-order potential file to " << makePathRelative(_exportPotentialFile) << endl;
        writePotential(_exportPotentialFile);
    }
}

/******************************************************************************
 * Generates a potential file to be used with simulation codes.
 ******************************************************************************/
void ABOPotential::writePotential(const FPString& filename) const
{
    ofstream stream(filename.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Could not open potential file for writing: %1%") % filename));

    stream << setprecision(9);

    // Write format version line.
    stream << "# Tersoff potential fitted using the PotFit code" << endl;
    stream << "#" << endl;

    // Write comment line.
    stream << "# " << job()->name() << " (" << boost::gregorian::day_clock::local_day() << ")" << endl << endl;
    stream
        << "# A1  A2  A3  m                gamma            lambda3          c                d                h                n"
           "                beta             lambda2          B                R                D                lambda1         "
           " A"
        << endl;

    // Write Parameter sets.
    int index = 0;
    for(int i = 0; i < _numAtomTypes; i++) {
        for(int j = 0; j < _numAtomTypes; j++) {
            for(int k = 0; k < _numAtomTypes; k++) {
                double D0, r0, beta, S;
                D0 = _params[index].D0.operator double();
                r0 = _params[index].r0.operator double();
                beta = _params[index].beta.operator double();
                S = _params[index].S.operator double();

                stream << std::scientific;
                stream << str(format("  %-2s") % job()->atomTypeName(i)) << str(format("  %-2s") % job()->atomTypeName(j))
                       << str(format("  %-2s") % job()->atomTypeName(k)) << "  " << _params[index].powerm << "  "
                       << _params[index].gamma.operator double() << "  " << _params[index].twomu.operator double() << "  "
                       << _params[index].c.operator double() << "  " << _params[index].d.operator double() << "  "
                       << -1.0 * _params[index].h.operator double() << "  " << _params[index].powern.operator double() << "  "
                       << _params[index].beta2.operator double() << "  " << (beta * sqrt(2.0 / S)) << "  "
                       << (D0 * S / (S - 1.0) * exp(beta * sqrt(2 / S) * r0)) << "  " << _params[index].bigr << "  "
                       << _params[index].bigd << "  " << (beta * sqrt(2.0 * S)) << "  "
                       << (D0 / (S - 1.0) * exp(beta * sqrt(2 * S) * r0)) << "  " << endl;

                index++;
            }
        }
    }

    // Write commented ABOP Parameters also.
    stream << "#  A1  A2  A3  r0               D0               beta             S                gamma            c             "
              "   d"
              "                h                twomu            beta2            n                m                D            "
              "    R"
           << endl;
    index = 0;
    for(int i = 0; i < _numAtomTypes; i++) {
        for(int j = 0; j < _numAtomTypes; j++) {
            for(int k = 0; k < _numAtomTypes; k++) {
                stream << "#" << str(format("  %-2s") % job()->atomTypeName(i)) << str(format("  %-2s") % job()->atomTypeName(j))
                       << str(format("  %-2s") % job()->atomTypeName(k)) << "  " << _params[index].r0.operator double() << "  "
                       << _params[index].D0.operator double() << "  " << _params[index].beta.operator double() << "  "
                       << _params[index].S.operator double() << "  " << _params[index].gamma.operator double() << "  "
                       << _params[index].c.operator double() << "  " << _params[index].d.operator double() << "  "
                       << _params[index].h.operator double() << "  " << _params[index].twomu.operator double() << "  "
                       << _params[index].beta2.operator double() << "  " << _params[index].powern.operator double() << "  "
                       << _params[index].powerm << "  " << _params[index].bigd << "  " << _params[index].bigr << endl;
                index++;
            }
        }
    }
}

/******************************************************************************
* Parses any potential-specific parameters in the XML element in the job file.
******************************************************************************/
void ABOPotential::parse(XML::Element potentialElement)
{
    Potential::parse(potentialElement);

    FPString filename = potentialElement.parsePathParameterElement("param-file");
    parseTersoffFile(filename);

    // Make sure that the parameter sets have been specified for all atom type triplets.
    for(size_t i = 0; i < _params.size(); i++) {
        int itype = i / (_numAtomTypes * _numAtomTypes);
        int jtype = (i / _numAtomTypes) % _numAtomTypes;
        int ktype = i % _numAtomTypes;
        if(_params[i].powermint == 0) {
            throw runtime_error(
                str(format("Tersoff parameters for the triplet %1% %2% %3% have not been specified for potential %4%.") %
                    job()->atomTypeName(itype) % job()->atomTypeName(jtype) % job()->atomTypeName(ktype) % id()));
        }

        // Get maximum cutoff for the potential
        double r = _params[i].bigr;
        if(_params[i].bigr.fitEnabled()) {
            if(_params[i].bigr.upperBound() == std::numeric_limits<double>::infinity()) {
                throw runtime_error(str(format("Tersoff parameter 'bigr' for the triplet %1% %2% %3% of potential %4% needs an "
                                               "upper boundary if fitting is enabled.") %
                                        job()->atomTypeName(itype) % job()->atomTypeName(jtype) % job()->atomTypeName(ktype) %
                                        id()));
            }
            r = _params[i].bigr.upperBound();
        }

        double d = _params[i].bigd;
        if(_params[i].bigd.fitEnabled()) {
            if(_params[i].bigd.upperBound() == std::numeric_limits<double>::infinity()) {
                throw runtime_error(str(format("Tersoff parameter 'bigd' for the triplet %1% %2% %3% of potential %4% needs an "
                                               "upper boundary if fitting is enabled.") %
                                        job()->atomTypeName(itype) % job()->atomTypeName(jtype) % job()->atomTypeName(ktype) %
                                        id()));
            }
            d = _params[i].bigd.upperBound();
        }

        if(_cutoff < r + d) {
            _cutoff = r + d;
        }
    }
    MsgLogger(maximum) << "Maximum cutoff for Tersoff potential '" << id() << "' is " << _cutoff << endl;

    // Parse output options.
    _exportPotentialFile = potentialElement.parseOptionalPathParameterElement("export-potential");

    // Parse DOF options.
    _dofMode = potentialElement.parseOptionalBooleanParameterAttribute("classic-mode", false);
    if(_dofMode) {
        linkDOF();
    }
}

/******************************************************************************
* Links the DOF together to get only doublett interaction as in the classical ABOP.
******************************************************************************/
void ABOPotential::linkDOF()
{
    for(int i = 0; i < _numAtomTypes; i++) {
        for(int j = 0; j < _numAtomTypes; j++) {
            for(int k = 0; k < _numAtomTypes; k++) {
                int master_2body, master_3body, slave;
                // Save the index of the current parameter set.
                slave = i * _numAtomTypes * _numAtomTypes + j * _numAtomTypes + k;
                // Find the index of the master DOF for the two-body parameter set.
                if(i < j) {
                    master_2body = i * (_numAtomTypes * _numAtomTypes) + j * (_numAtomTypes + 1);
                }
                else {
                    master_2body = j * (_numAtomTypes * _numAtomTypes) + i * (_numAtomTypes + 1);
                }
                // Find the index of the master DOF for the three-body parameter set.
                if(i < k) {
                    master_3body = i * (_numAtomTypes * _numAtomTypes) + k * (_numAtomTypes + 1);
                }
                else {
                    master_3body = k * (_numAtomTypes * _numAtomTypes) + i * (_numAtomTypes + 1);
                }

                // Link the DOFs.
                if(slave != master_2body || slave != master_3body) {
                    _params[slave].r0.setEqualTo(&_params[master_2body].r0);
                    _params[slave].D0.setEqualTo(&_params[master_2body].D0);
                    _params[slave].beta.setEqualTo(&_params[master_2body].beta);
                    _params[slave].S.setEqualTo(&_params[master_2body].S);
                    _params[slave].gamma.setEqualTo(&_params[master_3body].gamma);
                    _params[slave].c.setEqualTo(&_params[master_3body].c);
                    _params[slave].d.setEqualTo(&_params[master_3body].d);
                    _params[slave].h.setEqualTo(&_params[master_3body].h);
                    _params[slave].twomu.setEqualTo(&_params[master_3body].twomu);
                    _params[slave].beta2.setEqualTo(&_params[master_3body].beta2);
                    _params[slave].powern.setEqualTo(&_params[master_3body].powern);
                    _params[slave].bigr.setEqualTo(&_params[master_3body].bigr);
                    _params[slave].bigd.setEqualTo(&_params[master_3body].bigd);
                }
            }
        }
    }
}

/******************************************************************************
* Parses Tersoff parameters from a LAMMPS potential file.
******************************************************************************/
void ABOPotential::parseTersoffFile(const FPString& filename)
{
    ifstream stream(filename.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Could not open Tersoff potential file: %1%") % filename));

    // Helper function that counts the number of words in a text string.
    auto count_tokens = [](const std::string& s) -> size_t {
        size_t ntokens = 0;
        auto p = s.find_first_not_of(" \t\n\r\f");
        while(p != std::string::npos) {
            ntokens++;
            p = s.find_first_of(" \t\n\r\f", p);
            if(p == std::string::npos) break;
            p = s.find_first_not_of(" \t\n\r\f", p);
        }
        return ntokens;
    };

    while(stream) {
        FPString line;
        getline(stream, line);
        // Strip comments.
        line.erase(std::find(line.begin(), line.end(), '#'), line.end());
        // Skip line if empty.
        size_t ntokens = count_tokens(line);
        if(ntokens == 0) continue;
        // Concatenate additional lines until have enough tokens.
        while(ntokens < 17) {
            if(!stream) throw runtime_error(str(format("Unexpected end of Tersoff potential file: %1%") % filename));
            FPString line2;
            getline(stream, line2);
            // Strip comments.
            line2.erase(std::find(line2.begin(), line2.end(), '#'), line2.end());
            size_t ntokens2 = count_tokens(line2);
            if(ntokens2 == 0)
                throw runtime_error(str(format("Unexpected end of parameter line in Tersoff potential file: %1%") % filename));
            line.append(" ");
            line.append(line2);
            ntokens += ntokens2;
        }

        // Parse parameter set.
        char ename_i[16];
        char ename_j[16];
        char ename_k[16];
        double lam1, lam2, lam3;
        double c, d, h;
        double gamma;
        double powerm, powern;
        double beta2;
        double biga, bigb, bigd, bigr;
        int nt = sscanf(line.c_str(), "%15s %15s %15s %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg", ename_i, ename_j,
                        ename_k, &powerm, &gamma, &lam3, &c, &d, &h, &powern, &beta2, &lam2, &bigb, &bigr, &bigd, &lam1, &biga);
        if(nt != 17)
            throw runtime_error(
                str(format("Failed to parse parameter set in Tersoff potential file %1%: %2%") % filename % line));

        // Check if parameter values are valid.
        if(c < 0.0 || d < 0.0 || powern < 0.0 || beta2 < 0.0 || lam2 < 0.0 || bigb < 0.0 || bigr < 0.0 || bigd < 0.0 ||
           bigd > bigr || lam1 < 0.0 || biga < 0.0 || powerm - (int)powerm != 0.0 || (powerm != 3 && powerm != 1) || gamma < 0.0)
            throw runtime_error(
                str(format("Invalid parameter set for element triplet %2% %3% %4% in Tersoff potential file: %1%") % filename %
                    ename_i % ename_j % ename_k));

        // If all 3 elements are in atom type list, then use this line; otherwise skip to next line.
        int itype = -1, jtype = -1, ktype = -1;
        for(int i = 0; i < job()->numAtomTypes(); i++) {
            if(job()->atomTypeName(i) == ename_i) itype = i;
            if(job()->atomTypeName(i) == ename_j) jtype = i;
            if(job()->atomTypeName(i) == ename_k) ktype = i;
        }
        if(itype == -1 || jtype == -1 || ktype == -1) continue;

        // Store parameter set.
        ABOParamSet& triplet = _params[itype * _numAtomTypes * _numAtomTypes + jtype * _numAtomTypes + ktype];

        double r0, D0, beta, S, h_abop;
        h_abop = -h;
        r0 = (1.0 / (lam1 - lam2) * log((lam1 * biga) / (lam2 * bigb)));
        D0 = (biga * ((lam1 / lam2) - 1.0) * exp(-lam1 / (lam1 - lam2) * log((lam1 * biga) / (lam2 * bigb))));
        beta = (lam1 / sqrt(2.0 * (lam1 / lam2)));
        S = lam1 / lam2;

        triplet.setInitialParams(r0, D0, beta, S, gamma, c, d, h_abop, lam3, beta2, powern, powerm, bigd, bigr);
    }
}
}
