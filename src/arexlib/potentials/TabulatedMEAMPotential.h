///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Potential.h"
#include "functions/CubicSpline.h"

namespace atomicrex {

/**
 * Spline-based Modified Embedded Atom Method (MEAM) potential.
 *
 * Hennig et al.
 * Phys Rev B (2008) 78, 054121
 */
class TabulatedMEAMPotential : public Potential
{
public:
    /// Constructor.
    TabulatedMEAMPotential(const FPString& id, FitJob* job);

    /// Returns the maximum cutoff of the potential.
    virtual double cutoff() const override { return _cutoff; }

    /// Computes the total energy and forces of the structure.
    virtual double computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Computes the total energy of the structure.
    virtual double computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Writes the potential's parameters to the given output file.
    void writeParamsFile(const FPString& filename) const;

    /// Returns the number of bytes the potential needs per atom to store its intermediate
    /// calculation results during energy/force calculation.
    virtual size_t perAtomDataSize() const override { return sizeof(double); }

    /// Returns the number of bytes the potential needs per bond to store its intermediate
    /// calculation results during energy/force calculation.
    virtual size_t perBondDataSize() const override { return sizeof(MEAMBondData); }

    /// Parses the MEAM spline functionals from the given parameters file.
    void parseMEAMFile(const FPString& filename);

public:
    /// Parses any potential-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element potentialElement) override;

protected:
    /// Parses a single spline definition from the potential file.
    void parseSplineDefinition(CubicSpline& spline, std::istream& stream);

    /// Writes a single spline definition to the output potential file.
    void writeSplineDefinition(const CubicSpline& spline, std::ostream& stream) const;

protected:
    CubicSpline phi;          // Phi(r_ij)
    CubicSpline rho;          // Rho(r_ij)
    CubicSpline f;            // f(r_ij)
    CubicSpline U;            // U(rho)
    CubicSpline g;            // g(cos_theta)
    double zero_atom_energy;  // Embedding energy function is shifted by this value to make it vanish in vacuum.

    double _cutoff;  // The local cutoff radius.

    // Helper data structure used to store temporary per-bond values during energy/force calculation.
    struct MEAMBondData {
        double f, fprime;
    };
};

}  // End of namespace
