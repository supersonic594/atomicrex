///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AtomicStructure.h"
#include "../dof/DegreeOfFreedom.h"
#include "../properties/FitProperty.h"

/**
   @brief This file defines the SuperCellStructure class.
 */

namespace atomicrex {

/**
   @brief This class defines a N x N x N supercell structure.
   @details The class currently supports FCC, HCP, and BCC lattice and exposes a degree of freedom for the lattice constant and
   the c/a ratio in the case of HCP.
   The following code snippet exemplifies the definition of the super cell structure in the input file. The lattice parameter is
   typically slaved
   to the computed equilibrium lattice parameter of a primitive lattice structure using an 'equalto' reference.
   @code
   <super-cell id="MY_STRUCTURE_NAME">
     <atom-type-A> ELEMENT_NAME_1 </atom-type-A>
     <lattice-type> LATTICE_TYPE </lattice-type>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
     <cell-size> CELL_SIZE </cell-size>
     <properties>
       <lattice-parameter equalto="REFERNCE_STRUCTURE_ID.lattice-parameter"/>
     </properties>
   </super-cell>
   @endcode
*/
class SuperCellStructure : public AtomicStructure
{
public:
    /**
    * @brief The list of lattice types supported by this class.
    */
    enum LatticeType
    {
        LATTICE_FCC,
        LATTICE_HCP,
        LATTICE_BCC
    };

    /// Constructor.
    SuperCellStructure(const FPString& id, FitJob* job);

    /// Updates the structure (atom positions, simulation cell, etc.)
    virtual void updateStructure() override;

    /// Parses any structure-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element structureElement) override;

protected:
    /// Returns the geometry of the selected lattice unit cell.
    Matrix3 unitCell() const;

    /// Returns the geometry of the super cell.
    Matrix3 superCell() const;

    /// Returns the atoms in the lattice unit cell (reduced coordinates).
    std::vector<std::pair<Point3, int>> unitCellBasis() const;

    /// Builds a list of atoms in the super cell.
    std::vector<std::pair<Point3, int>> superCellAtoms() const;

private:
    /// The type of lattice.
    LatticeType _latticeType = LATTICE_FCC;

    /// The super cell size (N x N x N).
    int _cellSize = 1;

    /// DOF that controls the lattice parameter.
    ScalarDOF _latticeParameter;

    /// DOF that controls the c/a ration of hexagonal lattices.
    ScalarDOF _CtoAratio;

    /// This property allows to compute the relaxed lattice parameter.
    CoupledFitProperty _latticeParameterProperty;

    /// This property allows to compute the relaxed c/a ratio of hexagonal lattices.
    CoupledFitProperty _CtoAratioProperty;

protected:
    /// The atom type of the lattice.
    int _atomTypeA = 1;
};

}  // End of namespace
