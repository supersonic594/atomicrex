///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "DimerStructure.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor.
******************************************************************************/
DimerStructure::DimerStructure(const FPString& id, FitJob* job)
    : AtomicStructure(id, job), _atomDistance("atom-distance"), _atomDistanceProperty(_atomDistance, "A", job)
{
    registerDOF(&_atomDistance);
    registerProperty(&_atomDistanceProperty);
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void DimerStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Setup primitive lattice cell.
        setupSimulationCell(Matrix3(5 * _atomDistance, 0.0, 0.0, 0.0, 1 * _atomDistance, 0.0, 0.0, 0.0, 1 * _atomDistance),
                            Point3::Origin(), std::array<bool, 3>{{false, false, false}});

        // Create two atoms.
        setAtomCount(2);
        atomPositions()[0] = Point3(0, 0, 0);
        atomPositions()[1] = Point3(_atomDistance, 0, 0);
        atomTypes()[0] = _atomTypeA;
        atomTypes()[1] = _atomTypeB;
        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    AtomicStructure::updateStructure();
}

/******************************************************************************
* Parses any structure-specific parameters in the XML element in the job file.
******************************************************************************/
void DimerStructure::parse(XML::Element structureElement)
{
    AtomicStructure::parse(structureElement);

    // Parse parameters.
    _atomTypeA = job()->parseAtomTypeElement(structureElement, "atom-type-a");
    _atomTypeB = job()->parseAtomTypeElement(structureElement, "atom-type-b");
    _atomDistance.setInitialValue(structureElement.parseFloatParameterElement("atom-distance"));
}
}
