///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "NeighborList.h"
#include "AtomicStructure.h"
#include "../job/FitJob.h"
#include "../potentials/Potential.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Generates the neighbor list.
******************************************************************************/
void NeighborList::build(const AtomicStructure& structure, const Potential& potential, double cutoffRange)
{
    int nlocal = structure.numLocalAtoms();
    int ntotal = structure.numAtoms();

    _ilist.clear();
    _numNeighborsHalf.resize(nlocal);
    _numNeighborsFull.resize(nlocal);
    _firstNeighbor.resize(nlocal);
    _neighborList.clear();
    double cutoff2 = square(cutoffRange);
    for(int i = 0; i < nlocal; i++) {
        _numNeighborsHalf[i] = 0;
        _numNeighborsFull[i] = 0;
        _firstNeighbor[i] = _neighborList.size();
        _ilist.push_back(i);
        for(int pass = 0; pass < 2; pass++) {
            for(int j = 0; j < ntotal; j++) {
                if(j == i) continue;

                NeighborListEntry n;
                n.delta = structure.atomPositions()[j] - structure.atomPositions()[i];

                int itag = structure.atomTags()[i];
                int jtag = structure.atomTags()[j];
                if(itag > jtag) {
                    if((itag + jtag) % 2 == pass) continue;
                }
                else if(itag < jtag) {
                    if((itag + jtag) % 2 == 1 - pass) continue;
                }
                else if(pass == 0) {
                    if(n.delta.z() < 0.0) continue;
                    if(n.delta.z() == 0.0 && n.delta.y() < 0.0) continue;
                    if(n.delta.z() == 0.0 && n.delta.y() == 0.0 && n.delta.x() < 0.0) continue;
                }
                else {
                    if(n.delta.z() > 0.0) continue;
                    if(n.delta.z() == 0.0 && n.delta.y() > 0.0) continue;
                    if(n.delta.z() == 0.0 && n.delta.y() == 0.0 && n.delta.x() > 0.0) continue;
                }

                double rsq = n.delta.squaredLength();
                BOOST_ASSERT(rsq != 0.0);
                if(rsq < cutoff2) {
                    n.r = sqrt(rsq);
                    n.index = j;
                    if(j < nlocal)
                        n.localIndex = j;
                    else
                        n.localIndex = structure.reverseMapping()[j - nlocal];
                    n.bondDataPtr = nullptr;
                    _neighborList.push_back(n);
                    _numNeighborsFull[i]++;
                    if(pass == 0) _numNeighborsHalf[i]++;
                }
            }
        }
    }

    // Allocate per-bond data memory.
    // Every neighbor list entry stores a pointer to its per-bond memory.
    size_t bondDataSize = potential.perBondDataSize();
    _perBondPotentialData.resize(bondDataSize * _neighborList.size());
    if(bondDataSize != 0 && !_neighborList.empty()) {
        unsigned char* p = &_perBondPotentialData.front();
        for(NeighborListEntry& ne : _neighborList) {
            ne.bondDataPtr = p;
            p += bondDataSize;
        }
    }

    _numAtoms = _ilist.size();
}

/******************************************************************************
* Updates the stored list of interatomic vectors.
* This is called when the atoms of the structure have (slightly) moved or
* if the simulation cell geometry has (slightly) changed.
******************************************************************************/
void NeighborList::update(const AtomicStructure& structure)
{
    BOOST_ASSERT(_ilist.size() == _numNeighborsFull.size());
    auto nle = _neighborList.begin();
    auto i = _ilist.cbegin();
    for(auto nc = _numNeighborsFull.cbegin(); nc != _numNeighborsFull.cend(); ++nc, ++i) {
        const Point3& p0 = structure.atomPositions()[*i];
        for(int counter = *nc; counter != 0; counter--, ++nle) {
            nle->delta = structure.atomPositions()[nle->index] - p0;
            nle->r = nle->delta.length();
        }
    }
    BOOST_ASSERT(nle == _neighborList.end());
}
}
