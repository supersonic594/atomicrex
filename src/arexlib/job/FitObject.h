///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

class FitJob;           // Declared in FitJob.h
class DegreeOfFreedom;  // Declared in DegreeOfFreedom.h
class FitProperty;      // Declared in FitProperty.h

/**
 * This is the abstract base class for potentials, atomic structures, properties and groups.
 *
 * A FitObject has
 *  (a) degrees of freedom that are varied during the fitting process.
 *  (b) properties that are fitted to a target value during the fitting process.
 *  (c) sub-objects which in turn can have properties and DOFs.
 *
 */
class FitObject : boost::noncopyable
{
protected:
    /// Default Constructor.
    FitObject() {}

    /// Constructor.
    FitObject(const FPString& id, FitJob* job, const FPString& tag = FPString()) : _id(id), _job(job), _tag(tag) {}

public:
    /// Virtual destructor.
    virtual ~FitObject() = default;

    /************************************ Weights **************************************/

    /// Returns the relative fit weight assigned to this object.
    double relativeWeight() const { return _relativeWeight; }

    /// Assigns a relative fit weight to this object.
    void setRelativeWeight(double weight) { _relativeWeight = weight; }

    /// Recursively assigns absolute weights to the properties of this object and its sub-objects.
    virtual void assignAbsoluteWeights(double absoluteWeight);

    /*********************************** Properties ***********************************/

    /// Computes all enabled properties of the object.
    virtual bool computeProperties(bool isFitting) { return true; }

    /// Returns a list of fitting properties of this object.
    const std::vector<FitProperty*>& properties() const { return _fitPropertiesList; }

    /// Builds a list of properties of this object and all its sub-objects.
    void listAllProperties(std::vector<FitProperty*>& list) const;

    /// Returns the property with the given ID.
    FitProperty* propertyById(const FPString& id) const;

    /************************************** DOFs ***************************************/

    /// Returns a list of degrees of freedom of this object.
    const std::vector<DegreeOfFreedom*>& DOF() const { return _dofList; }

    /// Builds a list of degrees of freedom of this object and all its sub-objects.
    void listAllDOF(std::vector<DegreeOfFreedom*>& list) const;

    /// Returns the degree of freedom with the given ID (and tag).
    DegreeOfFreedom* DOFById(const FPString& id, const FPString& tag = FPString()) const;

    /// This callback function is called by the DOFs of this fit object each time when their values changes.
    virtual void dofValueChanged(DegreeOfFreedom& dof) {}

    /*************************************** Hierarchy **********************************/

    /// Returns the list of FitObjects which are part of this group.
    const std::vector<FitObject*>& fitObjects() const { return _subobjectList; }

    /// Registers a sub-object.
    void registerSubObject(FitObject* subobject, bool deleteOnShutdown = false);

    /*************************************** I/O ****************************************/

    /// Outputs the name of the object.
    virtual void print(MsgLogger& stream)
    {
        if(_parent) {
            _parent->print(stream);
            stream << ".";
        }
        stream << id();
        if(tag().empty() == false) stream << "[" << tag() << "]";
    }

    /// Parses the base parameters, common to structures, groups, and potentials, from the XML element in the job file.
    virtual void parse(XML::Element element);

    /************************************* Attributes ***********************************/

    /// Returns whether this object and it's children are included in the fit.
    bool fitEnabled() const { return _fitEnabled; }

    /// Sets whether this object and it's children are included in the fit.
    void setFitEnabled(bool enable) { _fitEnabled = enable; }

    /// Returns whether this object and it's children are calculated and displayed
    /// at the end of the fitting process.
    bool outputEnabled() const { return _outputEnabled; }

    /// Sets whether this object and it's children are calculated and displayed
    /// at the end of the fitting process.
    void setOutputEnabled(bool enable) { _outputEnabled = enable; }

    /// Returns the identifier of this object instance.
    const FPString& id() const { return _id; }

    /// Sets the main identification tag.
    void setId(const FPString& id) { _id = id; }

    /// Returns the assigned tag string.
    const FPString& tag() const { return _tag; }

    /// Sets the complementary identification tag.
    void setTag(const FPString& tag) { _tag = tag; }

    /// Returns a pointer to the job to which this object belongs.
    FitJob* job() const { return _job; }

    /// Returns the parent of this object in the hierarchy.
    FitObject* parent() const { return _parent; }

protected:
    /// Registers a property of this object.
    void registerProperty(FitProperty* prop, bool deleteOnShutdown = false);

    /// Registers a DOF of this object.
    void registerDOF(DegreeOfFreedom* dof);

protected:
    /// Controls whether this object and it's children are included in the fit.
    bool _fitEnabled = true;

    /// Controls whether this object and it's children are calculated and displayed
    /// at the end of the fitting process.
    bool _outputEnabled = true;

    /// Pointer to the job this object belongs to.
    FitJob* _job = nullptr;

    /// The identifier string of this object instance.
    FPString _id;

    /// The relative fit weight assigned to this object.
    double _relativeWeight = 1.0;

private:
    /// An additional tag that might be assigned to this object.
    FPString _tag;

    /// Lists the degrees of freedom of this object.
    std::vector<DegreeOfFreedom*> _dofList;

    /// Lists the fitting properties of this object.
    std::vector<FitProperty*> _fitPropertiesList;

    /// Lists the sub-objects of this fitting object.
    std::vector<FitObject*> _subobjectList;

    /// Lists the sub-objects that are deleted when this object is deleted.
    /// Includes structures, derived properties and subgroups but no structure properties.
    std::vector<std::unique_ptr<FitObject>> _ownedObjects;

    /// The parent of this object in the hierarchy.
    FitObject* _parent = nullptr;
};

}  // End of namespace
