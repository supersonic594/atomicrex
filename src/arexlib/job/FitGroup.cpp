///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "FitGroup.h"
#include "FitJob.h"
#include "../properties/FitProperty.h"
#include "../properties/DerivedProperty.h"
#include "../structures/AtomicStructure.h"
#include "../structures/UserStructure.h"
#include "../structures/LatticeStructures.h"
#include "../structures/DimerStructure.h"
#include "../structures/PointDefectStructure.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor
******************************************************************************/
FitGroup::FitGroup(const FPString& id, FitJob* job) : FitObject(id, job) {}

/******************************************************************************
* Creates a new child structure group.
******************************************************************************/
FitGroup* FitGroup::createSubGroup(const FPString& id, FitJob* job)
{
    FitGroup* subgroup = new FitGroup(id, job);
    registerSubObject(subgroup, true);
    return subgroup;
}

/******************************************************************************
* Parses the contents of the <structures>/<group> elements in the job file.
******************************************************************************/
void FitGroup::parseElement(XML::Element groupElement, FitJob* job)
{
    // Call base class.
    FitObject::parse(groupElement);

    // Parse list of structures.
    for(XML::Element structureElement = groupElement.firstChildElement(); structureElement;
        structureElement = structureElement.nextSibling()) {
        if(structureElement.tagEquals("group")) {
            // Create a new subgroup.
            FitGroup* subgroup = createSubGroup(structureElement.parseOptionalStringParameterAttribute("id"), job);
            // Inherit settings from parent group.
            subgroup->setOutputEnabled(this->outputEnabled());
            subgroup->setFitEnabled(this->fitEnabled());
            subgroup->parseElement(structureElement, job);
        }
        else if(structureElement.tagEquals("derived-property")) {
            // Create new property
            FPString propertyId = structureElement.parseStringParameterAttribute("id");
            FPString propertyUnit = structureElement.parseStringParameterAttribute("unit");
            std::unique_ptr<DerivedProperty> property(new DerivedProperty(propertyId, propertyUnit, job));

            // Parse structure-specific part.
            property->parse(structureElement);

            // Disable output of derived property if group (or one of it's parents) is disabled.
            if(!outputEnabled()) {
                property->setOutputEnabled(false);
            }

            // Disable output of derived property if group (or one of it's parents) is disabled.
            if(!fitEnabled()) {
                property->setFitEnabled(false);
            }

            // Add the new property object to the group.
            registerProperty(property.release(), true);
        }
        else {
            std::unique_ptr<AtomicStructure> structure;
            FPString structureId = structureElement.parseStringParameterAttribute("id");
            if(structureElement.tagEquals("user-structure"))
                structure.reset(new UserStructure(structureId, job));
            else if(structureElement.tagEquals("dimer-structure"))
                structure.reset(new DimerStructure(structureId, job));
            else if(structureElement.tagEquals("fcc-lattice") || structureElement.tagEquals("A1-lattice"))
                structure.reset(new FCCLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("bcc-lattice") || structureElement.tagEquals("A2-lattice"))
                structure.reset(new BCCLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("hcp-lattice") || structureElement.tagEquals("A3-lattice"))
                structure.reset(new HCPLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("dhcp-lattice"))
                structure.reset(new DHCPLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("betaSn-lattice") || structureElement.tagEquals("A5-lattice"))
                structure.reset(new betaSnLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("sc-lattice") || structureElement.tagEquals("Ah-lattice"))
                structure.reset(new SCLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("diamond-lattice") || structureElement.tagEquals("A4-lattice"))
                structure.reset(new DIALatticeStructure(structureId, job));
            else if(structureElement.tagEquals("omega-lattice"))
                structure.reset(new OMGLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("B1-lattice") || structureElement.tagEquals("CsCl-lattice"))
                structure.reset(new B1LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("B2-lattice") || structureElement.tagEquals("NaCl-lattice"))
                structure.reset(new B2LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("B3-lattice") || structureElement.tagEquals("zincblende"))
                structure.reset(new B3LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("B4-lattice") || structureElement.tagEquals("wurtzite"))
                structure.reset(new B4LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("Bh-lattice") || structureElement.tagEquals("WC-lattice"))
                structure.reset(new BhLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("C1-lattice") || structureElement.tagEquals("fluorite-lattice"))
                structure.reset(new C1LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("C15-lattice"))
                structure.reset(new C15LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("D2d-lattice"))
                structure.reset(new D2dLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("Ni17Th2-lattice"))
                structure.reset(new Ni17Th2LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("Th2Zn17-lattice"))
                structure.reset(new Th2Zn17LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("D8a-lattice"))
                structure.reset(new D8aLatticeStructure(structureId, job));
            else if(structureElement.tagEquals("L12-lattice"))
                structure.reset(new L12LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("L10-lattice"))
                structure.reset(new L10LatticeStructure(structureId, job));
            else if(structureElement.tagEquals("super-cell"))
                structure.reset(new SuperCellStructure(structureId, job));
            else if(structureElement.tagEquals("point-defect"))
                structure.reset(new PointDefectStructure(structureId, job));
            else
                throw runtime_error(str(format("Unknown structure type <%1%> in line %2% of XML file.") % structureElement.tag() %
                                        structureElement.lineNumber()));

            // Parse structure-specific part.
            structure->parse(structureElement);

            // Disable output of structure and all properties if group (or one of it's parents) is disabled.
            if(!outputEnabled()) {
                structure->setOutputEnabled(outputEnabled());
                for(FitProperty* prop : structure->properties()) prop->setOutputEnabled(outputEnabled());
            }

            // Disable fit of structure and all properties if group (or one of it's parents) is disabled.
            if(!fitEnabled()) {
                structure->setFitEnabled(fitEnabled());
                for(FitProperty* prop : structure->properties()) prop->setFitEnabled(fitEnabled());
            }

            // Update structure for the first time.
            structure->updateStructure();

            // Add the new structure to the group.
            registerSubObject(structure.release(), true);
        }
    }
}
}
