///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "FitProperty.h"
#include "../util/xml/XMLUtilities.h"
#include <muParser.h>

namespace atomicrex {

/**
 * This type of scalar property is not tied to any structure. Its value is
 * computed by a user-defined formula, which can contain references to structure properties.
 *
 * This allows fitting on derived properties like defect formation energies,
 * binding energies between defects, surface energies etc.
 */
class DerivedProperty : public ScalarFitProperty
{
public:
    /// Constructor.
    DerivedProperty(FPString id, FPString unit, FitJob* job);

    /// Lets the object compute the current value of the property.
    virtual void compute() override;

    /// Parse the contents of the <property> element in the job file.
    virtual void parse(XML::Element propertyElement) override;

private:
    /// The property expression parser and evaluator.
    mu::Parser _parser;

    /// The input properties this property depends on.
    std::vector<ScalarFitProperty*> _inputProperties;

    /// The array that stores the current values of the input properties.
    std::vector<double> _values;
};

}  // End of namespace
