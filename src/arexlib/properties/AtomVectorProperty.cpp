///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomVectorProperty.h"
#include "../dof/DegreeOfFreedom.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
 * Returns the residual of this property used for fitting.
 ******************************************************************************/
double AtomVectorProperty::residual(ResidualNorm norm) const
{
    BOOST_ASSERT(_computedValues.size() == _targetValues.size());

    if(!_hasTargetValues)
        throw runtime_error(
            str(format("Cannot fit atomic vector for structure '%1%'. Target values have not been specified.") % id()));

    if(norm == UserDefined) norm = _residualNorm;

    double res = 0;
    if(norm == SquaredRelative) {
        throw runtime_error(str(format("The residual style 'squared-relative'"
                                       " is currently not allowed for vector"
                                       " properties (structure '%1%'") %
                                id()));
    }
    else if(norm == Squared) {
        auto c = _computedValues.cbegin();
        for(auto t = _targetValues.cbegin(); t != _targetValues.cend(); ++t, ++c) res += (*c - *t).squaredLength();
        res /= tolerance() * tolerance();
    }
    else if(norm == AbsoluteDiff) {
        auto c = _computedValues.cbegin();
        for(auto t = _targetValues.cbegin(); t != _targetValues.cend(); ++t, ++c) {
            res += std::abs(c->x() - t->x());
            res += std::abs(c->y() - t->y());
            res += std::abs(c->z() - t->z());
        }
        res /= tolerance();
    }
    else
        throw runtime_error(str(format("Residual norm of atomic property '%1%' must be 'squared' or 'absolute-diff'.") % id()));

    return res;
}

/******************************************************************************
 * Print the residual of this property normalized by vector size. Instead of
 * printing all values of the vector averages are used to represent the result.
 ******************************************************************************/
void AtomVectorProperty::print(MsgLogger& stream)
{
    if(!stream.isEnabled()) return;

    BOOST_ASSERT(_computedValues.size() == _targetValues.size());
    if(_computedValues.empty()) return;

    stream << id() << " avg/max: ";

    // Compute average and maximum magnitude of computed vectors.
    double avgComputedValues = 0;
    double maxComputedValues = 0;
    for(const Vector3& v : _computedValues) {
        avgComputedValues += std::abs(v.x()) + std::abs(v.y()) + std::abs(v.z());
        maxComputedValues = std::max(std::abs(v.x()), maxComputedValues);
        maxComputedValues = std::max(std::abs(v.y()), maxComputedValues);
        maxComputedValues = std::max(std::abs(v.z()), maxComputedValues);
    }
    avgComputedValues /= 3 * _computedValues.size();
    stream << avgComputedValues << " " << maxComputedValues << " " << units();

    if(fitEnabled() && _hasTargetValues) {
        // Compute average of target values.
        double avgTargetValues = 0;
        double maxTargetValues = 0;
        for(const Vector3& v : _targetValues) {
            avgTargetValues += std::abs(v.x()) + std::abs(v.y()) + std::abs(v.z());
            maxTargetValues = std::max(std::abs(v.x()), maxTargetValues);
            maxTargetValues = std::max(std::abs(v.y()), maxTargetValues);
            maxTargetValues = std::max(std::abs(v.z()), maxTargetValues);
        }
        avgTargetValues /= 3 * _targetValues.size();
        stream << " (target: " << avgTargetValues << " " << maxTargetValues << " " << units();

        // Absolute weight and tolerance.
        stream << "; Abs. weight: " << absoluteWeight();
        stream << "; Tolerance: " << tolerance();

        // Also print normalized residual for comparison.
        double avgres = sqrt(residual(UserDefined) / _targetValues.size());
        stream << "; Normalized residual: " << avgres << " " << units() << " )";
        stream << "; Weighted residual: " << residual(_residualNorm) * absoluteWeight();
    }

    if(outputAllEnabled()) {
        stream << endl;

        // Print all computed values (and if available also all target values).
        int n = 0;
        vector<Vector3>::const_iterator t = _targetValues.begin();
        for(const Vector3& c : _computedValues) {
            stream << "     " << id() << ": " << n++ << " " << c;
            if(fitEnabled() && _hasTargetValues) {
                stream << "  target: " << *t << " diff: " << (c - *t);
                ++t;
            }
            stream << endl;
        }
    }
}

/******************************************************************************
 * Parse the contents of the <property> element in the job file.
 ******************************************************************************/
void AtomVectorProperty::parse(XML::Element propertyElement)
{
    FitProperty::parse(propertyElement);

    if(propertyElement.parseOptionalBooleanParameterAttribute("output-all", false)) {
        setOutputEnabled(true);
        setOutputAllEnabled(true);
    }
}
};
