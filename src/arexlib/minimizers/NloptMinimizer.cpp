///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "NloptMinimizer.h"
#include "../job/FitJob.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Wrapping function for the objective function.
******************************************************************************/
double obj_wrapper(const std::vector<double>& x, std::vector<double>& g, void* data)
{
    NloptMinimizer* obj = static_cast<NloptMinimizer*>(data);
    return obj->objective_function(x, g);
}

/******************************************************************************
* Constructor that copies another minimizer
******************************************************************************/
NloptMinimizer::NloptMinimizer(const NloptMinimizer& other)
    : Minimizer(other),
      nlopt_stopval(other.nlopt_stopval),
      nlopt_maxtime(other.nlopt_maxtime),
      nlopt_ftol_rel(other.nlopt_ftol_rel),
      nlopt_ftol_abs(other.nlopt_ftol_abs),
      nlopt_xtol_rel(other.nlopt_xtol_rel),
      seed(other.seed),
      alg(other.alg)
{
    if(other._submin) {
        _submin = unique_ptr<NloptMinimizer>{static_cast<NloptMinimizer*>(other._submin->clone().release())};
    }
}

/******************************************************************************
* Initializes the minimizer.
******************************************************************************/
void NloptMinimizer::prepare(std::vector<double>&& x0, const std::function<double(const std::vector<double>&)>& func,
                             const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient)
{
    Minimizer::prepare(std::move(x0), func, gradient);
    // _n = job()->numActiveDOF();
    x = std::move(x0);

    g.resize(x.size());
    // This assertion is not valid if we are doing structure optimization
    // BOOST_ASSERT(x.size() == _n);

    // Initialize opt Object
    opt.reset(new nlopt::opt(alg, x.size()));

    // Set Options
    if(nlopt_stopval != -1) opt->set_stopval(nlopt_stopval);
    opt->set_maxeval(_maximumNumberOfIterations);
    if(nlopt_maxtime != -1) opt->set_maxtime(nlopt_maxtime);
    if(nlopt_ftol_rel != -1) opt->set_ftol_rel(nlopt_ftol_rel);
    if(nlopt_ftol_abs != -1) opt->set_ftol_abs(nlopt_ftol_abs);
    if(nlopt_xtol_rel != -1) opt->set_xtol_rel(nlopt_xtol_rel);
    if(seed != 0) nlopt::srand(seed);

    if(_submin) {
        _submin->prepare(std::move(x0), func, gradient);
        opt->set_local_optimizer(*_submin->get_optimizer());
    }
    opt->set_min_objective(obj_wrapper, this);
}

/******************************************************************************
* Sets constraints for the variation of the parameters.
******************************************************************************/
void NloptMinimizer::setConstraints(std::vector<BoundConstraints>&& constraintTypes, std::vector<double>&& lowerBounds,
                                    std::vector<double>&& upperBounds)
{
    _constraintTypes = std::move(constraintTypes);
    _lowerBounds = std::move(lowerBounds);
    _upperBounds = std::move(upperBounds);

    opt->set_lower_bounds(_lowerBounds);
    opt->set_upper_bounds(_upperBounds);

    if(_submin) {
        _submin->setConstraints(std::vector<BoundConstraints>(_constraintTypes), std::vector<double>(_lowerBounds),
                                std::vector<double>(_upperBounds));
    }
}

/******************************************************************************
* Parses the minimizer's parameters from the XML file.
******************************************************************************/
void NloptMinimizer::parse(XML::Element minimizerElement)
{
    MsgLogger(debug) << "Parse NLopt minimizer parameters (base class Minimizer)." << endl;
    Minimizer::parse(minimizerElement);

    // Get Options
    MsgLogger(debug) << "Parse NLopt minimizer parameters." << endl;
    nlopt_stopval = minimizerElement.parseOptionalFloatParameterAttribute("stopval", nlopt_stopval);
    MsgLogger(debug) << "  nlopt_stopval: " << nlopt_stopval << endl;
    _maximumNumberOfIterations = minimizerElement.parseOptionalIntParameterAttribute("maxeval", _maximumNumberOfIterations);
    MsgLogger(debug) << "  nlopt_maxeval: " << _maximumNumberOfIterations << endl;
    nlopt_maxtime = minimizerElement.parseOptionalFloatParameterAttribute("maxtime", nlopt_maxtime);
    MsgLogger(debug) << "  nlopt_maxtime: " << nlopt_maxtime << endl;
    nlopt_ftol_rel = minimizerElement.parseOptionalFloatParameterAttribute("ftol_rel", nlopt_ftol_rel);
    MsgLogger(debug) << "  nlopt_ftol_rel: " << nlopt_ftol_rel << endl;
    nlopt_ftol_abs = minimizerElement.parseOptionalFloatParameterAttribute("ftol_abs", nlopt_ftol_abs);
    MsgLogger(debug) << "  nlopt_ftol_abs: " << nlopt_ftol_abs << endl;
    nlopt_xtol_rel = minimizerElement.parseOptionalFloatParameterAttribute("xtol_rel", nlopt_xtol_rel);
    MsgLogger(debug) << "  nlopt_xtol_rel: " << nlopt_xtol_rel << endl;
    seed = minimizerElement.parseOptionalIntParameterAttribute("seed", seed);
    MsgLogger(debug) << "  seed: " << seed << endl;

    // Get Algorithm
    _name = minimizerElement.parseStringParameterAttribute("algorithm");

    if(_name == "G_MLSL_LDS" || _name == "G_MLSL") {
        XML::Element sub = minimizerElement.firstChildElement();
        if(!sub)
            throw runtime_error(str(format("<%1%> element in line %2% of XML file must contain a sub-minimizer.") %
                                    minimizerElement.tag() % minimizerElement.lineNumber()));
        // _submin = dynamic_pointer_cast<NloptMinimizer>(createAndParse(sub, job()));
        _submin =
            unique_ptr<NloptMinimizer>{static_cast<NloptMinimizer*>(createAndParse(sub, job(), 0, _isFitMinimizer).release())};
        if(!_submin)
            throw runtime_error(str(format("<%1%> element in line %2% of XML file must contain a nlopt sub-minimizer.") %
                                    minimizerElement.tag() % minimizerElement.lineNumber()));
    }
    alg = get_algorithm(_name);
}

/******************************************************************************
* Get the Nlopt algorithm from string.
******************************************************************************/
nlopt::algorithm NloptMinimizer::get_algorithm(FPString s)
{
    nlopt::algorithm nalg;
    // Local, gradient based optimization algorithms
    if(s == "LD_LBFGS")
        nalg = nlopt::LD_LBFGS;
    else if(s == "LD_MMA")
        nalg = nlopt::LD_MMA;
    else if(s == "LD_CCSAQ")
        nalg = nlopt::LD_CCSAQ;
    else if(s == "LD_SLSQP")
        nalg = nlopt::LD_SLSQP;
    else if(s == "LD_VAR1")
        nalg = nlopt::LD_VAR1;
    else if(s == "LD_VAR2")
        nalg = nlopt::LD_VAR2;
    // Local, derivative free algorithms
    else if(s == "LN_COBYLA")
        nalg = nlopt::LN_COBYLA;
    else if(s == "LN_BOBYQA")
        nalg = nlopt::LN_BOBYQA;
    else if(s == "LN_NEWUOA")
        nalg = nlopt::LN_NEWUOA;
    else if(s == "LN_NEWUOA_BOUND")
        nalg = nlopt::LN_NEWUOA_BOUND;
    else if(s == "LN_PRAXIS")
        nalg = nlopt::LN_PRAXIS;
    else if(s == "LN_NELDERMEAD")
        nalg = nlopt::LN_NELDERMEAD;
    else if(s == "LN_SBPLX")
        nalg = nlopt::LN_SBPLX;
    // Global algorithms
    else if(s == "GN_CRS2_LM")
        nalg = nlopt::GN_CRS2_LM;
    else if(s == "GN_ESCH")
        nalg = nlopt::GN_ESCH;
    else if(s == "GN_DIRECT")
        nalg = nlopt::GN_DIRECT;
    else if(s == "GN_DIRECT_L")
        nalg = nlopt::GN_DIRECT_L;
    else if(s == "GN_ISRES")
        nalg = nlopt::GN_ISRES;
    else if(s == "G_MLSL")
        nalg = nlopt::G_MLSL;
    else if(s == "G_MLSL_LDS")
        nalg = nlopt::G_MLSL_LDS;
    else
        throw runtime_error(str(format("<%1%> is no valid Nlopt minimizer or is not implemented yet.") % s));
    return nalg;
}

/******************************************************************************
* The objective function to minimize.
******************************************************************************/
double NloptMinimizer::objective_function(const std::vector<double>& x, std::vector<double>& g)
{
    // Do not set f directly to the current residual. Returning temporary variable seems to be necessary.
    double residual;
    BOOST_ASSERT(_func);
    if(!g.empty()) {
        // We are using a gradient based algorithm
        if(_gradientFunc)
            // We have a gradient function (relaxation)
            residual = _gradientFunc(x, g);
        else
            // We have no gradient function (parameter optimization)
            residual = numericGradient(x, g);

        // calculate grad norm
        _gradNorm2 = 0;
        for(vector<double>::const_iterator g_iter = g.begin(); g_iter != g.end(); ++g_iter) _gradNorm2 += square(*g_iter);
        _gradNorm2 = sqrt(_gradNorm2);
    }
    else {
        // We are using a gradient free algorithm
        residual = _func(x);
    }

    _itercount++;

    // Remember the best results
    if(residual < _bestResidual) {
        _bestResidual = residual;
        _bestParameters = x;
    }

    // Output current step
    printStep(residual);

    return residual;
}

/******************************************************************************
* Performs one minimization iteration.
******************************************************************************/
Minimizer::MinimizerResult NloptMinimizer::iterate()
{
    result = opt->optimize(x, f);
    FPString result_string;
    if(result == 1)
        result_string = "NLOPT_SUCCESS";
    else if(result == 2)
        result_string = "NLOPT_STOPVAL_REACHED";
    else if(result == 3)
        result_string = "NLOPT_FTOL_REACHED";
    else if(result == 4)
        result_string = "NLOPT_XTOL_REACHED";
    else if(result == 5)
        result_string = "NLOPT_MAXEVAL_REACHED";
    else if(result == 6)
        result_string = "NLOPT_MAXTIME_REACHED";
    else if(result == -1)
        result_string = "NLOPT_FAILURE";
    else if(result == -2)
        result_string = "NLOPT_INVALID_ARGS";
    else if(result == -3)
        result_string = "NLOPT_OUT_OF_MEMORY";
    else if(result == -4)
        result_string = "NLOPT_ROUNDOFF_LIMITED";
    else if(result == -5)
        result_string = "NLOPT_FORCED_STOP";
    if(_isFitMinimizer)
        MsgLogger(medium) << "  " << _name << " stopped with return value: " << result_string << ", Residual: " << _bestResidual
                          << endl;
    if(result > 0)
        return MINIMIZATION_CONVERGED;
    else
        return MINIMIZATION_ERROR;
}

/******************************************************************************
* Function that creates a copy of this minimizer.
******************************************************************************/
std::unique_ptr<Minimizer> NloptMinimizer::clone() { return std::unique_ptr<NloptMinimizer>(new NloptMinimizer(*this)); }
}
