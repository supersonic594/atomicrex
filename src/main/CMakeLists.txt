###############################################################################
# 
#  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
#
#  This file is part of atomicrex.
#
#  Atomicrex is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Atomicrex is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Honor visibility properties for all target types.
# This is needed so we can set the symbol visibility of the compile target to hidden below. 
CMAKE_POLICY(SET CMP0063 NEW)

# Generate standalone Atomicrex executable.
ADD_EXECUTABLE(atomicrex Main.cpp)
TARGET_LINK_LIBRARIES(atomicrex PRIVATE atomicrexlib)
SET_TARGET_PROPERTIES(atomicrex PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")

# Compile with symbols hidden by default. This is needed because the atomicrex lib is compiled in the same way.
SET_TARGET_PROPERTIES(atomicrex PROPERTIES CXX_VISIBILITY_PRESET "hidden")
