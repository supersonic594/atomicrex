from __future__ import division

import numpy as np

from ase.calculators.calculator import Calculator, all_changes
from ._atomicrex import Job
from ase.units import GPa
import os


class Atomicrex(Calculator):
    implemented_properties = ["energy", "forces", "stress"]
    default_parameters = {}

    def __init__(self, file_name, **kwargs):
        # initialize job and parse parameter file
        self.job = Job()
        self.job.parse_input_file(file_name)
        Calculator.__init__(self, **kwargs)

    def set_atoms(self, atoms):
        """Add atoms object to the calculator.

        Args:
            atoms Atoms object that gets attached to the calculator.
        """
        self.reset()
        self.atoms = atoms.copy()
        if self.job.structures:
            # Update the complete default structure
            structure = self.job.structures["default"]
            # number of atoms
            structure.local_atom_count = len(atoms)
            # atom positions
            structure.set_positions(atoms.get_positions())
            # atom types
            types = self.job.types
            structure.types[:] = [types.index(s)
                                  for s in atoms.get_chemical_symbols()]
            # boundry conditions
            pbc = [bool(p) for p in atoms.get_pbc()]
            # simulation cell
            structure.setup_simulation_cell(atoms.get_cell().T, (0, 0, 0), pbc)
            structure.update_structure()
        else:
            structure = self.job.add_ase_structure("default", atoms)

    def calculate(self, atoms=None,
                  properties=["energy"],
                  system_changes=all_changes):
        """Calculate energies and other properties.

        Args:
            atoms Atoms object that gets attached to the calculator.
            properties Properties that are calculated.
            system_changes Changes to the structure that need to be updated.
        """

        # Call base class to get system_changes etc.
        Calculator.calculate(self, atoms, properties, system_changes)

        # update/add the atomicrex structure
        if self.job.structures:
            structure = self.job.structures["default"]
            if "numbers" in system_changes:
                structure.local_atom_count = len(atoms)
            if "positions" in system_changes:
                structure.set_positions(atoms.get_positions())
            if "cell" in system_changes or "pbc" in system_changes:
                # For some reason only using atoms.get_pbc() does not work!
                pbc = [bool(p) for p in atoms.get_pbc()]
                structure.setup_simulation_cell(atoms.get_cell().T,
                                                (0, 0, 0), pbc)
            structure.update_structure()
        else:
            structure = self.job.add_ase_structure("default", atoms)

        # calculate energy and/or forces
        if "forces" in properties or "stress" in properties:
            structure.compute_energy(compute_forces=True)

            # extract stress and forces from atomicrex structure
            forces = structure.get_forces()
            stress = []
            for i in range(6):
                stress.append(structure.get_pressure_tensor(i))

            # Convert Stress from bar (atomicrex output)
            # to eV/A^3 (ase calculator output) (and switch sign?!)
            self.results["forces"] = forces
            self.results["stress"] = np.array(stress) * (-1e-4 * GPa)
        else:
            structure.compute_energy(compute_forces=False)

        # Get the calculated energy from the atomicrex structure
        energy = structure.get_potential_energy()

        self.results["energy"] = energy
        self.results["free_energy"] = energy


class Abop(Atomicrex):

    def __init__(self, type_list, file_name, **kwargs):
        #  create temporary input file
        fname = "tmp.xml"
        if os.path.isfile(fname):
            error_message = "Temporary file '{}' already exists. Aborting..."
            raise OSError(error_message.format(fname))
        text = ["<?xml version='1.0' encoding='iso-8859-1'?><job>",
                "<name>ase calculator</name>",
                "<atom-types>"]
        for t in type_list:
            text.append("<species>{}</species>".format(t))
        text += ["</atom-types><potentials>",
                 "<abop id='Fe-Potential' species-a='*' species-b='*'>",
                 "<param-file>{}</param-file>".format(file_name),
                 "</abop></potentials><structures></structures></job>"]
        with open(fname, "w") as f:
            f.writelines(text)

        # initialize job
        self.job = Job()
        self.job.parse_input_file(fname)
        Calculator.__init__(self, **kwargs)
        os.remove(fname)

        # Would be nice to do it without the temporary file, but this requires
        # massive changes to the parsing scheme of atomicrex
        # job.add_atom_type(...)
        # job.add_potential(...)
