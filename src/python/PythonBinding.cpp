///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "PythonBinding.h"
#include <arexlib/job/FitJob.h>
#include <arexlib/job/FitGroup.h>
#include <arexlib/structures/AtomicStructure.h>
#include <arexlib/structures/UserStructure.h>
#include <arexlib/structures/LatticeStructures.h>
#include <arexlib/properties/AtomVectorProperty.h>
#include <arexlib/potentials/Potential.h>

using namespace atomicrex;

PYBIND11_MODULE(_atomicrex, m)
{
	using namespace pybind11;

	// Generate doc strings only for classes/methods that are explicitly documented
	// by the developer of the binding interface. Do not auto-generate them
	// for all C++ classes.
	pybind11::options opts;
	opts.disable_function_signatures();

	// Possible values for the verbosity of the logger.
	enum_<LoggerVerbosity>(m, "logger_verbosity")
	  .value("none", atomicrex::none)
	  .value("minimum", atomicrex::minimum)
	  .value("medium", atomicrex::medium)
	  .value("maximum", atomicrex::maximum)
	  .value("debug", atomicrex::debug)
	;

	/**
	   @brief Function for setting the logger verbosity from python.
	   @details A more convenient function is defined in the python module itself.
	   @param verb desired verbosity as LoggerVerbosity enum value
	*/
	m.def("set_msglogger_verbosity", &MsgLogger::setVerbosity);

	// Python class definition of FitJob.
	class_<FitJob>(m, "Job", "Class for handling an atomicrex fitjob.")
	  .def(init<>())
	  .def_property_readonly("name", &FitJob::name, "str: name of fitjob")
	  .def("parse_input_file",
	       &FitJob::parse,
		   "parse_input_file(filename)\n\n"
	       "Parse the general and potential parameters from an XML file.\n\n"
	       "Parameters\n----------\n"
	       "filename: str\n"
	       "    name of XML input file",
		   arg("filename"))
	  .def("prepare_fitting",
	       &FitJob::prepareFitting,
		   "prepare_fitting()\n\n"
	       "Set up a list of all degrees of freedom and properties that are to be fitted. "
	       "This function needs to be called prior to any computation. "
	       "It is called automatically before a model is fitted via the :meth:`perform_fitting` call.")
	  .def("perform_fitting",
	       &FitJob::performFitting,
		   "perform_fitting()\n\n"
	       "Perform the actual fitting by minimizing the residual.")
	  .def("_calculate_residual",
	       &FitJob::calculateResidual,
	       "Calculates the the objective function to be minimized "
	       "during fitting. This is the weighted sum of the deviations "
	       "of all properties from their target values.\n\n"
	       "Parameters\n----------\n"
	       "norm: ResidualNorm\n"
	       "    method employed for calculating the residual contribution "
	       "    of this property to the objective function")
	  .def("_print_potential_parameters",
	       &FitJob::printPotentialDOFList)
	  .def("_print_properties",
	       &FitJob::printFittingProperties)
	  .def("set_potential_parameters",
	       [](FitJob& fj, array_t<double, array::c_style | array::forcecast> arr) {
			if(arr.ndim() != 1)
            	throw std::runtime_error("Incompatible buffer dimension: expected one-dimensional array.");
			if(arr.shape(0) != fj.numActiveDOF())
            	throw std::runtime_error("Incompatible buffer size: Length of array does not match number of degrees of freedom.");
			if(arr.strides(0) != sizeof(double))
            	throw std::runtime_error("Incompatible buffer stride.");
			fj.unpackDOF(arr.data(0));
		   },
	       "set_potential_parameters(values)\n"
		   "\n"
		   "Parameters\n----------\n"
	       "Sets the potential parameters (DOFs) associated with the job.\n\n"
	       "values: array\n"
	       "    list of double values representing the parameters")
	  .def("get_potential_parameters",
	       [](FitJob& fj) {
				array_t<double> array((size_t)fj.numActiveDOF());
				fj.packDOF(array.mutable_data(0));
				return array;
		   },
	       "get_potential_parameters()\n"
		   "\n"
		   "Returns\n-------\nlist of floats:\n    potential parameters (DOFs) associated with job")

	  .def("create_user_structure",
	       [](FitJob& job, const FPString& id) { return new UserStructure(id, &job); }, return_value_policy::reference_internal)

	  .def("create_FCC_structure",
	       [](FitJob& job, const FPString& id) { return new FCCLatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_BCC_structure",
	       [](FitJob& job, const FPString& id) { return new BCCLatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_SC_structure",
	       [](FitJob& job, const FPString& id) { return new SCLatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_DIA_structure",
	       [](FitJob& job, const FPString& id) { return new DIALatticeStructure(id, &job); }, return_value_policy::reference_internal)

	  .def("create_HCP_structure",
	       [](FitJob& job, const FPString& id) { return new HCPLatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_DHCP_structure",
	       [](FitJob& job, const FPString& id) { return new DHCPLatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_OMG_structure",
	       [](FitJob& job, const FPString& id) { return new OMGLatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_betaSn_structure",
	       [](FitJob& job, const FPString& id) { return new betaSnLatticeStructure(id, &job); }, return_value_policy::reference_internal)

	  .def("create_B1_structure",
	       [](FitJob& job, const FPString& id) { return new B1LatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_B2_structure",
	       [](FitJob& job, const FPString& id) { return new B2LatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_B3_structure",
	       [](FitJob& job, const FPString& id) { return new B3LatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_L12_structure",
	       [](FitJob& job, const FPString& id) { return new L12LatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_D8a_structure",
	       [](FitJob& job, const FPString& id) { return new D8aLatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_C1_structure",
	       [](FitJob& job, const FPString& id) { return new C1LatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_C15_structure",
	       [](FitJob& job, const FPString& id) { return new C15LatticeStructure(id, &job); }, return_value_policy::reference_internal)

	  .def("create_Bh_structure",
	       [](FitJob& job, const FPString& id) { return new BhLatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_D2d_structure",
	       [](FitJob& job, const FPString& id) { return new D2dLatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_Ni17Th2_structure",
	       [](FitJob& job, const FPString& id) { return new Ni17Th2LatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_Th2Zn17_structure",
	       [](FitJob& job, const FPString& id) { return new Th2Zn17LatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_L10_structure",
	       [](FitJob& job, const FPString& id) { return new L10LatticeStructure(id, &job); }, return_value_policy::reference_internal)
	  .def("create_B4_structure",
	       [](FitJob& job, const FPString& id) { return new B4LatticeStructure(id, &job); }, return_value_policy::reference_internal)

	  .def_property_readonly("root_group", &FitJob::rootGroup)
	  .def_property_readonly("types",
			[](FitJob& job) {
			    list l;
			    for(int i = 0; i < job.numAtomTypes(); i++)
				  l.append(cast(job.atomTypeName(i)));
			    return l;
			  })
	  .def_property_readonly("_structures", &FitJob::structures)
      .def_property_readonly("_derived_properties", &FitJob::derivedProperties)
	  .def_property_readonly("_potentials", &FitJob::potentials)
	  ;

	// Python class definition for FitObject.
	class_<FitObject>(m, "FitObject")
      .def("registerSubObject",
	        &FitObject::registerSubObject)
	  .def_property("id",
			&FitObject::id,
			&FitObject::setId)
	  .def_property("tag",
			&FitObject::tag,
			&FitObject::setTag)
      .def_property("fit_enabled",
			&FitGroup::fitEnabled,
			&FitGroup::setFitEnabled,
			"boolean: if True all structures in the group will be included "
			"in the calculation of the objective function")
	  .def_property("output_enabled",
			&FitGroup::outputEnabled,
			&FitGroup::setOutputEnabled,
			"boolean: if True all structures in the group will be included "
			"when printing the results")
	  .def_property("relative_weight",
			&FitObject::relativeWeight,
            &FitObject::setRelativeWeight,
            "float: relative weight applied to this object and all subObjects "
			"in the calculation of the objective function")
	  .def_property_readonly("_properties",
			&FitObject::properties)
      .def_property_readonly("subObjects",
			&FitObject::fitObjects,
			"list: sub objects belonging to this fit object. Can be groups, "
            "structures or derived properties.")
	  .def_property_readonly("parent",
			cpp_function(&FitObject::parent, return_value_policy::reference),
			"Reference to the parent of this object.")
	  ;

	// Python class definition for FitGroup.
	class_<FitGroup, FitObject>(m, "FitGroup")
	  .def("create_subgroup",
	       &FitGroup::createSubGroup, return_value_policy::reference_internal)
	  ;

	// Python class definition for FitProperty.
	class_<FitProperty, FitObject>(m, "FitProperty")
	  .def_property("absolute_weight",
			&FitProperty::absoluteWeight,
			&FitProperty::assignAbsoluteWeights,
			"float: absolute weight employed for this property in the calculation of the objective function")
	  .def_property("tolerance",
			&FitProperty::tolerance,
			&FitProperty::setTolerance,
			"float: tolerance employed for this property in the calculation of the objective function")
	  .def_property_readonly("residual_norm",
			&FitProperty::residualNorm,
			"Method employed for calculating the residual contribution of this property to the objective function")
	  .def_property_readonly("residual",
			[](FitProperty& fp) { return fp.residual(fp.residualNorm()); })
	  .def("print_property",
			[](FitProperty& fp) {
				MsgLogger logger(atomicrex::none);
				fp.print(logger);
				logger << std::endl;
			  },
		   "Prints the computed value, target value, weight, etc. of the property. "
		   "Note that it is necessary to call :py:meth:`AtomicStructure.compute_properties` first to update the computed value of the property.")
	  .def("compute",
			&FitProperty::compute)
	;

	// Possible values for style of residual norm.
	enum_<FitProperty::ResidualNorm>(m, "residual_norm")
	  .value("user_defined", FitProperty::ResidualNorm::UserDefined)
	  .value("squared", FitProperty::ResidualNorm::Squared)
	  .value("squared_relative", FitProperty::ResidualNorm::SquaredRelative)
	  .value("absolute_diff", FitProperty::ResidualNorm::AbsoluteDiff)
	  ;

	// Python class definition for DegreeOfFreedom.
	class_<DegreeOfFreedom>(m, "DegreeOfFreedom")
	  .def_property("id",
			&DegreeOfFreedom::id,
			&DegreeOfFreedom::setId)
	  .def_property("tag",
			&DegreeOfFreedom::tag,
			&DegreeOfFreedom::setTag)
	  .def_property("fit_enabled",
			&DegreeOfFreedom::fitEnabled,
			&DegreeOfFreedom::setFitEnabled,
			"boolean: true if this DOF should be varied during the fitting process.")
	  .def_property("relax",
			&DegreeOfFreedom::relax,
			&DegreeOfFreedom::setRelax,
			"boolean: true if this DOF should be relaxed during each iteration when fitting.")
	  ;

	class_<AtomCoordinatesDOF, DegreeOfFreedom>(m, "AtomCoordinatesDOF")
	  ;

	// Python class definition for ScalarDOF.
	class_<ScalarDOF, DegreeOfFreedom>(m, "ScalarDOF")
	  .def_property("value",
			&ScalarDOF::getValue,
			&ScalarDOF::setValue,
			"float: value of the scalar degree of freedom")
	  .def_property("lower_bound",
			&ScalarDOF::lowerBound,
			&ScalarDOF::setLowerBound,
			"float: lower bound of degree of freedom")
	  .def_property("upper_bound",
			&ScalarDOF::upperBound,
			&ScalarDOF::setUpperBound,
			"float: upper bound of degree of freedom")
	  ;

	// Python class definition for ScalarFitProperty.
	class_<ScalarFitProperty, FitProperty>(m, "ScalarFitProperty")
	  .def_property("target_value",
			&ScalarFitProperty::targetValue,
			&ScalarFitProperty::setTargetValue,
			"float: target value of scalar fit property")
	  .def_property_readonly("computed_value",
			&ScalarFitProperty::computedValue,
			"float: computed value of scalar fit property")
	  .def_property_readonly("units",
			&ScalarFitProperty::units,
			"float: units of scalar fit property")
	  ;

    // Python class definition for DerivedProperty.
	class_<DerivedProperty, ScalarFitProperty>(m, "DerivedProperty")
	  ;

	// Python class definition for AtomVectorProperty.
	class_<AtomVectorProperty, FitProperty>(m, "AtomVectorProperty")
	  .def("get_computed_values",
	       [](AtomVectorProperty& fp) {
			    const std::vector<size_t> shape = { (size_t)fp.atomCount(), 3 };
				const std::vector<size_t> strides = { sizeof(Vector3), sizeof(Vector3::value_type) };
				array_t<Vector3::value_type> array(shape, strides,
					reinterpret_cast<const Vector3::value_type*>(fp.values().data()), cast(&fp));
				reinterpret_cast<pybind11::detail::PyArray_Proxy*>(array.ptr())->flags &= ~pybind11::detail::npy_api::NPY_ARRAY_WRITEABLE_;
				return array;
			  },
		   "get_computed_values()\n\n"
	       "Returns\n-------\n"
	       "Nx3 array of floats:\n"
	       "    computed values for an atomic vector property")
	  .def("get_target_values",
	       [](AtomVectorProperty& fp) {
			    const std::vector<size_t> shape = { (size_t)fp.atomCount(), 3 };
				const std::vector<size_t> strides = { sizeof(Vector3), sizeof(Vector3::value_type) };
				array_t<Vector3::value_type> array(shape, strides,
					reinterpret_cast<const Vector3::value_type*>(fp.targetValues().data()), cast(&fp));
				reinterpret_cast<pybind11::detail::PyArray_Proxy*>(array.ptr())->flags &= ~pybind11::detail::npy_api::NPY_ARRAY_WRITEABLE_;
				return array;
			  },
		   "get_target_values()\n\n"
	       "Returns\n-------\n"
	       "Nx3 array of floats:\n"
	       "    target values for an atomic vector property")
	  .def("set_target_values",
	       [](AtomVectorProperty& prop, array_t<double> arr) {
			if(arr.ndim() != 2)
            	throw std::runtime_error("Incompatible buffer dimensions: expected two-dimensional array.");
			if(arr.shape(0) != prop.atomCount() || arr.shape(1) != 3)
            	throw std::runtime_error("Incompatible buffer size: Expected Nx3 array with N being the number of atoms in the structure.");
			for(int i = 0; i < prop.atomCount(); i++)
				prop.setTargetValue(i, {arr.at(i,0), arr.at(i,1), arr.at(i,2)});
		   },
	       "set_target_values(vector)\n\n"
	       "Sets the target values for an atomic vector property.\n\n"
	       "Parameters\n----------\n"
	       "vector: array\n"
	       "    List of atomic vectors (N x 3 double values)")
	  .def_property_readonly("units",
			&AtomVectorProperty::units,
			"float: unit of atom vector property")
	  ;

	// Python class definition for CoupledFitProperty.
	class_<CoupledFitProperty, ScalarFitProperty>(m, "CoupledFitProperty")
		.def_property("relax",
			[](CoupledFitProperty& cfp) { return cfp.dof().relax(); },
			[](CoupledFitProperty& cfp, bool enable) { cfp.dof().setRelax(enable); },
			"Set relax flag of coupled degree of freedom.")
		.def_property("lower_bound",
			[](CoupledFitProperty& cfp) { return cfp.dof().lowerBound(); },
			[](CoupledFitProperty& cfp, double minValue) { cfp.dof().setLowerBound(minValue); },
			"float: lower bound of coupled degree of freedom")
		.def_property("upper_bound",
			[](CoupledFitProperty& cfp) { return cfp.dof().upperBound(); },
			[](CoupledFitProperty& cfp, double maxValue) { cfp.dof().setUpperBound(maxValue); },
			"float: upper bound of coupled degree of freedom")
		;

	// Python class definition for AtomicStructure.
	class_<AtomicStructure, FitObject>(m, "AtomicStructure",
			    "Class for handling atomic structures.")
	  .def("update_structure",
	       &AtomicStructure::updateStructure)
	  .def("set_positions",
	  	   &AtomicStructure::setAtomPositions)
	  .def("_compute_properties",
	       &AtomicStructure::computeProperties,
	       "Computes all enabled properties of the structures.\n\n"
	       "Parameters\n----------\n"
	       "isFitting: bool\n"
	       "    Determines whether this DOF should be relaxed (usually the desired behavior depends on the program phase).")
	  .def("_compute_energy",
	       &AtomicStructure::computeEnergy,
	       "Computes the total energy and optionally the forces for this structure."
	       " The function returns a scalar representing the energy of the structure."
	       " If the forces are computed they are stored in the structure object.\n\n"
	       "Parameters\n----------\n"
	       "computeForces: bool\n"
	       "    Turns on the computation of forces.\n"
	       "isFitting: bool\n"
	       "    An internal parameter that is True if the structure is included in the fitting data set (usually the desired behavior depends on the program phase).\n"
	       "suppressRelaxation: bool\n"
	       "    If True structure relaxation will be suppressed.\n")
	  .def("relax",
	       &AtomicStructure::relax)
	  .def("write_lammps_dump",
	       &AtomicStructure::writeToDumpFile,
		   "write_lammps_dump(filename, ghosts = False)\n\n"
	       "Exports the structure to a LAMMPS dump file.\n\n"
	       "Parameters\n----------\n"
	       "filename: string\n"
	       "    Name of the output file.\n"
	       "ghosts: bool\n"
	       "    If True, ghost atoms used during computation of the energy will be included in the output.",
		   arg("filename"),
		   arg("ghosts") = false)
	  .def("write_poscar",
	       &AtomicStructure::writeToPoscarFile,
		   "write_poscar(filename, ghosts = False)\n\n"
	       "Exports the structure to a POSCAR file.\n\n"
	       "Parameters\n----------\n"
	       "filename: string\n"
	       "    Name of the output file.\n"
	       "ghosts: bool\n"
	       "    If True, ghost atoms used during computation of the energy will be included in the output.",
		   arg("filename"),
	       arg("ghosts") = false)

	  .def("setup_simulation_cell",
	       &AtomicStructure::setupSimulationCell)
	  .def("deform_simulation_cell",
	       &AtomicStructure::deformSimulationCell,
	       "deform_simulation_cell(deformation)\n\n"
	       "Applies an affine transformation to the cell and the atoms.\n\n"
	       "Parameters\n----------\n"
	       "deformation: 3x3 matrix\n"
	       "    deformation matrix that is applied to the simulation cell.")
	  .def_property_readonly("cell", MatrixGetter<AtomicStructure, Matrix3, &AtomicStructure::simulationCell>(),
			"matrix: simulation cell metric")
	  .def_property_readonly("cell_origin", &AtomicStructure::simulationCellOrigin,
			"vector: origin point of the simulation cell in world coordinates")
	  .def("has_pbc",
	       &AtomicStructure::hasPBC)

	  .def_property("local_atom_count",
			&AtomicStructure::numLocalAtoms,
			&AtomicStructure::setAtomCount)
	  .def_property_readonly("positions",
			[](AtomicStructure& s) {
			    // Return a Numpy array that references our internal data buffer.
                const std::vector<size_t> shape = { (size_t)s.numLocalAtoms(), 3 };
                const std::vector<size_t> strides = { sizeof(Point3), sizeof(Point3::value_type) };
                array_t<Point3::value_type> array(shape, strides,
                    reinterpret_cast<Point3::value_type*>(s.atomPositions().data()), cast(&s));
                // Mark the Numpy array as read-only. We don't want anybody to mess with our stored
                // atomic positions. To change the atomic positions, one should use the set_positions() method.
                reinterpret_cast<pybind11::detail::PyArray_Proxy*>(array.ptr())->flags &= ~pybind11::detail::npy_api::NPY_ARRAY_WRITEABLE_;
                return array;
			  },
			"N x 3 doubles: atomic positions")
	  .def_property_readonly("types",
			[](AtomicStructure& s) {
				array_t<int> array((size_t)s.numLocalAtoms(), s.atomTypes().data(), cast(&s));
                reinterpret_cast<pybind11::detail::PyArray_Proxy*>(array.ptr())->flags &= ~pybind11::detail::npy_api::NPY_ARRAY_WRITEABLE_;
                return array;
			  },
			"list of integers: atom types")
	  .def_property_readonly("tags",
			[](AtomicStructure& s) {
				array_t<int> array((size_t)s.numLocalAtoms(), s.atomTags().data(), cast(&s));
                reinterpret_cast<pybind11::detail::PyArray_Proxy*>(array.ptr())->flags &= ~pybind11::detail::npy_api::NPY_ARRAY_WRITEABLE_;
                return array;
			  },
			"list of integers: atom tags")
	  .def_property_readonly("forces",
			[](AtomicStructure& s) {
			    const std::vector<size_t> shape = { (size_t)s.numLocalAtoms(), 3 };
                const std::vector<size_t> strides = { sizeof(Vector3), sizeof(Vector3::value_type) };
                array_t<Vector3::value_type> array(shape, strides,
                    reinterpret_cast<Vector3::value_type*>(s.atomForces().data()), cast(&s));
                reinterpret_cast<pybind11::detail::PyArray_Proxy*>(array.ptr())->flags &= ~pybind11::detail::npy_api::NPY_ARRAY_WRITEABLE_;
                return array;
			  },
			"N x 3 doubles: list of atomic forces")
	  .def_property_readonly("potential_energy",
			&AtomicStructure::totalEnergy,
			"float: potential energy of the structure")

	  // Some functions that make the structure behave more like an ASE Atoms object.
	  .def("get_potential_energy",
	       &AtomicStructure::totalEnergy,
		   "get_potential_energy()\n\n"
	       "Returns\n-------\nfloat:\n    potential energy of structure")
	  .def("get_number_of_atoms",
	       &AtomicStructure::numLocalAtoms,
		   "get_number_of_atoms()\n\n"
	       "Returns\n-------\nint:\n    number of atoms in structure")
	  .def("get_positions",
           [](AtomicStructure& s) {
                // Return Numpy array that contains a copy of our internal data array.
                const std::vector<size_t> shape = { (size_t)s.numLocalAtoms(), 3 };
                const std::vector<size_t> strides = { sizeof(Point3), sizeof(Point3::value_type) };
                array_t<Point3::value_type> array(shape, strides,
                    reinterpret_cast<const Point3::value_type*>(s.atomPositions().data()));
                return array;
           },
           "get_positions()\n\n"
           "Return the positions of the atoms\n\n"
           "Returns\n-------\nNx3 array:\n    atomic positions")
      .def("set_positions",
           [](AtomicStructure& s, array_t<Point3::value_type> array) {
                if(array.ndim() != 2)
                    throw value_error("Array must be two-dimensional");
                if(array.shape(0) != s.numLocalAtoms() || array.shape(1) != 3)
                    throw value_error("Array has wrong dimensions. Must be an Nx3 array with N being the existing number of local atoms in the structure.");
                for(int i = 0; i < s.numLocalAtoms(); i++)
                    for(int k = 0; k < 3; k++)
                        s.atomPositions()[i][k] = array.at(i, k);
                s.setDirty(AtomicStructure::ATOM_POSITIONS);
           },
           "set_positions(positions)\n\n"
           "Set atomic positions to some new values.\n\n"
           "Parameters\n----------\npos : Nx3 array\n    new atomic positions")
      .def("get_types",
            [](AtomicStructure& s) {
                array_t<int> array((size_t)s.numLocalAtoms(), s.atomTypes().data());
                reinterpret_cast<pybind11::detail::PyArray_Proxy*>(array.ptr())->flags &= ~pybind11::detail::npy_api::NPY_ARRAY_WRITEABLE_;
                return array;
              },
           "get_types()\n\n"
           "Return the atom types in the form of a list.\n\n"
           "Notes\n-----\nThe atom type indices start at 0 (zero).\n\n"
           "Returns\n-------\nN ints:\n    atom types")
      .def("set_types",
           [](AtomicStructure& s, array_t<int> array) {
                if(array.ndim() != 1)
                    throw value_error("Array must be one-dimensional");
                if(array.shape(0) != s.numLocalAtoms())
                    throw value_error("Array has wrong dimensions. Must be an N array with N being the existing number of local atoms in the structure.");
                for(int i = 0; i < s.numLocalAtoms(); i++)
                    s.atomTypes()[i] = array.at(i);
                s.setDirty(AtomicStructure::ATOM_POSITIONS);
           },
           "set_types(types)\n\n"
           "Set atom types.\n\n"
           "Parameters\n----------\ntypes : N array of ints\n    new atom types\n\n"
           "Notes\n-----\nThe atom type indices start at 0 (zero).")
	  .def("get_forces",
           [](AtomicStructure& s) {
                const std::vector<size_t> shape = { (size_t)s.numLocalAtoms(), 3 };
                const std::vector<size_t> strides = { sizeof(Vector3), sizeof(Vector3::value_type) };
                array_t<Vector3::value_type> array(shape, strides,
                    reinterpret_cast<const Vector3::value_type*>(s.atomForces().data()));
                reinterpret_cast<pybind11::detail::PyArray_Proxy*>(array.ptr())->flags &= ~pybind11::detail::npy_api::NPY_ARRAY_WRITEABLE_;
                return array;
           },
           "get_forces()\n\n"
           "Return the forces on the atoms in the form of an array.\n\n"
           "Returns\n-------\nlist of vectors:\n    atomic forces")
	  .def("get_pressure_tensor", &AtomicStructure::pressureTensor,
		   "get_pressure_tensor(voigtIndex)\n\n"
		   "Parameters\n----------\n"
	       "filename: int\n"
	       "    voigtIndex Index of the pressure tensor component in Voigt notation.\n"
	       "Returns\n-------\ndouble:\n    pressure tensor entry")
	  .def("get_cell", &AtomicStructure::simulationCell,
	       "get_cell()\n\n"
	       "Returns\n-------\nmatrix:\n    cell metric")
	  ;

	// Python class definition for UserStructure.
	class_<UserStructure, AtomicStructure>(m, "UserStructure")
	  .def("load_lammps_dump",
	       &UserStructure::loadLAMMPSDumpFile,
	       "Write the structure to file using the LAMMPS dump file format.\n"
	       "The LAMMPS dump format is described <a href='http://lammps.sandia.gov/doc/dump.html'>here</a>.\n\n"
	       "Parameters\n----------\n"
	       "filepath: string\n"
	       "    path to the output file")
	  .def("load_poscar",
	       &UserStructure::loadPOSCARFile,
	       "Write the structure to file using the VASP POSCAR file format.\n"
	       "The VASP POSCAR format is described <a href='http://cms.mpi.univie.ac.at/vasp/vasp/POSCAR_file.html'>here</a>.\n\n"
	       "Parameters\n----------\n"
	       "filepath: string\n"
	       "    path to the output file")
	  ;

	// Python class definitions for unary cubic lattice structures.
	class_<UnaryCubicLatticeStructure, AtomicStructure>(m, "UnaryCubicLatticeStructure")
	  .def_property_readonly("lattice_parameter_initial",
			&UnaryCubicLatticeStructure::latticeParameter,
			"float: initial lattice parameter (for structure creation)")
	  .def_property_readonly("lattice_parameter_property",
			&UnaryCubicLatticeStructure::latticeParameterProperty,
			"property: lattice parameter as property")
	  .def_property("type",
			&UnaryCubicLatticeStructure::atomType,
			&UnaryCubicLatticeStructure::setAtomType,
			"int: atom type")
	  ;
	class_<FCCLatticeStructure, UnaryCubicLatticeStructure>(m, "FCCLatticeStructure");
	class_<BCCLatticeStructure, UnaryCubicLatticeStructure>(m, "BCCLatticeStructure");
	class_<SCLatticeStructure,  UnaryCubicLatticeStructure>(m, "SCLatticeStructure");
	class_<DIALatticeStructure, UnaryCubicLatticeStructure>(m, "DIALatticeStructure");

	// Python class definitions for unary hexagonal/rhombohedral/tetragonal lattice structures.
	class_<UnaryHexaRhomboTetraLatticeStructure, AtomicStructure>(m, "UnaryHexaRhomboTetraLatticeStructure")
		.def_property_readonly("lattice_parameter_initial",
				&UnaryHexaRhomboTetraLatticeStructure::latticeParameter,
				"float: initial lattice parameter (for structure creation)")
		.def_property_readonly("lattice_parameter_property",
				&UnaryHexaRhomboTetraLatticeStructure::latticeParameterProperty,
				"property: lattice parameter as property")
		.def_property_readonly("ca_ratio_initial",
				&UnaryHexaRhomboTetraLatticeStructure::CtoAratio,
				"float: initial lattice parameter c/a")
		.def_property_readonly("ca_ratio_property",
				&UnaryHexaRhomboTetraLatticeStructure::CtoAratioProperty,
				"property: lattice parameter c/a as property")
		.def_property("type",
				&UnaryHexaRhomboTetraLatticeStructure::atomType,
				&UnaryHexaRhomboTetraLatticeStructure::setAtomType,
				"int: atom type")
	  ;
	class_<HCPLatticeStructure,    UnaryHexaRhomboTetraLatticeStructure>(m, "HCPLatticeStructure");
	class_<DHCPLatticeStructure,   UnaryHexaRhomboTetraLatticeStructure>(m, "DHCPLatticeStructure");
	class_<betaSnLatticeStructure, UnaryHexaRhomboTetraLatticeStructure>(m, "betaSnLatticeStructure");
	class_<OMGLatticeStructure,    UnaryHexaRhomboTetraLatticeStructure>(m, "OMGLatticeStructure");

	// Python class definitions for binary cubic lattice structures.
	class_<BinaryCubicLatticeStructure, AtomicStructure>(m, "BinaryCubicLatticeStructure")
		.def_property_readonly("lattice_parameter_initial",
				&BinaryCubicLatticeStructure::latticeParameter,
				"float: initial lattice parameter (for structure creation)")
		.def_property_readonly("lattice_parameter_property",
				&BinaryCubicLatticeStructure::latticeParameterProperty,
				"property: lattice parameter as property")
		.def_property("type_A",
				&BinaryCubicLatticeStructure::atomTypeA,
				&BinaryCubicLatticeStructure::setAtomTypeA,
				"int: atom type A")
		.def_property("type_B",
				&BinaryCubicLatticeStructure::atomTypeB,
				&BinaryCubicLatticeStructure::setAtomTypeB,
				"int: atom type B")
	  ;
	class_<B1LatticeStructure,  BinaryCubicLatticeStructure>(m, "B1LatticeStructure");
	class_<B2LatticeStructure,  BinaryCubicLatticeStructure>(m, "B2LatticeStructure");
	class_<B3LatticeStructure,  BinaryCubicLatticeStructure>(m, "B3LatticeStructure");
	class_<L12LatticeStructure, BinaryCubicLatticeStructure>(m, "L12LatticeStructure");
	class_<D8aLatticeStructure, BinaryCubicLatticeStructure>(m, "D8aLatticeStructure");
	class_<C1LatticeStructure,  BinaryCubicLatticeStructure>(m, "C1LatticeStructure");
	class_<C15LatticeStructure, BinaryCubicLatticeStructure>(m, "C15LatticeStructure");

	// Python class definitions for binary hexagonal/rhombohedral/tetragonal lattice structures.
	class_<BinaryHexaRhomboTetraLatticeStructure, AtomicStructure>(m, "BinaryHexaRhomboTetraLatticeStructure")
		.def_property_readonly("lattice_parameter_initial",
				&BinaryHexaRhomboTetraLatticeStructure::latticeParameter,
				"float: initial lattice parameter (for structure creation)")
		.def_property_readonly("lattice_parameter_property",
				&BinaryHexaRhomboTetraLatticeStructure::latticeParameterProperty,
				"property: lattice parameter as property")
		.def_property_readonly("ca_ratio_initial",
				&BinaryHexaRhomboTetraLatticeStructure::CtoAratio,
				"float: initial lattice parameter c/a")
		.def_property_readonly("ca_ratio_property",
				&BinaryHexaRhomboTetraLatticeStructure::CtoAratioProperty,
				"property: lattice parameter c/a as property")
		.def_property("type_A",
				&BinaryHexaRhomboTetraLatticeStructure::atomTypeA,
				&BinaryHexaRhomboTetraLatticeStructure::setAtomTypeA,
				"int: atom type A")
		.def_property("type_B",
				&BinaryHexaRhomboTetraLatticeStructure::atomTypeB,
				&BinaryHexaRhomboTetraLatticeStructure::setAtomTypeB,
				"int: atom type B")
	  ;
	class_<BhLatticeStructure,      BinaryHexaRhomboTetraLatticeStructure>(m, "BhLatticeStructure");
	class_<D2dLatticeStructure,     BinaryHexaRhomboTetraLatticeStructure>(m, "D2dLatticeStructure");
	class_<Ni17Th2LatticeStructure, BinaryHexaRhomboTetraLatticeStructure>(m, "Ni17Th2LatticeStructure");
	class_<Th2Zn17LatticeStructure, BinaryHexaRhomboTetraLatticeStructure>(m, "Th2Zn17LatticeStructure");
	class_<L10LatticeStructure,     BinaryHexaRhomboTetraLatticeStructure>(m, "L10LatticeStructure");
	class_<B4LatticeStructure,      BinaryHexaRhomboTetraLatticeStructure>(m, "B4LatticeStructure")
		.def_property_readonly("internal_parameter_u_initial",
			&B4LatticeStructure::internalParameterU,
			"float: initial internal parameter u")
		.def_property_readonly("internal_parameter_u_property",
			&B4LatticeStructure::internalParameterUProperty,
			"property: internal parameter u as property")
	  ;

	// Python class definition for Potential.
	class_<Potential, FitObject>(m, "Potential")
	  .def_property_readonly("cutoff", &Potential::cutoff)
	  .def("output_results",
	       &Potential::outputResults,
		   "output_results()\n\n"
	       "Exports the resulting potential parameters to a file.\n\n")
	  ;
}
