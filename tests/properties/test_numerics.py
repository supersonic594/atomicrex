from __future__ import print_function
from subprocess import Popen, PIPE
from os import environ
import yaml


def get_computed_values(f):
    data = {}
    for line in f.readlines():
        # this takes care of Python3 compatibility
        line = line.decode('UTF-8')
        if 'Structure' in line:
            structure = line.split()[1]
            structure = structure.replace("'", '')
            structure = structure.replace(':', '')
            structure = str(structure)
            data[structure] = {}
        for prop in ['atomic-energy', 'lattice-parameter', 'ca-ratio',
                     'pxx', 'pyy', 'pzz', 'pyz', 'pxz', 'pxy',
                     'C11', 'C12', 'C13',
                     'C44', 'C44', 'C55', 'C66']:
            if prop in line:
                data[structure][prop] = float(line.split()[1])
    return data

if __name__ == '__main__':
    print('Testing numerics of property computation')
    with open('reference-data.yml', 'r') as infile:
        refdata = yaml.load(infile)
    fname = 'main-numerics.xml'
    proc = Popen([environ['ATOMICREX_EXECUTABLE'], fname], stdout=PIPE)
    data = get_computed_values(proc.stdout)
    for structure in data:
        for key in data[structure]:
            print('{} - {}'.format(structure, key))
            value = data[structure][key]
            value = value if abs(value) > 1e-6 else 0.0
            assert abs(value - refdata[structure][key]) < 1e-6
