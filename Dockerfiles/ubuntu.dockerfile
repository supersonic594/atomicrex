# Base image
FROM ubuntu:latest

# Upgrade the base pacakges
RUN apt-get update -qy
RUN apt-get upgrade -qy

# Install packages that are needed for building the code
RUN apt-get install -qy cmake \
                        build-essential \
                        gfortran \
                        libboost-dev \
                        libboost-system-dev \
                        libboost-thread-dev \
                        libboost-filesystem-dev \
                        libxml2-dev \
                        libnlopt-dev \
                        libmuparser-dev

# - the vim-common package provides the xxd program,
# which is used during compilation
RUN apt-get install -qy vim-common

# - packages for Python3
RUN apt-get install -qy python3-dev \
                        python3-pip

# - packages for Python2
RUN apt-get install -qy python-dev \
                        python-pip

# - packages needed for building the homepage
RUN apt-get install -qy wget \
                        unzip

# Set up additional Python2 packages via pip
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
RUN pip install ase \
                coverage \
                flake8 \
		numpy \
		pyyaml \
		scipy \
		sphinx \
                sphinx-rtd-theme \
                sphinx_sitemap


# Set up additional Python3 packages via pip
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools
RUN pip3 install ase \
                 coverage \
                 flake8 \
		 numpy \
		 pyyaml \
		 scipy \
		 sphinx \
                 sphinx-rtd-theme \
                 sphinx_sitemap
