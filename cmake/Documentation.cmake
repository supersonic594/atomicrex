###############################################################################
# 
#  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
#
#  This file is part of atomicrex.
#
#  Atomicrex is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Atomicrex is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# This script define CMake targets that generate the user manual using Sphinx 
# and the C++ API documentation using Doxygen.

# Building the user guide using Sphinx requires a working Python interpreter.
# Note: Building the Atomicrex Python module must be enabled to find the Python interpreter.
IF(NOT PYTHON_EXECUTABLE)
	RETURN()
ENDIF()

# Check if Sphinx is installed.
EXECUTE_PROCESS(COMMAND "${PYTHON_EXECUTABLE}" 
	"-c" "from sphinx import main"
    RESULT_VARIABLE _SPHINX_RETVALUE
	ERROR_QUIET OUTPUT_QUIET)

IF(_SPHINX_RETVALUE MATCHES 0)
	# Create the CMake target 'userguide' that invokes Sphinx to generate the 
	# user documentation in HTML format. 
	ADD_CUSTOM_TARGET(userguide 
		COMMAND "${CMAKE_COMMAND}" -E env "PYTHONPATH=${CMAKE_BINARY_DIR}/python"
		"${PYTHON_EXECUTABLE}"

		# Invoke Sphinx generator:
		"-c" "import sys;from sphinx import main;sys.exit(main(sys.argv))"

		# Sphinx command line options:
		"-b" "html" # Generate user guide in HTML format
		"-W"        # Treat warnings as errors

		# Pass release number to the Sphinx generator to keep user guide in sync with code:
		"-D" "version=${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}" 
		"-D" "release=${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}"

		"source"   # Input directory containing conf.py
		"build"	   # Output directory

		WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/doc/userguide/"
		COMMENT "Generating user guide"
		VERBATIM)

	# In order for Sphinx auto-doc to generate the documentation for the Python bindings, 
	# our Python module should be up to date.
	ADD_DEPENDENCIES(userguide atomicrexpy)

ELSE()
	MESSAGE(STATUS "Sphinx is not available to build the user guide ('import sphinx' failed for Python interpreter ${PYTHON_EXECUTABLE}).")
ENDIF()

